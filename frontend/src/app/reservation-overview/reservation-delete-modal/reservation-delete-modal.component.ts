import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Reservation } from '../state/reservation.model';
import { ReservationService } from '../state/reservation.service';

@Component({
  selector: 'app-reservation-delete-modal',
  templateUrl: './reservation-delete-modal.component.html',
  styleUrls: ['./reservation-delete-modal.component.scss']
})
export class ReservationDeleteModalComponent implements OnInit {

  reservation: Reservation;

  constructor(
    public activeModal: NgbActiveModal,
    private reservationService: ReservationService
  ) { }

  ngOnInit(): void {
  }

  onDelete() {
    this.reservationService.remove(this.reservation.id);
    this.activeModal.close();
  }

}
