import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl, ValidatorFn, Validator } from '@angular/forms';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenStorageService } from 'src/app/auth/token-storage.service';
import { UI_STATE } from '../state/reservation.store';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ShowErrorModalComponent } from 'src/app/shared/components/show-error-modal/show-error-modal.component';
import { RoomReservation, PaymentInformation, Reservation, PickableRoomType } from '../state/reservation.model';
import { ReservationService } from '../state/reservation.service';
import { ReservationQuery } from '../state/reservation.query';
import { RoomService } from '../state/room.service';
import { RoomTypeQuery } from '../state/room.query';
import { ReservationCreateModalComponent } from '../reservation-create-modal/reservation-create-modal.component';
import { AVAILABLE_ROOMS } from '../state/available-rooms';
import { Room, getCopyArray, getCopy } from '../state/available-rooms.model';
import { findIndex } from 'rxjs/operators';
import { RoomReservationPipe } from 'src/app/pipes/room-reservation.pipe';

@Component({
  selector: 'app-reservation-create',
  templateUrl: './reservation-create.component.html',
  styleUrls: ['./reservation-create.component.scss'],
  providers: [DatePipe]
})
export class ReservationCreateComponent implements OnInit {
  
  uiState$: Observable<UI_STATE>;

  UI_STATE = UI_STATE;

  isLoggedIn = false;

  reservationForm: FormGroup;
  reservationID: number | string = -1;
  updateReservation: Reservation;

  availableRoomTypes: PickableRoomType[] = [];
  roomTypesPickable: PickableRoomType[] = [];
  rooms: RoomReservation[] = [];

  constructor(
    private modalService: NgbModal,
    private router: Router,
    private route: ActivatedRoute,
    private tokenStorageService: TokenStorageService,
    private datePipe: DatePipe,
    private reservationService: ReservationService,
    private reservationQuery: ReservationQuery,
    private roomService: RoomService,
    private roomQuery: RoomTypeQuery,
  ) { 
    this.loadData();

    this.roomQuery.selectAll().subscribe(
      (rooms) => {
        if (rooms.length <= 0) {
          console.log("No rooms available...");
        } else {
          this.rooms = [];
          this.availableRoomTypes = rooms.map((room) => {
            if (room.quantityOvercrowding > 0)
              return [{room_type: room, overcrowding: true}, {room_type: room, overcrowding: false}]
            return [{room_type: room, overcrowding: false}]
          }).reduce((prevRoom, curRoom) => prevRoom.concat(curRoom)).sort((roomA, roomB) => {
            if (roomA.overcrowding) {
              if (roomB.overcrowding)
                return 0;
              else
                return 1;
            } else {
              if (roomB.overcrowding)
                return -1;
              else
                return 0; 
            }
          });
          if (this.reservationID == -1) {
            // New reservation
            this.rooms.push({room_type: this.availableRoomTypes[0].room_type, count: 1, overcrowding: this.availableRoomTypes[0].overcrowding})
          } else {
            // Update reservation
            this.reservationQuery.selectEntity(this.reservationID).subscribe( reservation => {
              if (!reservation || reservation == undefined) {
                this.onReservationOverview();
              }
              reservation.rooms.forEach((roomReservation) => this.rooms.push({room_type: roomReservation.room_type, count: roomReservation.count, overcrowding: roomReservation.overcrowding}));
            });
          }
          this.filterPickableRoomTypes();
        }
      }
    );

    this.uiState$ = reservationQuery.UIState$;
    this.route.params.subscribe(params => {
      if ('id' in params) {
        this.reservationID = params['id'];
      } else {
        this.reservationID = -1;
      }
    });
    this.reservationService.updateUIState(UI_STATE.DEFAULT);

    this.isLoggedIn = this.tokenStorageService.isLoggedIn();
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        if (!event.url.includes("reservation")) {
          this.isLoggedIn = false;
        }
      }
    });
  }

  ngOnInit(): void {
    var today = new Date(); 
    var yesterday = new Date();
    yesterday.setDate(today.getDate() - 1);
    var tomorrow = new Date();
    tomorrow.setDate(today.getDate() + 1);
    var inTwoDays = new Date();
    inTwoDays.setDate(today.getDate() + 2);
    var inThreeDays = new Date();
    inThreeDays.setDate(today.getDate() + 3);

    this.reservationForm = new FormGroup({
      arrivalDate: new FormControl('', [Validators.required, this.checkMinDateValidator(this.isLoggedIn ? yesterday : today)]),
      departureDate: new FormControl('', ),
      firstName: new FormControl('', [Validators.required, ]),
      lastName: new FormControl('', [Validators.required, ]),
      telefonnumber: new FormControl('', [Validators.required, ]),
      email: new FormControl('', [Validators.required, Validators.email]),
      address: new FormControl('', [Validators.required, ]),
      plz: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(4), this.checkOnlyNumbers()]),
      city: new FormControl('', [Validators.required, ]),
      discount: new FormControl('', [Validators.required, Validators.min(0), Validators.max(100)]),
      creditCardHolder: new FormControl({value: "", disabled: true}, [Validators.required, ]),
      creditCardNumber: new FormControl('', [Validators.required, ]),
      creditCardValidUntilMonth: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(2), this.checkOnlyNumbers(), Validators.max(12)]),
      creditCardValidUntilYear: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(2), this.checkOnlyNumbers(), Validators.min(new Date().getFullYear() - 2000)]),
      creditCardVerificationNumber: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(3), this.checkOnlyNumbers()]),
    });

    this.departureDate.setValidators([Validators.required, this.checkDepartureDateAfterArrivalValidator(this.reservationForm, 'arrivalDate'), this.checkMinDateValidator(this.isLoggedIn ? today : tomorrow)]);

    this.discount.setValue(0);
    if (this.isLoggedIn) {
      if(this.reservationID === -1) {
        this.arrivalDate.setValue(this.datePipe.transform(today, "yyyy-MM-dd"));
        this.departureDate.setValue(this.datePipe.transform(inTwoDays, "yyyy-MM-dd"));
      } else {
        this.reservationQuery.selectEntity(this.reservationID).subscribe( reservation => {
          if (!reservation || reservation == undefined) {
            this.onReservationOverview();
          }
          this.updateReservation = reservation;
          this.arrivalDate.setValue(this.datePipe.transform(this.updateReservation.arrival_date, "yyyy-MM-dd"));
          this.departureDate.setValue(this.datePipe.transform(this.updateReservation.departure_date, "yyyy-MM-dd"));
          this.firstName.setValue(this.updateReservation.firstName);
          this.lastName.setValue(this.updateReservation.lastName);
          this.telefonnumber.setValue(this.updateReservation.tel);
          this.email.setValue(this.updateReservation.email);
          this.address.setValue(this.updateReservation.address);
          this.plz.setValue(this.updateReservation.postcode);
          this.city.setValue(this.updateReservation.city);
          this.discount.setValue(this.updateReservation.discount);
          this.creditCardHolder.setValue(this.updateReservation.paymentInformation.creditCardHolder);
          this.creditCardNumber.setValue(this.updateReservation.paymentInformation.creditCardNumber);
          this.creditCardValidUntilMonth.setValue(this.updateReservation.paymentInformation.creditCardValidUntilMonth);
          this.creditCardValidUntilYear.setValue(this.updateReservation.paymentInformation.creditCardValidUntilYear);
          this.creditCardVerificationNumber.setValue(this.updateReservation.paymentInformation.creditCardVerificationNumber);
          this.rooms = [];
          this.updateReservation.rooms.forEach((roomReservation) => this.rooms.push({room_type: roomReservation.room_type, count: roomReservation.count, overcrowding: roomReservation.overcrowding}))
        });
      }
    }
    else {
      this.arrivalDate.setValue(this.datePipe.transform(tomorrow, "yyyy-MM-dd"));
      this.departureDate.setValue(this.datePipe.transform(inThreeDays, "yyyy-MM-dd"));
    }
  }
  
  loadData(): void {
    this.roomService.get();
  }

  filterPickableRoomTypes() {
    this.roomTypesPickable = this.availableRoomTypes.filter((roomType) => this.roomTypeIsPickable(roomType, this.rooms));
  }

  roomTypeIsPickable(roomType: PickableRoomType, rooms: RoomReservation[]): boolean {
    let foundRoom = rooms.find((roomReservation) => roomReservation.room_type.id == roomType.room_type.id && roomReservation.overcrowding == roomType.overcrowding);
    if (foundRoom !== undefined)
      return false;
    return true;
  }

  onValidate() {
    this.departureDate.setValue(this.departureDate.value);
    this.reservationForm.markAllAsTouched();
    var firstErrorFound = false;
    Object.keys(this.reservationForm.controls).forEach(key => {
      if (!firstErrorFound && this.reservationForm.controls[key].errors !== null) {
        const error = this.modalService.open(ShowErrorModalComponent);
        error.componentInstance.headline = "Eingaben prüfen";
        error.componentInstance.errormessage = "Bitte überprüfen Sie die Eingaben der Reservierung.";
        firstErrorFound = true;
        return;
      }
    });
    if (firstErrorFound) return;

    let days = this.diffDays(new Date(this.departureDate.value), new Date(this.arrivalDate.value));
    //this.onDoubleCheck(this.calcPrice(days));
    if (this.reservationID === -1) {
      this.reservationService.check_rooms(this.generateReservation(this.calcPrice(days)), this.checkValid.bind(this));
    } else {
      this.reservationService.check_rooms(this.generateReservation(this.calcPrice(days), this.updateReservation), this.checkValid.bind(this));
    }
  }

  checkValid(sendReservation: Reservation, recievedReservation: Reservation) {
    let valid: boolean  = true;
    sendReservation.rooms.forEach((roomReservationSend) => {
      if (recievedReservation.rooms.find((roomReservationRecieved) => 
        roomReservationRecieved.count == roomReservationSend.count && roomReservationRecieved.overcrowding == roomReservationSend.overcrowding &&
        roomReservationRecieved.room_type.id == roomReservationSend.room_type.id) == undefined) {
          valid = false;
        }
    });
    
    if (valid) {
      this.reservationService.updateUIState(UI_STATE.RESERVATION_VALID);
      this.onDoubleCheck(recievedReservation);
    } else  {
      this.reservationService.updateUIState(UI_STATE.RESERVATION_ERROR);
      this.onNotValid(recievedReservation);
    }
  }

  onNotValid(reservation: Reservation) {
    const error = this.modalService.open(ShowErrorModalComponent);
    error.componentInstance.headline = "Buchung nicht möglich!";
    error.componentInstance.errormessage = "Leider gibt es zu Ihrem gewünschten Termin die gewünschten Zimmer nicht mehr. Es sind nur folgende Zimmer in dem Zeitraum möglich:";
    error.componentInstance.reservation = reservation;
  }

  calcPrice(days: number) : number {
    let sum = 0;
    this.rooms.forEach(roomType => {
      if (roomType.overcrowding)
        sum += roomType.room_type.priceOvercrowding * roomType.count * days;
      else
        sum += roomType.room_type.priceBase * roomType.count * days;
    });
    return sum;
  }

  generateReservation(price: number, originalReservation?: Reservation) : Reservation {
    var reservation: Reservation = {
      id: originalReservation ? originalReservation.id : -1, // Id of reservation
      customer: this.firstName.value + " " + this.lastName.value, // Pre + Sur name of customer
      customerId: originalReservation ? originalReservation.customerId : "", // Customer id
      firstName: this.firstName.value,
      lastName: this.lastName.value,
      tel: this.telefonnumber.value,
      email: this.email.value,
      address: this.address.value,  // Address of customer
      postcode: this.plz.value, // Postcode of customer
      city: this.city.value, // City of customer
      rooms: this.rooms,  // Room reservations made by the customer 
      arrival_date: new Date(this.arrivalDate.value), // Arrival date of the customer
      departure_date: new Date(this.departureDate.value), // Departure date of the customer
      num_stays_days: this.diffDays(new Date(this.departureDate.value), new Date(this.arrivalDate.value)),  // Number of days at the hotel
      price: price * (100 - this.discount.value) / 100,  // Price in €
      discount: this.discount.value, // Discount in percentage.
      paymentInformation: this.generatePaymentInformation(
        this.firstName.value + " " + this.lastName.value,
        this.creditCardNumber.value,
        this.creditCardValidUntilMonth.value,
        this.creditCardValidUntilYear.value,
        this.creditCardVerificationNumber.value
      ),
    };
    return reservation;
  }

  generatePaymentInformation(creditCardHolder: string, creditCardNumber: string, creditCardValidUntilMonth: string, 
    creditCardValidUntilYear: string, creditCardVerificationNumber: string): PaymentInformation {
      var paymentInformation: PaymentInformation = {
        creditCardHolder: creditCardHolder, 
        creditCardNumber: creditCardNumber, 
        creditCardValidUntilMonth: creditCardValidUntilMonth, 
        creditCardValidUntilYear: creditCardValidUntilYear, 
        creditCardVerificationNumber: creditCardVerificationNumber
      };
      return paymentInformation;
    }

  onDoubleCheck(reservation: Reservation) {
    const modal = this.modalService.open(ReservationCreateModalComponent);

    modal.componentInstance.reservation = reservation;
    modal.componentInstance.arrivalDate = this.datePipe.transform(reservation.arrival_date, "dd-MM-yyyy")
    modal.componentInstance.departureDate = this.datePipe.transform(reservation.departure_date, "dd-MM-yyyy")
    modal.componentInstance.callback = this.reservationID === -1 ? this.onReserve.bind(this) : this.onUpdate.bind(this);
  }

  onReserve(reservation: Reservation) {
    this.reservationService.add(reservation)

    if (this.isLoggedIn) {
      this.onReservationOverview();
    }
    else {
      const modal = this.modalService.open(ShowErrorModalComponent);
      modal.componentInstance.headline = "Reservierung abgeschickt!"
      modal.componentInstance.errormessage = "Vielen Dank für die Reservierung bei uns! Ein Mitarbeiter wird sich demnächst bei Ihnen melden."
      modal.componentInstance.onCloseCallback = this.onReload;
    }
  }

  onReload() {
    window.location.reload();
  }

  onUpdate(reservation: Reservation) {
    this.reservationService.update(this.reservationID, reservation)

    this.onReservationOverview();
  }
  
  isAvailable(roomType: PickableRoomType): boolean {
    if (this.roomTypesPickable.find(avRoomType => avRoomType.room_type.id == roomType.room_type.id && avRoomType.overcrowding == roomType.overcrowding))
      return true;
    return false;
  }

  addRoomType(): void {
    this.rooms.push({room_type: this.roomTypesPickable[0].room_type, count: 1, overcrowding: this.roomTypesPickable[0].overcrowding})
    this.filterPickableRoomTypes();
  }

  removeRoomTypeAtIndex(index: number): void {
    this.rooms.splice(index, 1);
    this.filterPickableRoomTypes();
  }
  
  changeRoomTypeAtIndex(roomType: PickableRoomType, index: number) {
    this.rooms[index].room_type = roomType.room_type;
    this.rooms[index].overcrowding = roomType.overcrowding;
    this.filterPickableRoomTypes();
  }

  changeCountAtIndex(count: number, index: number) {
    console.log(count)
    //this.rooms[index].count = count;
    this.rooms[index].count = count;
  }
  
  onReservationOverview() {
    this.router.navigate(['/reservation']);
  }

  get arrivalDate() { return this.reservationForm.get('arrivalDate'); }
  get departureDate() { return this.reservationForm.get('departureDate'); }
  get firstName() { return this.reservationForm.get('firstName'); }
  get lastName() { return this.reservationForm.get('lastName'); }
  get telefonnumber() { return this.reservationForm.get('telefonnumber'); }
  get email() { return this.reservationForm.get('email'); }
  get address() { return this.reservationForm.get('address'); }
  get plz() { return this.reservationForm.get('plz'); }
  get city() { return this.reservationForm.get('city'); }
  get discount() { return this.reservationForm.get('discount'); }
  get creditCardHolder() { return this.reservationForm.get('creditCardHolder'); }
  get creditCardNumber() { return this.reservationForm.get('creditCardNumber'); }
  get creditCardValidUntilMonth() { return this.reservationForm.get('creditCardValidUntilMonth'); }
  get creditCardValidUntilYear() { return this.reservationForm.get('creditCardValidUntilYear'); }
  get creditCardVerificationNumber() { return this.reservationForm.get('creditCardVerificationNumber'); }

  onKeyupEventChangeName(event: any){
    this.creditCardHolder.setValue(this.firstName.value + " " + this.lastName.value);
  }

  onKeyupEventChangeCreditCardNumber(event: KeyboardEvent){
    if (event.key == "Backspace")
      return;
    var helper:String = this.creditCardNumber.value.replace(new RegExp(/\D/g), '')
    if (helper.length >= 16)
      this.creditCardNumber.setValue(helper.substring(0,4) + " " + helper.substring(4,8) + " " + helper.substring(8,12) + " " + helper.substring(12,16))
    else if (helper.length >= 12)
      this.creditCardNumber.setValue(helper.substring(0,4) + " " + helper.substring(4,8) + " " + helper.substring(8,12) + " " + helper.substring(12))
    else if (helper.length >= 8)
      this.creditCardNumber.setValue(helper.substring(0,4) + " " + helper.substring(4,8) + " " + helper.substring(8))
    else if (helper.length >= 4)
      this.creditCardNumber.setValue(helper.substring(0,4) + " " + helper.substring(4))
    else
    this.creditCardNumber.setValue(helper)
  }

  diffDays(date1: Date, date2: Date) : number {
    return Math.ceil(Math.abs(date1.getTime() - date2.getTime()) / (1000 * 3600 * 24));
  }

  checkMinDateValidator(checkDate: Date): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      const forbidden = checkDate >= new Date(control.value);
      return forbidden ? {minDate: {value: control.value}} : null;
    };
  }

  checkDepartureDateAfterArrivalValidator(formGroup: FormGroup, formName: string): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      const forbidden = new Date(formGroup.controls[formName].value) >= new Date(control.value);
      return forbidden ? {depAfterArr: {value: control.value}} : null;
    };
  }

  checkOnlyNumbers(): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      var forbidden: boolean = control.value.match(new RegExp(/\D/g)) != null;
      return forbidden ? {onlyNumbers: {value: control.value}} : null;
    };
  }

}
