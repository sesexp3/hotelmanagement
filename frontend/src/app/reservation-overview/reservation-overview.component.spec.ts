import { CurrencyPipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AddressPipe } from '../pipes/address.pipe';
import { DiscountPipe } from '../pipes/discount.pipe';
import { RoomReservationPipe } from '../pipes/room-reservation.pipe';
import { StayDurationPipe } from '../pipes/stay-duration.pipe';

import { ReservationOverviewComponent } from './reservation-overview.component';

describe('ReservationOverviewComponent', () => {
  let component: ReservationOverviewComponent;
  let fixture: ComponentFixture<ReservationOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservationOverviewComponent, AddressPipe, RoomReservationPipe, StayDurationPipe, CurrencyPipe, DiscountPipe ],
      imports: [RouterTestingModule, HttpClientModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservationOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have create reservation button', () => {
    const fixture = TestBed.createComponent(ReservationOverviewComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('#createReservationBtn').textContent).toContain('Neue Reservierung');
  });

  it('should have search field', () => {
    const fixture = TestBed.createComponent(ReservationOverviewComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('#searchField').placeholder).toContain('Suche');
  });

  it('should have reservation title', () => {
    const fixture = TestBed.createComponent(ReservationOverviewComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.h2').textContent).toContain('Reservierungen');
  });

});
