import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Reservation } from '../state/reservation.model';

@Component({
  selector: 'app-reservation-create-modal',
  templateUrl: './reservation-create-modal.component.html',
  styleUrls: ['./reservation-create-modal.component.scss']
})
export class ReservationCreateModalComponent implements OnInit {

  reservation: Reservation;
  callback: Function;
  arrivalDate: string;
  departureDate: string;
  firstName: string;
  lastName: string;

  constructor(
    public activeModal: NgbActiveModal,
  ) { }

  ngOnInit(): void {
    
  }

  onReserve() {
    this.callback(this.reservation);
    this.activeModal.close();
  }

}
