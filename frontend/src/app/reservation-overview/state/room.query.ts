import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { RoomTypeStore, RoomTypeState } from './room.store';

@Injectable({ providedIn: 'root' })
export class RoomTypeQuery extends QueryEntity<RoomTypeState> {

  UIState$ = this.select(store => store.ui.state);

  constructor(protected store: RoomTypeStore) {
    super(store);
  }

}
