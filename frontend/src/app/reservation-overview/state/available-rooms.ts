import {Room} from './available-rooms.model';

export const AVAILABLE_ROOMS: Room[] = [
                    {room_type: "EZ", price: 50},
                    {room_type: "DZ", price: 100},
                    {room_type: "DZ Comfort", price: 150},
                    {room_type: "Suit", price: 200}, 
    ];