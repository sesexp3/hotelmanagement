import { Injectable } from '@angular/core';
import { RoomType } from './reservation.model';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';

export enum UI_STATE {
  LOADING = 'LOADING',
  DEFAULT = 'DEFAULT',
  ERROR_BAD_SERVER = 'ERROR_BAD_SERVER',
  DELETION_SUCCESS = 'DELETION_SUCCESS',
  DELETION_ERROR = 'DELETION_ERROR',
  ADD_SUCCESS = 'ADD_SUCCESS',
  ADD_ERROR = 'ADD_ERROR',
  UPDATE_SUCCESS = 'UPDATE_SUCCESS',
  UPDATE_ERROR = 'UPDATE_ERROR',
}

export interface RoomTypeState extends EntityState<RoomType> {
  ui: {
    state: UI_STATE,
  },
  remaining_retry: number;
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'room' })
export class RoomTypeStore extends EntityStore<RoomTypeState> {

  constructor() {
    super({
      ui: {
        state: UI_STATE.DEFAULT
      },
      remaining_retry: 0
    });
  }

}

