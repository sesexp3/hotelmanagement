import { Injectable } from '@angular/core';
import { Reservation } from './reservation.model';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';

export enum UI_STATE {
  LOADING = 'LOADING',
  DEFAULT = 'DEFAULT',
  ERROR_BAD_SERVER = 'ERROR_BAD_SERVER',
  DELETION_SUCCESS = 'DELETION_SUCCESS',
  DELETION_ERROR = 'DELETION_ERROR',
  RESERVATION_VALID = 'RESERVATION_VALID',
  RESERVATION_NOT_VALID = 'RESERVATION_NOT_VALID',
  RESERVATION_SUCCESS = 'RESERVATION_SUCCESS',
  RESERVATION_ERROR = 'RESERVATION_ERROR',
}

export interface ReservationState extends EntityState<Reservation> {
  ui: {
    state: UI_STATE,
  },
  remaining_retry: number;
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'reservation' })
export class ReservationStore extends EntityStore<ReservationState> {

  constructor() {
    super({
      ui: {
        state: UI_STATE.DEFAULT
      },
      remaining_retry: 0
    });
  }

}

