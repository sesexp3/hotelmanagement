import { RoomReservationPipe } from 'src/app/pipes/room-reservation.pipe';

export interface Room {
    room_type: string; // Type of room. e.g. EZ, DZ
    price: number; // price of the room

  }

export function getCopy(room: Room): Room {
  return (JSON.parse(JSON.stringify(room)));
}

export function getCopyArray(roomArray: Room[]): Room[] {
  let ret: Room[] = [];
  roomArray.forEach(room => ret.push(getCopy(room)));
  return ret;
}