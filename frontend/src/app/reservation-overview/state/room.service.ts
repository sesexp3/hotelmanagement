import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { action, ID } from '@datorama/akita';
import { of, throwError } from 'rxjs';
import { catchError, delay, mergeMap, retryWhen, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { RoomType } from './reservation.model';
import { RoomTypeStore, UI_STATE } from './room.store';

@Injectable({
  providedIn: 'root'
})
export class RoomService {

  constructor(
    private roomStore: RoomTypeStore,
    private http: HttpClient) { 
    }

    MAX_RETRIES = 3;

    get() {
      let retries = this.MAX_RETRIES;
      this.updateUIState(UI_STATE.LOADING);
      
      this.http.get<RoomType[]>(`${environment.apiUrl}/room/type`).pipe(
        retryWhen((errors) => errors.pipe(
          delay(1000),
          tap(() => {
            retries--;
            this.roomStore.update({
              remaining_retry: retries
            });
          }),
          mergeMap(error => (retries > 0) ? of(error) : throwError(`API not reachable. Remaining attempts: ${retries}`)),
        )),
        tap(entities => {
          this.roomStore.set(entities);
          this.updateUIState(UI_STATE.DEFAULT);
        }),
        catchError(error => {
          this.updateUIState(UI_STATE.ERROR_BAD_SERVER);
          return of([]);
        }),
      ).subscribe();
    }

    add(room: RoomType) {
      this.http.post<RoomType[]>(`${environment.apiUrl}/room/type/`, room).pipe(
        tap(response => {
          this.updateUIState(UI_STATE.ADD_SUCCESS);
          console.log("change Status!: " + UI_STATE.ADD_SUCCESS);
        }),
        catchError(error => {
          this.updateUIState(UI_STATE.ADD_ERROR);
          console.log("change Status!: " + UI_STATE.ADD_ERROR);
          return of(error);
        }),
      ).subscribe();
      this.roomStore.add(room);
    }
  
    update(id, room: Partial<RoomType>) {
      this.http.put<RoomType[]>(`${environment.apiUrl}/room/type/${id}`, room).pipe(
        tap(response => {
          this.updateUIState(UI_STATE.UPDATE_SUCCESS);
          console.log("change Status!: " + UI_STATE.UPDATE_SUCCESS);
        }),
        catchError(error => {
          this.updateUIState(UI_STATE.UPDATE_ERROR);
          console.log("change Status!: " + UI_STATE.UPDATE_ERROR);
          return of(error);
        }),
      ).subscribe();
      this.roomStore.update(id, room);
    }
  
    remove(id: ID) {
      this.http.delete<RoomType[]>(`${environment.apiUrl}/room/type/${id}`).pipe(
        tap(response => {
          this.roomStore.remove(id);
          this.updateUIState(UI_STATE.DELETION_SUCCESS);
        }),
        catchError(error => {
          this.updateUIState(UI_STATE.DELETION_ERROR);
          return of(error);
        }),
      ).subscribe();
    }
    
  @action('Update ui state')
  updateUIState(state: UI_STATE) {
    this.roomStore.update({
      ui: {
        state: state,
      }
    });
  }
}
