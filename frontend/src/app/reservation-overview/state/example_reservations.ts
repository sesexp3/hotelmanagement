import { Reservation } from './reservation.model';

const today: Date = new Date();
let inTwoDays: Date = new Date();
inTwoDays.setDate(today.getDate() + 2);
let inFiveDays: Date = new Date();
inFiveDays.setDate(today.getDate() + 5);
export const RESERVATIONS: Reservation[] = [
  {
    id: 1,
    customer: "Max Mistermann",
    customerId: "",
    firstName: "Max",
    lastName: "Mistermann",
    tel: "0123456789",
    email: "max@mistermann.at",
    address: "Musterstrasse",
    postcode: "1000",
    city: "Vienna",
    rooms: [{
      count: 2,
      room_type: {
        id: 0,
        name: "EZ",
        priceBase: 500,
        priceOvercrowding: 500,
        quantity: 3,
        quantityOvercrowding: 2,
      },
      overcrowding: false,
    },
    ],
    arrival_date: today,
    departure_date: inTwoDays,
    num_stays_days: 2,
    price: 23.23,
    discount: 2,
    paymentInformation: {
      creditCardHolder: "Max Mistermann",
      creditCardNumber: "1234 1234 1234 1234",
      creditCardValidUntilMonth: "12",
      creditCardValidUntilYear: "22",
      creditCardVerificationNumber: "657"
    }
  },
  {
    id: 2,
    customer: "Max Mastermann",
    customerId: "",
    firstName: "Max",
    lastName: "Mastermann",
    tel: "0987654321",
    email: "max@Mastermann.at",
    address: "Musterstrasse 2",
    postcode: "1001",
    city: "Vienna",
    rooms: [
      {
        count: 2,
        room_type: {
          id: 0,
          name: "EZ",
          priceBase: 500,
          priceOvercrowding: 500,
          quantity: 3,
          quantityOvercrowding: 2,
        },
        overcrowding: false,
      },
      {
        count: 1,
        room_type: {
          id: 0,
          name: "Suite",
          priceBase: 500,
          priceOvercrowding: 500,
          quantity: 3,
          quantityOvercrowding: 2,
        },
        overcrowding: false,
      },
      {
        count: 3,
        room_type: {
          id: 0,
          name: "DZ",
          priceBase: 500,
          priceOvercrowding: 500,
          quantity: 3,
          quantityOvercrowding: 2,
        },
        overcrowding: false,
      },
    ],
    arrival_date: today,
    departure_date: inFiveDays,
    num_stays_days: 5,
    price: 1223.23,
    discount: 40,
    paymentInformation: {
      creditCardHolder: "Max Mastermann",
      creditCardNumber: "4321 4321 4321 4321",
      creditCardValidUntilMonth: "01",
      creditCardValidUntilYear: "26",
      creditCardVerificationNumber: "123"
    }
  }
]