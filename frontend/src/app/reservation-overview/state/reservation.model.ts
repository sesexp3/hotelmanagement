import { StringMap } from '@angular/compiler/src/compiler_facade_interface';

export interface RoomType {
  id: number;
  name: string;
  priceBase: number;
  priceOvercrowding: number;
  quantity: number;
  quantityOvercrowding: number;
}

export interface PickableRoomType {
  room_type: RoomType;
  overcrowding: boolean;
}

export interface RoomReservation {
  count: number; // Number of reserved rooms
  room_type: RoomType; // Type of room. e.g. EZ, DZ
  overcrowding: boolean;
}

export interface PaymentInformation {
  creditCardNumber: string;
  creditCardHolder: string;
  creditCardValidUntilMonth: string;
  creditCardValidUntilYear: string;
  creditCardVerificationNumber: string;
}

export interface Reservation {
  id: number | string; // Id of reservation
  customer: string; // Pre + Sur name of customer
  customerId: string; // Customer id
  firstName: string; // first name of customer
  lastName: string; // last name of customer
  email: string; // emailaddress of customer
  tel: string; // telefonnumber of customer
  address: string;  // Address of customer
  postcode: string; // Postcode of customer
  city: string; // City of customer
  rooms: RoomReservation[];  // Room reservations made by the customer 
  arrival_date: Date; // Arrival date of the customer
  departure_date: Date; // Departure date of the customer
  num_stays_days: number;  // Number of days at the hotel
  price: number;  // Price in €
  discount: number; // Discount in percentage.
  paymentInformation: PaymentInformation; //Payment information for the reservation
}

export function createReservation(params: Partial<Reservation>) {
  return {

  } as Reservation;
}
