import { Injectable } from '@angular/core';
import { action, ID } from '@datorama/akita';
import { HttpClient } from '@angular/common/http';
import { ReservationStore, UI_STATE } from './reservation.store';
import { Reservation, RoomReservation } from './reservation.model';
import { catchError, delay, mergeMap, retryWhen, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Observable, of, throwError } from 'rxjs';
import { RESERVATIONS } from './example_reservations';

@Injectable({ providedIn: 'root' })
export class ReservationService {

  constructor(
    private reservationStore: ReservationStore,
    private http: HttpClient) {
  }

  MAX_RETRIES = 3;

  get() {
    let retries = this.MAX_RETRIES;
    this.updateUIState(UI_STATE.LOADING);
    
    this.http.get<Reservation[]>(`${environment.apiUrl}/reservation/`).pipe(
      retryWhen((errors) => errors.pipe(
        delay(1000),
        tap(() => {
          retries--;
          this.reservationStore.update({
            remaining_retry: retries
          });
        }),
        mergeMap(error => (retries > 0) ? of(error) : throwError(`API not reachable. Remaining attempts: ${retries}`)),
      )),
      tap(entities => {
        this.reservationStore.set(entities);
        this.updateUIState(UI_STATE.DEFAULT);
      }),
      catchError(error => {
        this.updateUIState(UI_STATE.ERROR_BAD_SERVER);
        return of([]);
      }),
    ).subscribe();
  }

  check_rooms(reservation: Reservation, callback: Function) {
    this.updateUIState(UI_STATE.LOADING);
    console.log("change Status!: LOADING");
    this.http.post<Reservation>(`${environment.apiUrl}/reservation/check/`, reservation).pipe(
      tap(response => {
        callback(reservation, response);
      }),
      catchError(error => {
        console.log(error)
        this.updateUIState(UI_STATE.RESERVATION_ERROR);
        console.log("change Status!: RESERVATION_ERROR");
        return of(error);
      }),
    ).subscribe();
  }

  add(reservation: Reservation) {
    this.http.post<RoomReservation[]>(`${environment.apiUrl}/reservation/`, reservation).pipe(
      tap(response => {
        this.updateUIState(UI_STATE.RESERVATION_SUCCESS);
        console.log("change Status!: RESERVATION_SUCCESS");
      }),
      catchError(error => {
        this.updateUIState(UI_STATE.RESERVATION_ERROR);
        console.log("change Status!: RESERVATION_ERROR");
        return of(error);
      }),
    ).subscribe();
    this.reservationStore.add(reservation);
  }

  update(id, reservation: Partial<Reservation>) {
    this.http.put<RoomReservation[]>(`${environment.apiUrl}/reservation/${id}`, reservation).pipe(
      tap(response => {
        this.updateUIState(UI_STATE.RESERVATION_SUCCESS);
        console.log("change Status!: RESERVATION_SUCCESS");
      }),
      catchError(error => {
        this.updateUIState(UI_STATE.RESERVATION_ERROR);
        console.log("change Status!: RESERVATION_ERROR");
        return of(error);
      }),
    ).subscribe();
    this.reservationStore.update(id, reservation);
  }

  remove(id: ID) {
    this.http.delete<Reservation[]>(`${environment.apiUrl}/reservation/${id}`).pipe(
      tap(response => {
        this.reservationStore.remove(id);
        this.updateUIState(UI_STATE.DELETION_SUCCESS);
      }),
      catchError(error => {
        this.updateUIState(UI_STATE.DELETION_ERROR);
        return of(error);
      }),
    ).subscribe();
  }

  @action('Update ui state')
  updateUIState(state: UI_STATE) {
    this.reservationStore.update({
      ui: {
        state: state,
      }
    });
  }
}
