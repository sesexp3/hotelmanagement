import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { ReservationStore, ReservationState } from './reservation.store';

@Injectable({ providedIn: 'root' })
export class ReservationQuery extends QueryEntity<ReservationState> {

  UIState$ = this.select(store => store.ui.state);

  constructor(protected store: ReservationStore) {
    super(store);
  }

}
