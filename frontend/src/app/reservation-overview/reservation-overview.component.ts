import { Component, Directive, EventEmitter, Input, OnInit, Output, QueryList, ViewChildren } from '@angular/core';
import { Reservation } from './state/reservation.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ReservationDeleteModalComponent } from './reservation-delete-modal/reservation-delete-modal.component';
import { FormControl } from '@angular/forms';
import { map, tap } from 'rxjs/operators';
import { combineLatest, Observable, Subscription } from 'rxjs';
import { DiscountPipe } from '../pipes/discount.pipe';
import { AddressPipe } from '../pipes/address.pipe';
import { DatePipe } from '@angular/common';
import { StayDurationPipe } from '../pipes/stay-duration.pipe';
import { CurrencyPipe } from '../pipes/currency.pipe';
import { ReservationQuery } from './state/reservation.query';
import { ReservationService } from './state/reservation.service';
import { RoomReservationPipe } from '../pipes/room-reservation.pipe';
import { ActivatedRoute, Router } from '@angular/router';
import { UI_STATE } from './state/reservation.store';
import { TokenStorageService } from '../auth/token-storage.service';
import { RESERVATIONS } from './state/example_reservations';
import { BillingService } from '../billingmanagement/billing.service';

export type SortColumn = keyof Reservation | '';
export type SortDirection = 'asc' | 'desc' | '';
const rotate: { [key: string]: SortDirection } = { 'asc': 'desc', 'desc': '', '': 'asc' };

const compare = (v1: string | number, v2: string | number) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;

export interface SortEvent {
  column: SortColumn;
  direction: SortDirection;
}
@Directive({
  selector: 'th[sortable]',
  host: {
    '[class.asc]': 'direction === "asc"',
    '[class.desc]': 'direction === "desc"',
    '(click)': 'rotate()'
  }
})
export class NgbdSortableHeader {

  @Input() sortable: SortColumn = '';
  @Input() direction: SortDirection = '';
  @Output() sort = new EventEmitter<SortEvent>();

  rotate() {
    this.direction = rotate[this.direction];
    this.sort.emit({ column: this.sortable, direction: this.direction });
  }
}

@Component({
  selector: 'app-reservation-overview',
  templateUrl: './reservation-overview.component.html',
  styleUrls: ['./reservation-overview.component.scss'],
  providers: [DiscountPipe, AddressPipe, StayDurationPipe, CurrencyPipe, DatePipe, RoomReservationPipe]
})
export class ReservationOverviewComponent implements OnInit {

  reservations: Reservation[] = [];
  unsortedReservations = [];
  uiState$: Observable<UI_STATE>;
  expandedSearch = false;

  UI_STATE = UI_STATE;

  searchTerm = new FormControl('');
  dateBeginEnabled = new FormControl(false);
  dateEndEnabled = new FormControl(false);
  dateFilterBegin = new FormControl(
    new Date().toISOString().substring(0, 10)
  );
  dateFilterEnd = new FormControl(
    new Date().toISOString().substring(0, 10)
  );
  dateSearchInvalid: boolean = false;

  @ViewChildren(NgbdSortableHeader) 
  headers: QueryList<NgbdSortableHeader>;

  onSort({ column, direction }: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    // sorting reservations
    if (direction === '' || column === '') {
      this.reservations = this.unsortedReservations;
    } else {
      this.reservations = [...this.unsortedReservations].sort((a, b) => {
        const res = compare(a[column].toString(), b[column].toString());
        return direction === 'asc' ? res : -res;
      });
    }
  }

  searchTermSubscription: Subscription;

  constructor(
    private modalService: NgbModal,
    private router: Router,
    private route: ActivatedRoute,
    private reservationService: ReservationService,
    private discountPipe: DiscountPipe,
    private addressPipe: AddressPipe,
    private currencyPipe: CurrencyPipe,
    private stayDurationPipe: StayDurationPipe,
    private datePipe: DatePipe,
    private reservationQuery: ReservationQuery,
    private roomReservationPipe: RoomReservationPipe,
    private tokenStorage: TokenStorageService,
    private billingService: BillingService
  ) {
    this.reservationQuery.selectAll().subscribe(
      (reservations) => {
        this.unsortedReservations = reservations;
        this.reservations = reservations;
      }
    );
    this.uiState$ = reservationQuery.UIState$;
    this.searchTermSubscription = this.searchTerm.valueChanges.pipe(
    ).subscribe((value) => {
      if (!this.expandedSearch) {
        this.filterDateSpan(null, null)
      } else {
        this.onDateFilterChanged()
      }
    });
  }

  onDateFilterChanged() {
    const beginValue = new Date(this.dateFilterBegin.value);
    const endValue = new Date(this.dateFilterEnd.value);
    const beginEnabled = this.dateBeginEnabled.value;
    const endEnabled = this.dateEndEnabled.value;
    if (beginValue <= endValue ||  !(beginEnabled && endEnabled)) {
      this.dateSearchInvalid = false;
      this.filterDateSpan(
        (beginEnabled) ? beginValue : null,
        (endEnabled) ? endValue : null
      )
    } else {
      // Departure date is before arrival date. Show error
      if (beginEnabled && endEnabled) {
        this.dateSearchInvalid = true;
      }
    }
  }

  private filterDateSpan(begin: Date, end: Date) {
    const beginEnabled = (begin !== null) ? true : false;
    const endEnabled = (end !== null) ? true : false;

    const rs = this.search(
      this.searchTerm.value, 
      this.discountPipe, 
      this.addressPipe, 
      this.currencyPipe, 
      this.stayDurationPipe, 
      this.datePipe,
      this.roomReservationPipe
    )

    this.reservations = rs.filter((reservation: Reservation) => {
      if (beginEnabled) {
        if (begin > new Date(reservation.arrival_date)) {
          return false;
        }
      }

      if (endEnabled) {
        if (end < new Date(reservation.departure_date)) {
          return false;
        }
      }
      return true;
    })
  }

  ngOnInit(): void {
    if (!this.tokenStorage.isLoggedIn()) {
      this.router.navigate(['']);
    }

    setTimeout(() => this.loadData(), 1000);
  }

  loadData(): void {
    this.reservationService.get();
  }

  ngOnDestroy(): void {
    this.searchTermSubscription.unsubscribe();
  }

  onToggleExpandedSearch(): void {
    this.expandedSearch = !this.expandedSearch;
  }

  search(
    text: string,
    discountPipe: DiscountPipe,
    addressPipe: AddressPipe,
    currencyPipe: CurrencyPipe,
    stayDurationPipe: StayDurationPipe,
    datePipe: DatePipe,
    roomReservationPipe: RoomReservationPipe
    )
    : Reservation[] {
    return this.unsortedReservations.filter(reservation => {
      const searchTerm = text.toLowerCase();
      // Show only entries, which satisfy following conditions:
      return reservation?.customer?.toLowerCase()?.includes(searchTerm)
        || addressPipe.transform(reservation)?.toLowerCase()?.includes(searchTerm)
        || roomReservationPipe.transform(reservation.rooms)?.toLowerCase()?.includes(searchTerm) 
        || reservation.num_stays_days?.toString()?.includes(searchTerm)
        || datePipe.transform(reservation.arrival_date, 'shortDate')?.toLowerCase()?.includes(searchTerm)
        || stayDurationPipe.transform(reservation.num_stays_days)?.toLowerCase()?.includes(searchTerm)
        || currencyPipe.transform(reservation.price).toString()?.includes(searchTerm)
        || discountPipe.transform(reservation.discount).toLowerCase()?.includes(searchTerm)
    })
  }

  onCreateReservation(): void {
    this.router.navigate(['./create'], {relativeTo: this.route});
  }

  onEditReservation(reservation: Reservation): void {
    this.router.navigate(['./'+ reservation.id], {relativeTo: this.route});
  }

  onRetry(): void {
    this.reservationService.get();
  }

  onDeleteReservation(reservation: Reservation): void {
    const modalRef = this.modalService.open(ReservationDeleteModalComponent);
    modalRef.componentInstance.reservation = reservation;
  }

  onCreateInvoice(reservation: Reservation) {
    this.billingService.createInvoice(reservation);
    setTimeout(()=>{this.router.navigate([`/billing`]);},650);
    //this.router.navigate([`/billing`]);
  }
}
