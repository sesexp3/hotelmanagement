import { Room } from '../../shared/models/Room';
import { EntityState, StoreConfig, EntityStore } from '@datorama/akita';
import { Injectable } from '@angular/core';

export enum UI_STATE {
    LOADING = 'LOADING',
    DEFAULT = 'DEFAULT',
    ERROR_BAD_SERVER = 'ERROR_BAD_SERVER',
    DELETION_SUCCESS = 'DELETION_SUCCESS',
    DELETION_ERROR = 'DELETION_ERROR',
    ROOM_VALID = 'ROOM_VALID',
    ROOM_NOT_VALID = 'ROOM_NOT_VALID',
    ROOM_SUCCESS = 'ROOM_SUCCESS',
    ROOM_ERROR = 'ROOM_ERROR',
  }

export interface RoomState extends EntityState<Room> {
    ui: {
        state: UI_STATE,
    };
    remaining_retry: number;
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'room' })
export class RoomStore extends EntityStore<RoomState> {

    constructor() {
      super({
        ui: {
          state: UI_STATE.DEFAULT
        },
        remaining_retry: 0
      });
    }
}
