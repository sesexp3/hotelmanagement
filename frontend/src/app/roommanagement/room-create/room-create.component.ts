import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UI_STATE } from '../state/room.store';
import { Router, ActivatedRoute } from '@angular/router';
import { TokenStorageService } from '../../auth/token-storage.service';
import { RoommanagementService } from '../roommanagement.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Room } from '../../shared/models/Room';
import { RoomQuery } from '../state/room.query';
import { RoomType } from 'src/app/shared/models/RoomType';
import { RoomService } from 'src/app/reservation-overview/state/room.service';
import { RoomTypeQuery } from 'src/app/reservation-overview/state/room.query';


@Component({
  selector: 'app-room-create',
  templateUrl: './room-create.component.html',
  styleUrls: ['./room-create.component.scss']
})
export class RoomCreateComponent implements OnInit {

  uiState$: Observable<UI_STATE>;

  UI_STATE = UI_STATE;

  roomForm: FormGroup;

  roomID: number;
  selectedRoomType: RoomType = {id: -1, name: "default", priceBase:-1, priceOvercrowding:-1, quantity:-1, quantityOvercrowding:-1};
  updateRoom: Room;
  invalidName = false;

  roomTypes: RoomType[] = [];
  
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private tokenStorageService: TokenStorageService,
    private roommanagementService: RoommanagementService,
    private roomQuery: RoomQuery,
    private roomTypeService: RoomService,
    private roomTypeQuery: RoomTypeQuery,

  ) {

    this.route.params.subscribe(params => {
      if ('id' in params) {
        this.roomID = params['id'];
      } else {
        this.roomID = -1;
      }
    });

    this.loadData();

    this.roomTypeQuery.selectAll().subscribe(
      (rooms) => {
        if (rooms.length <= 0) {
          console.log("No rooms available...");
        } else {
          this.roomTypes = rooms;
          this.selectedRoomType = this.roomTypes[0];
        }
      }
    );
   }

  ngOnInit(): void {
    if (!this.tokenStorageService.isLoggedIn()) {
      this.router.navigate(['']);
    }

    this.roomForm = new FormGroup({
      roomnumber: new FormControl('', ),
      roomCategory: new FormControl('', ),
      roomOccupancy: new FormControl('', ),
      roomOvercrowding: new FormControl('', )
    });

    if(this.roomID === -1) {
      // creating new room with default values
      //this.roomForm.controls['roomOccupancy'].setValue(1);
      this.changeRoomType(this.selectedRoomType);
      this.roomForm.controls['roomOvercrowding'].setValue(false);
    } else {
      //editing existing room
      this.roomQuery.selectEntity(this.roomID).subscribe( room => {
        console.log(room)
        if (!room) {
          this.onRoomOverview();
        }
        this.updateRoom = room;
        this.roomForm.controls['roomnumber'].setValue(this.updateRoom.name);
        this.roomForm.controls['roomOccupancy'].setValue(this.updateRoom.occupancy);
        this.selectedRoomType = this.updateRoom.roomType;
        this.roomForm.controls['roomOvercrowding'].setValue(this.updateRoom.overcrowding);
      });
    }
  }
  
  loadData(): void {
    this.roomTypeService.get();
  }

  onSaveRoom() {
    var room : Room;
    if (this.roomForm.get('roomnumber').value === "") {
      this.invalidName = true;
      return;
    }
    if (this.roomID === -1) {
      room = this.generateRoom();
      this.roommanagementService.add(room);
    } else {
      room = this.generateRoom(this.updateRoom);
      this.roommanagementService.update(this.roomID, room);
    }
    this.onRoomOverview();
  }

  printRoom(room: Room) {
    console.log("save room: ")
    console.log(room);
  }

  generateRoom(originalRoom?: Room) : Room {
    const room: Room = {
      id: originalRoom ? originalRoom.id : -1,
      name: this.roomForm.get('roomnumber').value,
      occupancy: this.roomForm.get('roomOccupancy').value,
      overcrowding: this.roomForm.get('roomOvercrowding').value,
      roomType: this.selectedRoomType
    };
    return room;
  }

  onRoomOverview() {
    this.router.navigate(['/rooms']);
  }

  changeRoomType(roomType: RoomType) {
    this.selectedRoomType = roomType;
    //console.log(this.selectedRoomType.name );
    if (this.selectedRoomType.name === "Einzelzimmer") {
      this.roomForm.controls['roomOccupancy'].setValue(1);
    } else {
      this.roomForm.controls['roomOccupancy'].setValue(2);
    }
  }

}
