import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Room } from 'src/app/shared/models/Room';
import { RoommanagementService } from '../roommanagement.service';

@Component({
  selector: 'app-room-delete',
  templateUrl: './room-delete.component.html',
  styleUrls: ['./room-delete.component.scss']
})
export class RoomDeleteComponent implements OnInit {

  room: Room;

  constructor(
    public activeModal: NgbActiveModal,
    private roomManagementService: RoommanagementService
  ) { }

  ngOnInit(): void {
  }

  onDelete() {
    this.roomManagementService.remove(this.room.id);
    this.activeModal.close();
  }

}
