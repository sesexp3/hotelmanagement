import { Component, Directive, EventEmitter, Input, OnInit, Output, QueryList, ViewChildren } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { TokenStorageService } from 'src/app/auth/token-storage.service';
import { BooleanPipe } from 'src/app/pipes/boolean.pipe';
import { RoomTypePipe } from 'src/app/pipes/room-type.pipe';
import { Room } from 'src/app/shared/models/Room';
import { RoomDeleteComponent } from '../room-delete/room-delete.component';
import { RoommanagementService } from '../roommanagement.service';
import { RoomQuery } from '../state/room.query';

export type SortColumn = keyof Room | '';
export type SortDirection = 'asc' | 'desc' | '';
const rotate: { [key: string]: SortDirection } = { 'asc': 'desc', 'desc': '', '': 'asc' };
const compare = (v1: string | number, v2: string | number) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;

export interface SortEvent {
  column: SortColumn;
  direction: SortDirection;
}
@Directive({
  selector: 'th[sortable]',
  host: {
    '[class.asc]': 'direction === "asc"',
    '[class.desc]': 'direction === "desc"',
    '(click)': 'rotate()'
  }
})
export class NgbdSortableHeader {

  @Input() sortable: SortColumn = '';
  @Input() direction: SortDirection = '';
  @Output() sort = new EventEmitter<SortEvent>();

  rotate() {
    this.direction = rotate[this.direction];
    this.sort.emit({ column: this.sortable, direction: this.direction });
  }
}

@Component({
  selector: 'app-room-overview',
  templateUrl: './room-overview.component.html',
  styleUrls: ['./room-overview.component.scss'],
  providers: [RoomTypePipe, BooleanPipe]
})
export class RoomOverviewComponent implements OnInit {

  searchFormControl = new FormControl('');


  @ViewChildren(NgbdSortableHeader)
  headers: QueryList<NgbdSortableHeader>;

  onSort({ column, direction }: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    // sorting reservations
    if (direction === '' || column === '') {
      this.rooms = this.unsortedRooms;
    } else {
      this.rooms = [...this.unsortedRooms].sort((a, b) => {
        const res = compare(a[column].toString(), b[column].toString());
        return direction === 'asc' ? res : -res;
      });
    }
  }

  rooms = []
  unsortedRooms = [];

  searchTermSubscription: Subscription;

  constructor(
    private roomQuery: RoomQuery,
    private modalService: NgbModal,
    private router: Router,
    private route: ActivatedRoute,
    private roomService: RoommanagementService,
    private tokenStorage: TokenStorageService,
    private roomTypePipe: RoomTypePipe,
    private booleanPipe: BooleanPipe
  ) {
    this
      .searchFormControl
      .valueChanges
      .subscribe((searchTerm) => {
        this.rooms = this.search(
          searchTerm,
          roomTypePipe,
          booleanPipe
        )
      })

    this.roomQuery.selectAll().subscribe(
      (rooms) => {
        //alert(JSON.stringify(rooms));
        console.log(rooms);
        this.unsortedRooms = rooms;
        this.rooms = rooms;
      }
    )
  }

  search(
    text: string,
    roomTypePipe: RoomTypePipe,
    booleanPipe: BooleanPipe,
  )
    : Room[] {
    return this.unsortedRooms.filter(
      (room: Room) => {
        const searchTerm = text.toLowerCase();
        // Show only entries, which satisfy following conditions:
        return room.id.toString().toLowerCase().includes(searchTerm)
          || roomTypePipe.transform(room?.roomType).toLowerCase().includes(searchTerm)
          || room.name?.includes(searchTerm)
          || room.occupancy.toString().toLowerCase().includes(searchTerm)
          || booleanPipe.transform(room?.overcrowding).toLowerCase().includes(searchTerm);
      })
  }


  ngOnInit(): void {
    if (!this.tokenStorage.isLoggedIn()) {
      this.router.navigate(['']);
    }

    setTimeout(() => this.loadData(), 1000);
  }

  loadData(): void {
    this.roomService.get();
  }

  onCreate(): void {
    this.router.navigate(['./create'], { relativeTo: this.route });
  }

  onEdit(room: Room) {
    this.router.navigate(['./' + room.id], { relativeTo: this.route });
  }

  onDelete(room: Room) {
    const modalRef = this.modalService.open(RoomDeleteComponent);
    modalRef.componentInstance.room = room;
  }

}
