import { Injectable } from '@angular/core';
import { action, ID } from '@datorama/akita';
import { RoomStore, UI_STATE } from './state/room.store';
import { HttpClient } from '@angular/common/http';
import { catchError, delay, mergeMap, retryWhen, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { of, throwError } from 'rxjs';
import { Room } from '../shared/models/Room';

@Injectable({
  providedIn: 'root'
})
export class RoommanagementService {

  constructor(
    private roomStore: RoomStore,
    private http: HttpClient) {
  }

  MAX_RETRIES = 3;

  getRoomTypes() {
    
  }

  get() {
    let retries = this.MAX_RETRIES;
    this.updateUIState(UI_STATE.LOADING);
    
    this.http.get<Room[]>(`${environment.apiUrl}/room/`).pipe(
      retryWhen((errors) => errors.pipe(
        delay(1000),
        tap(() => {
          retries--;
          this.roomStore.update({
            remaining_retry: retries
          });
        }),
        mergeMap(error => (retries > 0) ? of(error) : throwError(`API not reachable. Remaining attempts: ${retries}`)),
      )),
      tap(entities => {
        this.roomStore.set(entities);
        this.updateUIState(UI_STATE.DEFAULT);
      }),
      catchError(error => {
        this.updateUIState(UI_STATE.ERROR_BAD_SERVER);
        return of([]);
      }),
    ).subscribe();
  }

  add(room: Room) {
    this.http.post<Room[]>(`${environment.apiUrl}/room/`, room).pipe(
      tap(response => {
        //console.error("need to check if valid or not, and get price!");
        this.updateUIState(UI_STATE.ROOM_SUCCESS);
        console.log("change Status!: ROOM_SUCCESS");
      }),
      catchError(error => {
        this.updateUIState(UI_STATE.ROOM_ERROR);
        console.log("change Status!: ROOM_ERROR");
        return of(error);
      }),
    ).subscribe();
  }

  update(id, room: Partial<Room>) {
    this.http.put<Room[]>(`${environment.apiUrl}/room/${id}`, room).pipe(
      tap(response => {
        //console.error("need to check if valid or not, and get price!");
        this.updateUIState(UI_STATE.ROOM_SUCCESS);
        console.log("change Status!: ROOM_SUCCESS");
      }),
      catchError(error => {
        this.updateUIState(UI_STATE.ROOM_ERROR);
        console.log("change Status!: ROOM_ERROR");
        return of(error);
      }),
    ).subscribe();
    this.roomStore.update(id, room);
  }

  remove(id: ID) {
    this.http.delete<Room[]>(`${environment.apiUrl}/room/${id}`).pipe(
      tap(response => {
        this.roomStore.remove(id);
        this.updateUIState(UI_STATE.DELETION_SUCCESS);
      }),
      catchError(error => {
        this.updateUIState(UI_STATE.DELETION_ERROR);
        return of(error);
      }),
    ).subscribe();
  }

  @action('Update ui state')
  updateUIState(state: UI_STATE) {
    this.roomStore.update({
      ui: {
        state: state,
      }
    });
  }
}
