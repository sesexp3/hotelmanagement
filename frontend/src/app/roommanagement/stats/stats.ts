export interface Occupancy {
    date: string;
    occupancy: number;
}

export interface Stat {
    roomType: string;
    occupancies: Occupancy[];
}

export interface Stats {
    stats: Stat[];
}