import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Stats } from './stats';

@Injectable({
  providedIn: 'root'
})
export class StatsService {

  constructor(
    private http: HttpClient
  ) { }

  get() {
    return this.http.get<Stats>(`${environment.apiUrl}/reservation/stats`);
  }
}
