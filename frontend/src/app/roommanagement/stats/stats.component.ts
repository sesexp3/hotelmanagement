import { Component, OnInit } from '@angular/core';
import { StatsService } from './stats.service';
import { Stats } from './stats';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import * as pluginAnnotations from 'chartjs-plugin-annotation';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss']
})
export class StatsComponent implements OnInit {

  constructor(
    private statsService: StatsService
  ) { }

  public lineChartData: ChartDataSets[] = [];
  public lineChartLabels: Label[] = ['1','2','3'];
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
          ticks: {
            beginAtZero: true,
            stepSize: 1
          }
        },
      ]
    },
    annotation: {
    },
  };
  public lineChartLegend = true;
  public lineChartType: ChartType = 'line';
  public lineChartPlugins = [pluginAnnotations];

  stats: Stats;

  ngOnInit(): void {
    this.statsService.get().subscribe(
      result => {
        this.stats = result;
        let roomTypeStats = this.stats.stats;
        // y-axis rooms
        let chartData = [];
        let rooms = roomTypeStats.map(stat => stat.occupancies.map((occupancy) => occupancy.occupancy));
        roomTypeStats.forEach(stat => {
          let data = {
            data: stat.occupancies.map((occupancy) => occupancy?.occupancy),
            label: stat.roomType
          }
          chartData.push(data);
        });

        this.lineChartData = chartData;

        // x-axis dates
        let dates = roomTypeStats.map(stat => stat.occupancies.map(occupancy => occupancy.date))[0];
        this.lineChartLabels = [...dates];
        console.log(JSON.stringify(result));
      }
    )
  }


   // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

}
