import { Pipe, PipeTransform } from '@angular/core';
import { ID } from '@datorama/akita';

@Pipe({
  name: 'invoiceNumber'
})
export class InvoiceNumberPipe implements PipeTransform {

  transform(value: ID, ...args: unknown[]): string {
    return `${value}`;
  }

}
