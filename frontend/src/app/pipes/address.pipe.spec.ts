import { Reservation } from '../reservation-overview/state/reservation.model';
import { AddressPipe } from './address.pipe';

describe('AddressPipe', () => {
  it('create an instance', () => {
    const pipe = new AddressPipe();
    expect(pipe).toBeTruthy();
  });


  it('transforms example reservation to address', () => {
    const pipe = new AddressPipe();
    let address: Reservation = {
      id: 2,
      customer: "Max Mastermann",
      customerId: "",
      firstName: "Max",
      lastName: "Mastermann",
      tel: "0987654321",
      email: "max@Mastermann.at",
      address: "Musterstrasse 2",
      postcode: "1001",
      city: "Vienna",
      rooms: [
        {
          count: 2,
          room_type: {
            id: 0,
            name: "EZ",
            priceBase: 500,
            priceOvercrowding: 500,
            quantity: 3,
            quantityOvercrowding: 2,
          },
          overcrowding: false,
        },
        {
          count: 1,
          room_type: {
            id: 0,
            name: "Suite",
            priceBase: 500,
            priceOvercrowding: 500,
            quantity: 3,
            quantityOvercrowding: 2,
          },
          overcrowding: false,
        },
        {
          count: 3,
          room_type: {
            id: 0,
            name: "DZ",
            priceBase: 500,
            priceOvercrowding: 500,
            quantity: 3,
            quantityOvercrowding: 2,
          },
          overcrowding: false,
        },
      ],
      arrival_date: new Date(),
      departure_date: new Date(),
      num_stays_days: 5,
      price: 1223.23,
      discount: 40,
      paymentInformation: {
        creditCardHolder: "Max Mastermann",
        creditCardNumber: "4321 4321 4321 4321",
        creditCardValidUntilMonth: "01",
        creditCardValidUntilYear: "26",
        creditCardVerificationNumber: "123"
      }
    }
    const result = 'Musterstrasse 2, 1001, Vienna';
    expect(pipe.transform(address)).toEqual(result);
  });

});
