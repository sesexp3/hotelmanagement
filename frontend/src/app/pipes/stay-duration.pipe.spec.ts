import { StayDurationPipe } from './stay-duration.pipe';

describe('StayDurationPipe', () => {
  it('create an instance', () => {
    const pipe = new StayDurationPipe();
    expect(pipe).toBeTruthy();
  });

  it('transforms stay duration of 1 day ', () => {
    const pipe = new StayDurationPipe();
    expect(pipe.transform(1)).toBe("1 Nächtigung");
  });

  it('transforms stay duration of 4 days ', () => {
    const pipe = new StayDurationPipe();
    expect(pipe.transform(4)).toBe("4 Nächtigungen");
  });
});
