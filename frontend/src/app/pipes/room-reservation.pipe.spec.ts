import { RoomReservationPipe } from './room-reservation.pipe';

describe('RoomReservationPipe', () => {
  it('create an instance', () => {
    const pipe = new RoomReservationPipe();
    expect(pipe).toBeTruthy();
  });

  it('transforms reservation of 2x EZ ', () => {
    const roomReservation = [{
      count: 2,
      room_type: {
        id: 0,
        name: "EZ",
        priceBase: 500,
        priceOvercrowding: 500,
        quantity: 3,
        quantityOvercrowding: 2,
      },
      overcrowding: false,
    }];

    const result = '2x EZ';
    const pipe = new RoomReservationPipe();
    expect(pipe.transform(roomReservation)).toBe(result);
  });

  it('transforms reservation of 2x EZ, 1x Suite, 3x DZ ', () => {
    const roomReservation = [
      {
        count: 2,
        room_type: {
          id: 0,
          name: "EZ",
          priceBase: 500,
          priceOvercrowding: 500,
          quantity: 3,
          quantityOvercrowding: 2,
        },
        overcrowding: false,
      },
      {
        count: 1,
        room_type: {
          id: 0,
          name: "Suite",
          priceBase: 500,
          priceOvercrowding: 500,
          quantity: 3,
          quantityOvercrowding: 2,
        },
        overcrowding: false,
      },
      {
        count: 3,
        room_type: {
          id: 0,
          name: "DZ",
          priceBase: 500,
          priceOvercrowding: 500,
          quantity: 3,
          quantityOvercrowding: 2,
        },
        overcrowding: false,
      },
    ];

    const result = '2x EZ, 1x Suite, 3x DZ';
    const pipe = new RoomReservationPipe();
    expect(pipe.transform(roomReservation)).toBe(result);
  });
});
