import { Pipe, PipeTransform } from '@angular/core';
import { currentAction } from '@datorama/akita';
import { RoomReservation } from '../reservation-overview/state/reservation.model';

@Pipe({
  name: 'roomReservation'
})
export class RoomReservationPipe implements PipeTransform {

  transform(value: RoomReservation[], ...args: unknown[]): string {
    return value.map(room => `${room.count}x ${room.room_type.name}`).reduce((prev, cur) => prev + ', ' + cur);
  }

}
