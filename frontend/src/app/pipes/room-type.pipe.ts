import { Pipe, PipeTransform } from '@angular/core';
import { RoomType } from '../shared/models/RoomType';

@Pipe({
  name: 'roomType'
})
export class RoomTypePipe implements PipeTransform {

  transform(value: RoomType, ...args: unknown[]): string {
    return value.name;
  }

}
