import { Pipe, PipeTransform } from '@angular/core';
import { Reservation } from '../reservation-overview/state/reservation.model';

@Pipe({
  name: 'address'
})
export class AddressPipe implements PipeTransform {

  transform(value: Reservation, ...args: unknown[]): string {
    return value.address + ', ' + value.postcode + ', ' + value.city;
  }

}
