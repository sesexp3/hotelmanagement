import { DiscountPipe } from './discount.pipe';

describe('DiscountPipe', () => {
  it('create an instance', () => {
    const pipe = new DiscountPipe();
    expect(pipe).toBeTruthy();
  });

  it('transforms "93" to "93%"', () => {
    const pipe = new DiscountPipe();
    expect(pipe.transform(1)).toBe('1%');
  });

  it('transforms "21.02" to "21.02%"', () => {
    const pipe = new DiscountPipe();
    expect(pipe.transform(21.02)).toBe('21.02%');
  });
});
