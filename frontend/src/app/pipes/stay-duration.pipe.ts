import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stayDuration'
})
export class StayDurationPipe implements PipeTransform {

  transform(value: number, ...args: unknown[]): string {
    return (value === 1) ? `${value} Nächtigung`: `${value} Nächtigungen`;
  }

}
