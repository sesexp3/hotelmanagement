import { CurrencyPipe } from './currency.pipe';

describe('CurrencyPipe', () => {
  it('create an instance', () => {
    const pipe = new CurrencyPipe();
    expect(pipe).toBeTruthy();
  });

  it('transforms "1" to "1€"', () => {
    const pipe = new CurrencyPipe();
    expect(pipe.transform(1)).toBe('1€');
  });

  it('transforms "212.02" to "212.02€"', () => {
    const pipe = new CurrencyPipe();
    expect(pipe.transform(212.02)).toBe('212.02€');
  });
});
