import { User } from "../shared/models/User";

export class ReceitInfo {
  receitnumber: number;
  date: Date;
  user: User;

  constructor(receitnumber: number, date: Date, user: User) {
    this.receitnumber = receitnumber;
    this.date = date;
    this.user = user;
  }

  public toString = (): string => {
    return `ReceitInfo (receitnumber: ${this.receitnumber} date: ${this.date} user: ${this.user})`;
  }
}