import { Component, OnInit } from '@angular/core';
import { Receit } from 'src/app/shared/models/Receit';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BillingService } from '../../billing.service';


@Component({
  selector: 'app-billing-cancel-modal',
  templateUrl: './billing-cancel-modal.component.html',
  styleUrls: ['./billing-cancel-modal.component.scss']
})
export class BillingCancelModalComponent implements OnInit {

  receit: Receit
  constructor(
    public activeModal: NgbActiveModal,
    private billingService: BillingService
  ) { }
  

  ngOnInit(): void {
  }

  onCancel() {
    this.billingService.cancel(this.receit);
    this.activeModal.close();
  }
}
