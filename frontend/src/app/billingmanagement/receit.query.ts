import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { ReceitStore, ReceitState } from './receit.store';

@Injectable({ providedIn: 'root' })
export class ReceitQuery extends QueryEntity<ReceitState> {

  UIState$ = this.select(store => store.ui.state);

  constructor(protected store: ReceitStore) {
    super(store);
  }
}
