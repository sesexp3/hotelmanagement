import { Injectable } from '@angular/core';
import { Receit } from '../shared/models/Receit';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';

export enum UI_STATE {
  LOADING = 'LOADING',
  DEFAULT = 'DEFAULT',
  ERROR_BAD_SERVER = 'ERROR_BAD_SERVER',
  DELETION_SUCCESS = 'DELETION_SUCCESS',
  DELETION_ERROR = 'DELETION_ERROR',
  REVOKE_SUCCESS = 'REVOKE_SUCCESS',
  REVOKE_ERROR = 'REVOKE_ERROR'
}

export interface ReceitState extends EntityState<Receit> {
    ui: {
      state: UI_STATE,
    };
    remaining_retry: number;
  }

@Injectable({ providedIn: 'root' })
  @StoreConfig({ name: 'receit' })
  export class ReceitStore extends EntityStore<ReceitState> {
    constructor() {
      super({
        ui: {
          state: UI_STATE.DEFAULT
        },
        remaining_retry: 0
      });
    }
  }
