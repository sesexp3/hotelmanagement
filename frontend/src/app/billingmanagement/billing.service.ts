import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { action, ID } from '@datorama/akita';
import { catchError, delay, mergeMap, retryWhen, tap } from 'rxjs/operators';
import { of, throwError } from 'rxjs';
import { ReceitStore, UI_STATE } from './receit.store';
import { environment } from '../../environments/environment';
import { Receit } from '../shared/models/Receit';
import { Router } from '@angular/router';
import { Reservation } from '../reservation-overview/state/reservation.model';

@Injectable({
  providedIn: 'root'
})
export class BillingService {
  getPDF(invoiceNumber: number) {
    this.http.get(`${environment.apiUrl}/invoices/${invoiceNumber}/pdf`,{responseType: "text"}).subscribe(
      (pdfb64:string) => {
        var link = document.createElement('a');
  
        link.download = `invoice${invoiceNumber}.pdf`;
        link.href = 'data:application/octet-stream;base64,' + pdfb64;
        document.body.appendChild(link);
        link.click();
        //in the history of hacks, this is definitly highly ranked, but everything else failed 
      }
    )

  }

  MAX_RETRIES = 3;

  constructor(
    private receitStore: ReceitStore,
    private http: HttpClient,
    private router: Router) { }

  get() {
    let retries = this.MAX_RETRIES;
    this.updateUIState(UI_STATE.LOADING);
    this.http.get<Receit[]>(`${environment.apiUrl}/invoices/`).pipe(
      retryWhen((errors) => errors.pipe(
        delay(1000),
        tap(() => {
          retries--;
          this.receitStore.update({
            remaining_retry: retries
          });
        }),
        mergeMap(error => (retries > 0) ? of(error) : throwError(`API not reachable. Remaining attempts: ${retries}`)),
      )),
      catchError(error => {
        return of([]);
      }),
      tap(entities => {
        console.log(JSON.stringify(entities));
        this.receitStore.set(entities);
        this.updateUIState(UI_STATE.ERROR_BAD_SERVER);
      }),
    ).subscribe();
  }

  add(receit: Receit) {
    this.http.put<Receit>(`${environment.apiUrl}/invoices`,receit).subscribe(
      receit => {
        this.receitStore.add(receit);
        this.router.navigate([`/billing`]);
      },
      err => {
        console.log(err);
      })
  }

  createInvoice(reservation: Reservation) {
    this.http.put<Receit>(`${environment.apiUrl}/invoices/${reservation.id}`,reservation.id).subscribe(
      receit => {
        let modifiedReceit = receit;
        modifiedReceit.reservation = reservation;
        console.log(modifiedReceit)
        this.receitStore.add(modifiedReceit);
        //this.router.navigate([`/billing`]);
        //location.href = 'billing';
      },
      err => {
        console.log(err)
      })
  }

  delete(receit: Receit) {
    this.http.delete<Receit>(`${environment.apiUrl}/invoices/${receit.invoiceNumber}`).subscribe(
      receit => {
        this.receitStore.remove(receit);
        //TODO: this "refresh" doesn't work
        this.router.navigate([`/billing`]);
      },
      err => {
        console.log(err);
      })
    }
  cancel(receit:Receit) {
    //receit.invoiceCancellationDate=new Date();
    this.http.delete<Receit>(`${environment.apiUrl}/invoices/${receit.invoiceNumber}`).subscribe(
      response => {
        //console.log(response)
        //let updatedReceit = new Receit(response.id,response.invoiceNumber,response.grandTotal,
         // response.reservation,response.invoiceCancellationDate, response.cancelledFrom);
        //let receivedDate = response.invoiceCancellationDate;
        //updatedReceit.invoiceCancellationDate = receivedDate;
        //updatedReceit.cancelledFrom = response.cancelledFrom;
        //this.receitStore.update(updatedReceit);
        //console.log(updatedReceit)
        
        this.receitStore.update(response);
        location.reload();
        //this.router.navigate([`/billing`]);
        },
      err => {
        console.log(err)
      })
  }
  

  @action('Update ui state')
  updateUIState(state: UI_STATE) {
    this.receitStore.update({
      ui: {
        state: state,
      }
    });
  }
}
