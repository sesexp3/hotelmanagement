import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BillingService } from '../billing.service';
import { Receit } from '../../shared/models/Receit';
import { TokenStorageService } from '../../auth/token-storage.service';
import { ReceitInfo } from '../receit-info'
import { Router } from '@angular/router';

@Component({
  selector: 'app-cancel-receit',
  templateUrl: './cancel-receit.component.html',
  styleUrls: ['./cancel-receit.component.scss']
})
export class CancelReceitComponent implements OnInit {

  receit: Receit;
  receitInfo: ReceitInfo;

  constructor(
    private billingService: BillingService,
    private tokenStorage: TokenStorageService,
    private router: Router
  ) { }

  ngOnInit(): void {
    if (!this.tokenStorage.isLoggedIn()) {
      this.router.navigate(['']);
    }
  }

  onSubmit(logForm: NgForm) {
/* TODO: needs to be adjusted, either only paper invoices or check in Backend if number exists
    this.billingService.add(new Receit(null,logForm.form.value.receitnumber, this.tokenStorage.getUserInfo().username,null));
    console.log(this.receitInfo);
    */
  }
}
