import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { CancelReceitComponent } from './cancel-receit.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('CancelReceitComponent', () => {
  let component: CancelReceitComponent;
  let fixture: ComponentFixture<CancelReceitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        CancelReceitComponent
       ],
       imports: [
         FormsModule,
         HttpClientModule,
         RouterTestingModule
       ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelReceitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
