import { Component, Directive, EventEmitter, Input, OnInit, Output, QueryList, ViewChildren } from '@angular/core';
import { faPrint,faTrash,faDownload,faEnvelope} from '@fortawesome/free-solid-svg-icons';
import { Observable, Subscription } from 'rxjs';
import { UI_STATE } from '../receit.store';
import { BillingService } from '../billing.service';
import { TokenStorageService } from '../../auth/token-storage.service';
import { Router } from '@angular/router';
import { ReceitQuery } from '../receit.query';
import { Receit } from 'src/app/shared/models/Receit';
import { FormControl } from '@angular/forms';
import { SortDirection } from '../../reservation-overview/reservation-overview.component';
import { DiscountPipe } from '../../pipes/discount.pipe';
import { AddressPipe } from '../../pipes/address.pipe';
import { DatePipe } from '@angular/common';
import { StayDurationPipe } from '../../pipes/stay-duration.pipe';
import { CurrencyPipe } from '../../pipes/currency.pipe';
import { RoomReservationPipe } from '../../pipes/room-reservation.pipe';
import { map } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BillingCancelModalComponent } from '../billing-cancel-modal/billing-cancel-modal/billing-cancel-modal.component';
import { InvoiceNumberPipe } from 'src/app/pipes/invoice-number.pipe';


@Directive({
  selector: 'th[sortable]',
  host: {
    '[class.asc]': 'direction === "asc"',
    '[class.desc]': 'direction === "desc"',
    '(click)': 'rotate()'
  }
})
export class NgbdSortableHeader {

  @Input() sortable: SortColumn = '';
  @Input() direction: SortDirection = '';
  @Output() sort = new EventEmitter<SortEvent>();

  rotate() {
    this.direction = rotate[this.direction];
    this.sort.emit({ column: this.sortable, direction: this.direction });
  }
}

@Component({
  selector: 'app-billing-overview',
  templateUrl: './billing-overview.component.html',
  styleUrls: ['./billing-overview.component.scss'],
  providers: [DiscountPipe, AddressPipe, StayDurationPipe, CurrencyPipe, DatePipe, RoomReservationPipe, InvoiceNumberPipe]
})
export class BillingOverviewComponent implements OnInit {

  faMail= faEnvelope;
  faDownload= faDownload;
  uiState$: Observable<UI_STATE>;
  UI_STATE = UI_STATE;

  receits$: Receit[]=[];


  searchTermSubscription: Subscription;

  constructor(
    private modalService: NgbModal,
    private billingService: BillingService,
    private receitQuery: ReceitQuery,
    private tokenStorage: TokenStorageService,
    private router: Router, 
    private discountPipe: DiscountPipe,
    private addressPipe: AddressPipe,
    private currencyPipe: CurrencyPipe,
    private stayDurationPipe: StayDurationPipe,
    private datePipe: DatePipe,
    private roomReservationPipe: RoomReservationPipe,
    private invoiceNumberPipe: InvoiceNumberPipe,
  ) { this.receitQuery.selectAll().subscribe(
    (receits) => {
      this.unsortedReceits = receits;
      this.receits$ = receits;
    }
  );
  this.searchTermSubscription = this.searchTerm.valueChanges.pipe(
    map(searchTerm => this.receits$ = this.search(
      searchTerm, 
      this.discountPipe, 
      this.addressPipe, 
      this.currencyPipe, 
      this.stayDurationPipe, 
      this.datePipe,
      this.roomReservationPipe,
      this.invoiceNumberPipe
    ))
  ).subscribe();
  }

  ngOnInit(): void {
    if (!this.tokenStorage.isLoggedIn()) {
      this.router.navigate(['']);
    }
    else this.billingService.get();
  }

  onRetry(): void {
    this.billingService.get();
  }

  onCancelReceit(receit:Receit) {
    //this.billingService.cancel(receit);
    const modalRef = this.modalService.open(BillingCancelModalComponent);
    modalRef.componentInstance.receit = receit;
  }

  onDownloadReceit(receit:Receit) {
    this.billingService.getPDF(receit.invoiceNumber);
    //alert("download test");
  }

  onDeleteCancellation(receit) {
    //TODO: receits are now fully functional and not only cancelllations -> only modify the necessary fields
    // how to handle "paper invoices"?
    this.billingService.delete(receit);
  }
  
  ngOnDestroy(): void {
  }

    /* Search related*/
  unsortedReceits = [];
  expandedSearch = false;
  searchTerm = new FormControl('');
  dateBeginEnabled = new FormControl(true);
  dateEndEnabled = new FormControl(true);
  dateFilterBegin = new FormControl(
    new Date().toISOString().substring(0, 10)
  );
  dateFilterEnd = new FormControl(
    new Date().toISOString().substring(0, 10)
  );
  dateSearchInvalid: boolean = false;

  @ViewChildren(NgbdSortableHeader) 
  headers: QueryList<NgbdSortableHeader>;


  onSort({ column, direction }: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    // sorting receits
    if (direction === '' || column === '') {
      this.receits$ = this.unsortedReceits;
    } else {
      this.receits$ = [...this.unsortedReceits].sort((a, b) => {
        const res = compare(a[column].toString(), b[column].toString());
        return direction === 'asc' ? res : -res;
      });
    }
  }
  search(
    text: string,
    discountPipe: DiscountPipe,
    addressPipe: AddressPipe,
    currencyPipe: CurrencyPipe,
    stayDurationPipe: StayDurationPipe,
    datePipe: DatePipe,
    roomReservationPipe: RoomReservationPipe,
    invoiceNumberPipe: InvoiceNumberPipe
    )
    : Receit[] {
    return this.unsortedReceits.filter((receit:Receit) => {
      const searchTerm = text.toLowerCase();
 
      console.log(JSON.stringify(receit));
      // Show only entries, which satisfy following conditions:
      return receit.reservation.customer.toLowerCase().includes(searchTerm) 
        || (receit.reservation.firstName + " " + receit.reservation.lastName).toLowerCase().includes(searchTerm)
        || receit.invoiceNumber.toString().includes(searchTerm)
        || addressPipe.transform(receit.reservation).toLowerCase().includes(searchTerm)
        || roomReservationPipe.transform(receit.reservation.rooms).toLowerCase().includes(searchTerm) 
        || receit.reservation.num_stays_days.toString().includes(searchTerm)
        || (receit.invoiceCancellationDate !== null ? datePipe.transform(receit.invoiceCancellationDate, 'shortDate').toLowerCase().includes(searchTerm) : null)
        || stayDurationPipe.transform(receit.reservation.num_stays_days).toLowerCase().includes(searchTerm)
        || currencyPipe.transform(receit.reservation.price).toString().includes(searchTerm)
        || discountPipe.transform(receit.reservation.discount).toLowerCase().includes(searchTerm)
        || datePipe.transform(receit.invoiceDate, 'shortdate').toLowerCase().includes(searchTerm)
        || (receit.cancelledFrom !== null?  receit.cancelledFrom.toLowerCase().includes(searchTerm) : null)
    })
  }

  onToggleExpandedSearch() {
    this.expandedSearch = !this.expandedSearch;
  }

}


export type SortColumn = keyof Receit | '';
export interface SortEvent {
  column: SortColumn;
  direction: SortDirection;
}
const rotate: { [key: string]: SortDirection } = { 'asc': 'desc', 'desc': '', '': 'asc' };  
const compare = (v1: string | number, v2: string | number) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0; 
