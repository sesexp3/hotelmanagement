import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Reservation } from 'src/app/reservation-overview/state/reservation.model';

@Component({
  selector: 'app-show-error-modal',
  templateUrl: './show-error-modal.component.html',
  styleUrls: ['./show-error-modal.component.scss']
})
export class ShowErrorModalComponent implements OnDestroy {

  errormessage: string;
  headline: string;
  onCloseCallback: Function;
  reservation: Reservation;

  constructor(
    public activeModal: NgbActiveModal,
    ) { }

  ngOnDestroy(): void {
    if (this.onCloseCallback != null)
      this.onCloseCallback();
  }

}
