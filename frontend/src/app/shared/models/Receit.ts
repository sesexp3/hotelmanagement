import { ID } from '@datorama/akita';
import { Reservation } from 'src/app/reservation-overview/state/reservation.model';
import { User } from "./User";

export class Receit {
    id: ID;
    invoiceNumber: number;
    grandTotal: number;
    reservation: Reservation;
    invoiceCancellationDate?: Date;
    cancelledFrom?: String;
    invoiceDate: Date;

    constructor(id: ID, invoiceNumber: number, grandTotal: number, reservation: Reservation,
         invoiceCancellationDate?: Date, cancelledFrom?: String, invoiceDate?: Date) {
        
        this.id = id;
        this.invoiceNumber = invoiceNumber;
        this.grandTotal = grandTotal;
        this.reservation = reservation;
        this.invoiceCancellationDate = invoiceCancellationDate;
        this.cancelledFrom = cancelledFrom;
        this.invoiceDate = invoiceDate;
    }
}