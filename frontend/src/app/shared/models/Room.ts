import { ID } from '@datorama/akita';
import { RoomType } from './RoomType';

export class Room {
    id: ID;
    name: string;
    roomType: RoomType;
    occupancy: number; // Belegzahl
    overcrowding: boolean; // Überbelegung möglich
}