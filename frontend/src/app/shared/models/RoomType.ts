import { ID } from "@datorama/akita";

export interface RoomType {
    id: ID,
    name: string,
    priceBase: number,
    priceOvercrowding: number,
    quantity: number,
    quantityOvercrowding,
  }

  export class RoomType implements RoomType {
    constructor(id: ID, name: string, priceBase: number, priceOvercrowding: number) {
        this.id = id;
        this.name = name;
        this.priceBase = priceBase;
        this.priceOvercrowding = priceOvercrowding;
        this.quantity = 0;
        this.quantityOvercrowding = 0;
    }
  }

