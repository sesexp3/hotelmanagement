import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { faCreditCard, faUser, faBed, faTh } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from './auth/auth.service';
import { AuthLogoutInfo } from './auth/logout-info';
import { TokenStorageService } from './auth/token-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  faCreditCard = faCreditCard;
  faUser = faUser;
  faBed = faBed;
  faRoom = faTh;

  isLoggedIn = this.tokenStorageService.isLoggedIn();
  isBillingActive = false;
  isReservationActive = true;
  isRoomsActive = false;
  title = "Zum goldenen Löwen";

  constructor (
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private tokenStorageService: TokenStorageService
  ) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        if (event.url.includes("reservation")) {
          this.onReservation();
        } else if (event.url.includes("billing") || event.url.includes("cancelreceit")) {
          this.onBilling();
        } else if (event.url.includes("rooms")) {
          this.onRooms();
        }
      }
    });
  }

  onTokenChanged() {
    this.route.url.subscribe((url) => {console.log(url)});
    this.isLoggedIn = this.tokenStorageService.isLoggedIn();
  }

  ngOnInit() {
    this.tokenStorageService.getLoggedInValue().subscribe((value) => {
      this.isLoggedIn = value;
    });
  }

  onBilling() {
    this.isBillingActive = true;
    this.isReservationActive = false;
    this.isRoomsActive = false;
  }

  onReservation() {
    this.isBillingActive = false;
    this.isReservationActive = true;
    this.isRoomsActive = false;
  }

  onRooms() {
    this.isBillingActive = false;
    this.isReservationActive = false;
    this.isRoomsActive = true;
  }

  onLogout() {

    //TODO: the methods of signout and navigate are not properly called.
    this.authService.logout(new AuthLogoutInfo(this.tokenStorageService.getEmail())).subscribe(
      data => {
        this.tokenStorageService.signOut();
        this.router.navigate(['']);
      },
      error => {
        this.tokenStorageService.signOut();
        this.router.navigate(['']);
      }
    );
  }
}
