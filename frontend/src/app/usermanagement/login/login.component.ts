import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AuthLoginInfo } from '../../auth/login-info';
import { AuthService } from '../../auth/auth.service';
import { TokenStorageService } from '../../auth/token-storage.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ShowErrorModalComponent } from '../../shared/components/show-error-modal/show-error-modal.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  loginInfo: AuthLoginInfo;

  constructor(
    private router: Router,
    private authService: AuthService,
    private tokenStorage: TokenStorageService,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.isLoggedIn = this.tokenStorage.isLoggedIn()
  }

  onSubmit(logForm: NgForm, type: string) {
    this.loginInfo = new AuthLoginInfo(
      logForm.form.value.email,
      logForm.form.value.password);

    this.authService.attemptAuth(this.loginInfo).subscribe(
      data => {
        this.tokenStorage.saveToken(data.accessToken);
        this.tokenStorage.saveUsername(data.username);
        this.tokenStorage.saveEmail(logForm.form.value.email);

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.router.navigate(['reservation']);
      },
      error => {
        //console.error(error.error.reason)
        this.errorMessage = error.error.reason;
        this.isLoginFailed = true;
        const modal = this.modalService.open(ShowErrorModalComponent);
        modal.componentInstance.headline = "Anmeldung fehlgeschlagen!";
        modal.componentInstance.errormessage = "Die eigegebene E-Mailadresse oder das Passwort ist ungültig. Wiederholen Sie die Eingabe, und versuchen Sie es noch mal.";
      }
    );
  }
}
