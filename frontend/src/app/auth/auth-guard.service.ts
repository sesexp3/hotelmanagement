import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {TokenStorageService} from './token-storage.service';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private router: Router, private tokenStorageService: TokenStorageService) {
  }

  canActivate() {
    if (this.tokenStorageService.isLoggedIn()) {
      return true;
    }
    this.router.navigate(['']);
    return false;
  }
}