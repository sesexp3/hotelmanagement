import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AuthLoginInfo } from './login-info';
import { AuthLogoutInfo } from './logout-info';
import { JwtResponse } from './jwt-response';
import { environment } from '../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {
  }

  attemptAuth(credentials: AuthLoginInfo): Observable<JwtResponse> {
    return this.http.post<JwtResponse>(API_URL + '/employees/login', credentials, httpOptions);
  }

  logout(info: AuthLogoutInfo): Observable<string> {
    return this.http.post<string>(API_URL + '/employees/logout', info, httpOptions);
  }
}
