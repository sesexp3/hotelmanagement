import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {  HTTP_INTERCEPTORS,HttpClientModule } from '@angular/common/http';
import { LOCALE_ID} from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
registerLocaleData(localeDe, 'de');

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AkitaNgDevtools } from '@datorama/akita-ngdevtools';
import { environment } from '../environments/environment';
import { NgbDropdown, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReservationCreateComponent } from './reservation-overview/reservation-create/reservation-create.component';
import { ReservationOverviewComponent } from './reservation-overview/reservation-overview.component';
import { NgbdSortableHeader } from './reservation-overview/reservation-overview.component';
import { NG_ENTITY_SERVICE_CONFIG } from '@datorama/akita-ng-entity-service';
import { CurrencyPipe } from './pipes/currency.pipe';
import { DiscountPipe } from './pipes/discount.pipe';
import { ReservationDeleteModalComponent } from './reservation-overview/reservation-delete-modal/reservation-delete-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddressPipe } from './pipes/address.pipe';
import { StayDurationPipe } from './pipes/stay-duration.pipe';
import { RoomReservationPipe } from './pipes/room-reservation.pipe';

import { HeaderComponent } from './shared/components/header/header.component';
import { LoginComponent } from './usermanagement/login/login.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AuthGuardService } from './auth/auth-guard.service';
import { ReservationCreateModalComponent } from './reservation-overview/reservation-create-modal/reservation-create-modal.component';
import { ShowErrorModalComponent } from './shared/components/show-error-modal/show-error-modal.component';
import { BillingOverviewComponent } from './billingmanagement/billing-overview/billing-overview.component';
import { CancelReceitComponent } from './billingmanagement/cancel-receit/cancel-receit.component';
import { AuthInterceptor } from './auth/auth-interceptor';
import { RoomCreateComponent } from './roommanagement/room-create/room-create.component';
import { RoomOverviewComponent } from './roommanagement/room-overview/room-overview.component';
import { RoomDeleteComponent } from './roommanagement/room-delete/room-delete.component';
import { RoomTypePipe } from './pipes/room-type.pipe';
import { BooleanPipe } from './pipes/boolean.pipe';
import { StatsComponent } from './roommanagement/stats/stats.component';
import { ChartsModule } from 'ng2-charts';
import { BillingCancelModalComponent } from './billingmanagement/billing-cancel-modal/billing-cancel-modal/billing-cancel-modal.component';
import { InvoiceNumberPipe } from './pipes/invoice-number.pipe';

@NgModule({
  declarations: [
    AppComponent,
    NgbdSortableHeader,
    HeaderComponent,
    LoginComponent,
    ReservationCreateComponent,
    ReservationOverviewComponent,
    CurrencyPipe,
    DiscountPipe,
    ReservationDeleteModalComponent,
    AddressPipe,
    RoomTypePipe,
    StayDurationPipe,
    RoomReservationPipe,
    ReservationCreateModalComponent,
    ShowErrorModalComponent,
    BillingOverviewComponent,
    CancelReceitComponent,
    RoomCreateComponent,
    RoomOverviewComponent,
    RoomDeleteComponent,
    RoomTypePipe,
    BooleanPipe,
    StatsComponent,
    BillingCancelModalComponent,
    InvoiceNumberPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    environment.production ? [] : AkitaNgDevtools.forRoot(),
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    ChartsModule
  ],
  exports: [
    ReservationOverviewComponent,
    ReservationDeleteModalComponent,
    RoomDeleteComponent,
    ReservationCreateComponent,
    ReservationCreateModalComponent,
    ShowErrorModalComponent,
  ],
  providers: [
    { provide: NG_ENTITY_SERVICE_CONFIG, useValue: { baseUrl: 'https://jsonplaceholder.typicode.com' } },
    AuthGuardService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    { provide: LOCALE_ID, useValue: 'de' },
  ],
  bootstrap: [AppComponent],
  entryComponents: [ReservationDeleteModalComponent]
})
export class AppModule { }
