import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReservationCreateComponent } from './reservation-overview/reservation-create/reservation-create.component';
import { ReservationOverviewComponent } from './reservation-overview/reservation-overview.component';
import { LoginComponent } from './usermanagement/login/login.component';
import { AuthGuardService } from './auth/auth-guard.service';
import { BillingOverviewComponent } from './billingmanagement/billing-overview/billing-overview.component';
import { CancelReceitComponent } from './billingmanagement/cancel-receit/cancel-receit.component';
import { RoomCreateComponent } from './roommanagement/room-create/room-create.component';
import { RoomOverviewComponent } from './roommanagement/room-overview/room-overview.component';
import { StatsComponent } from './roommanagement/stats/stats.component';


const routes: Routes = [
  { path: '', component: ReservationCreateComponent },
  { path: 'login', component: LoginComponent },
  { path: 'reservation', component: ReservationOverviewComponent, canActivate: [AuthGuardService] },
  { path: 'reservation/create', component: ReservationCreateComponent, canActivate: [AuthGuardService] }, 
  { path: 'reservation/:id', component: ReservationCreateComponent, canActivate: [AuthGuardService] },
  { path: 'billing', component: BillingOverviewComponent, canActivate: [AuthGuardService] },
  //{ path: 'cancelreceit', component: CancelReceitComponent, canActivate: [AuthGuardService] },
  { path: 'rooms', component: RoomOverviewComponent },
  { path: 'rooms/stats', component: StatsComponent },
  { path: 'rooms/create', component: RoomCreateComponent, canActivate: [AuthGuardService]},
  { path: 'rooms/:id', component: RoomCreateComponent, canActivate: [AuthGuardService]},
  { path: '**', redirectTo: 'reservation' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
