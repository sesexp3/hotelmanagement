package sese.hotel;

import org.mockito.Mockito;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import sese.hotel.Model.*;
import sese.hotel.Model.Archive.ArchivedReservation;
import sese.hotel.Model.Archive.ArchivedRoomReservation;
import sese.hotel.Model.Archive.ArchivedRoomType;

import java.sql.Date;
import java.util.*;

@Component
@Profile("test")
public class TestDataStore {

    Reservation reservation;
    Customer customer;
    Address address;
    RoomReservation roomReservation;
    RoomType roomType;
    ArchivedReservation archivedReservation;
    Invoice invoice;
    ArchivedRoomReservation archivedRoomReservation;
    InvoiceLineRoom invoiceLine;
    PaymentInformation paymentInformation;
    Room room;

    public Reservation createReservation(){
        Reservation reservation = new Reservation();
        reservation.setAddress(this.address);
        reservation.setArrivalDate(new Date(System.currentTimeMillis()));
        reservation.setDepartureDate(new Date(System.currentTimeMillis() + 86400000));
        reservation.setCustomer(this.customer);
        reservation.setDiscount(10d);
        reservation.setId(1L);
        return reservation;
    }

    public Address createAddress(){
        Address address = new Address();
        address.setStreet("Teststraße");
        address.setCity("Vienna");
        address.setPostcode("1100");
        address.setNumber("11");
        address.setCountry("Austria");
        address.setId(1L);
        return address;
    }

    public Customer createCustomer(){
        Customer customer = new Customer();
        customer.setFirstName("Max");
        customer.setSurName("Mustermann");
        customer.setId(1L);
        return customer;
    }

    public RoomType createRoomType(){
        RoomType dz = new RoomType();
        dz.setName("Doppelzimmer");
        dz.setPriceBase(200);
        dz.setPriceOvercrowding(250);
        dz.setId(1L);
        return dz;
    }

    public Room createRoom(){
        Room room = new Room();
        room.setId(1L);
        room.setName("Zimmer 101");
        room.setOccupancy(2);
        room.setOvercrowding(true);
        room.setRoomType(this.roomType);
        return room;
    }

    public RoomReservation createRoomReservation(){
        RoomReservation roomReservation = new RoomReservation();
        roomReservation.setCount(3);
        roomReservation.setRoomType(this.roomType);
        roomReservation.setOvercrowding(false);
        roomReservation.setId(1L);
        return roomReservation;
    }

    public Invoice createInvoice(){
        Invoice invoice = new Invoice();
        invoice.setId(1L);
        invoice.setInvoiceDate(new Date(System.currentTimeMillis()));
        invoice.setOriginalReservationNumber(1);
        invoice.setRunningNumber(0);
        invoice.setInvoiceNumber(10L);
        invoice.setReservation(archivedReservation);
        return invoice;
    }

    public InvoiceLineRoom createInvoiceLineRoom(){
        InvoiceLineRoom invoiceLine = new InvoiceLineRoom();
        invoiceLine.setLineNumber(1);
        invoiceLine.setId(1L);
        invoiceLine.setInvoice(this.invoice);
        invoiceLine.setRoomReservation(this.archivedRoomReservation);
        return invoiceLine;
    }

    public PaymentInformation createPaymentInformation(){
        PaymentInformation paymentInformation = new PaymentInformation();
        paymentInformation.setCreditCardHolder(customer.getName());
        paymentInformation.setCreditCardNumber("1234123412341234");
        paymentInformation.setCreditCardValidUntilMonth("12");
        paymentInformation.setCreditCardValidUntilYear("25");
        paymentInformation.setCreditCardVerificationNumber("089");
        return paymentInformation;
    }

    public void init(){
        this.address = this.createAddress();
        this.customer = this.createCustomer();
        this.roomType = this.createRoomType();
        this.roomReservation = this.createRoomReservation();
        this.archivedRoomReservation = this.convert(roomReservation);
        this.reservation = this.createReservation();
        this.paymentInformation=this.createPaymentInformation();
        this.room = createRoom();

        this.reservation.setRoomReservations(Collections.singletonList(roomReservation));
        this.reservation.setPaymentInformation(this.paymentInformation);
        this.archivedReservation = this.convert(reservation);
        this.archivedReservation.setId(1L);
        this.invoice= createInvoice();
        this.invoiceLine = createInvoiceLineRoom();
        invoice.setLineList(Collections.singletonList(this.invoiceLine));
    }

    private ArchivedRoomReservation convert(RoomReservation roomReservation){
        ArchivedRoomReservation archivedRoomReservation = new ArchivedRoomReservation();
        archivedRoomReservation.setOvercrowding(roomReservation.isOvercrowding());
        archivedRoomReservation.setCount(roomReservation.getCount());

        ArchivedRoomType roomType = new ArchivedRoomType();
        roomType.setName(roomReservation.getRoomType().getName());
        roomType.setPriceBase(roomReservation.getRoomType().getPriceBase());
        roomType.setPriceOvercrowding(roomReservation.getRoomType().getPriceOvercrowding());
        roomType.setId(1L);
        archivedRoomReservation.setRoomType(roomType);

        return archivedRoomReservation;
    }

    private ArchivedReservation convert(Reservation reservation){
        ArchivedReservation archivedReservation = new ArchivedReservation();
        archivedReservation.setArrivalDate(reservation.getArrivalDate());
        archivedReservation.setDepartureDate(reservation.getDepartureDate());
        archivedReservation.setDiscount(reservation.getDiscount());
        archivedReservation.setPrice(reservation.getPrice());
        archivedReservation.setCustomer(reservation.getCustomer());
        archivedReservation.setAddress(reservation.getAddress());
        List <ArchivedRoomReservation> roomReservations = new ArrayList<>();
        for (RoomReservation roomReservation: reservation.getRoomReservations()) {
            roomReservations.add(convert(roomReservation));
        }
        archivedReservation.setRoomReservations(roomReservations);
        return archivedReservation;
    }
}
