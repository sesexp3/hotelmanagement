package sese.hotel.ControllerTest;

import org.assertj.core.api.Assert;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import sese.hotel.Controller.EmployeeController;
import sese.hotel.Controller.Message.DTO.UserDTO;
import sese.hotel.Controller.Message.Response.JwtResponse;
import sese.hotel.Model.Address;
import sese.hotel.Model.Employee;
import sese.hotel.Model.Enum.TaskArea;
import sese.hotel.Repository.EmployeeRepository;
import sese.hotel.Security.UserDetailsServiceImpl;
import sese.hotel.Security.UserPrincipal;
import sese.hotel.Service.EmployeeService;
import sese.hotel.Service.TestData;

import java.sql.Date;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.springframework.mock.http.server.reactive.MockServerHttpRequest.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;


@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class EmployeeControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private EmployeeController employeeController;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    TestData testData;

    @WithMockUser(value = "admin")
    @Test
    public void getEmployeeGivenAuth_shouldSucceedWith200() throws Exception {
        mvc.perform(get("/employees/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @WithMockUser(value = "admin")
    @Test
    public void loginGivenAuth_shouldSucceedWith200() throws Exception {

       /* Employee employee = new Employee();
        employee.setPassword(encoder.encode("admin"));
        employee.setName("Test User");
        employee.setEmail("admin@admin.com");
        employee.setMinutesPerWeek(1000);
        employee.setIban("asdasd");
        employee.setEmployeeCode("asd");
        employee.setDateOfBirth(Date.valueOf("2020-01-01"));
        employee.setPhoneNumber("1111");
        employee.setInsuranceNumber("11111");
        employee.setSex("M");
        employee.setTaskArea(TaskArea.OTHERS);

        Address address = new Address();
        address.setStreet("asd");
        address.setCity("asda");
        address.setPostcode("11");
        address.setNumber("11");
        address.setCountry("asda");
        employee.setAddress(address);
        employeeRepository.save(employee);*/
        testData.createDefaultEmployee();

        JSONObject testUser = new JSONObject();
        testUser.put("username", "admin@admin.com");
        testUser.put("password", "admin");

        mvc.perform(MockMvcRequestBuilders.post("/employees/login")
                .content(testUser.toString())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }


}
