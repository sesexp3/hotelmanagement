package sese.hotel.ControllerTest;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import sese.hotel.Model.User;
import sese.hotel.Repository.UserRepository;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class TestControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private UserRepository userRepository;

/*    @Test
    void helloWorld_Returns200() throws Exception {
        mockMvc.perform(get("/")
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(content().string("Hello World!"));
    }*/

/*    @Test
    public void helloWorld_Returns200_1() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/",
                String.class)).contains("Hello, World!");
    }*/



    @Test
    public void testJpa() throws Exception {
        User userSaved = userRepository.save(new User());

        User user = userRepository.findById(userSaved.getId()).get();

        assertThat(userSaved.getId()).isEqualTo(user.getId());
    }

}
