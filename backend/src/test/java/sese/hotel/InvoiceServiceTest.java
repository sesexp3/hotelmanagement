package sese.hotel;


import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import sese.hotel.Controller.Message.DTO.InvoiceDTO;
import sese.hotel.Controller.Message.DTO.ReservationDTO;
import sese.hotel.Model.*;
import sese.hotel.Model.Archive.ArchivedReservation;
import sese.hotel.Model.Archive.ArchivedRoomReservation;
import sese.hotel.Model.Archive.ArchivedRoomType;
import sese.hotel.Repository.InvoiceRepository;
import sese.hotel.Repository.ReservationRepository;
import sese.hotel.Service.InvoiceService;

import java.sql.Date;
import java.text.ParseException;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
@ActiveProfiles("test")
public class InvoiceServiceTest {

    @Autowired
    private InvoiceService invoiceService;

    @MockBean
    private InvoiceRepository invoiceRepository;

    @MockBean
    private ReservationRepository reservationRepository;

    @Autowired
    private TestDataStore testDataStore;

    @BeforeEach
    public void setUp() {
        testDataStore.init();
        List<Invoice> allInvoices = Arrays.asList(testDataStore.invoice);

        Mockito.when(invoiceRepository.findAll()).thenReturn(allInvoices);
        Mockito.when(invoiceRepository.findByInvoiceNumber(10L)).thenReturn(testDataStore.invoice);
        Mockito.when(invoiceRepository.findByOriginalReservationNumber(1)).thenReturn(allInvoices);
        Mockito.when(reservationRepository.findById(1L)).thenReturn(Optional.of(testDataStore.reservation));
        Mockito.when(invoiceRepository.saveAndFlush(Mockito.any(Invoice.class))).thenAnswer(i -> i.getArguments()[0]);

    }

    @Test
    public void returnAllInvoices() {
        List<Invoice> invoices = invoiceRepository.findAll();
        assertThat(invoices).hasSize(1).extracting(Invoice::getInvoiceNumber).contains((long) 10);

        assert invoiceService != null;
        List<InvoiceDTO> allInvoices1 = invoiceService.getAllInvoices();

        assertThat(allInvoices1).hasSize(1).extracting(InvoiceDTO::getInvoiceNumber).contains((long) 10);
    }

    @Test
    public void returnSingleInvoiceByInvoiceNumber() {
        InvoiceDTO invoice = invoiceService.getInvoiceForInvoiceNumber(10L);
        assertThat(invoice.getId()).isEqualTo(1L);
    }

    @Test
    public void returnSingleInvoiceByInvalidInvoiceNumber() {
        InvoiceDTO invoice = invoiceService.getInvoiceForInvoiceNumber(50L);
        assertThat(invoice).isEqualTo(null);
    }

    @Test
    public void returnSingleInvoiceByInvalidReservation() {
        InvoiceDTO invoice = invoiceService.getInvoiceForInvoiceNumber(90L);
        assertThat(invoice).isEqualTo(null);
    }

    @Test
    public void returnSingleInvoiceByReservation() {
        Invoice invoice = invoiceService.getInvoiceForReservation(1);
        assertThat(invoice.getId()).isEqualTo(1L);
    }

    @Test
    public void cancelInvoice() {
        assertThat(testDataStore.invoice.getCancelledFrom()).isEqualTo(null);
        InvoiceDTO invoice = invoiceService.cancelInvoice(10L);
        assertThat(invoice.getCancelledFrom()).isEqualTo("admin");
    }

    @Test
    public void revokeCancelInvoice() {
        testDataStore.invoice.setCancelledFrom("admin");
        assertThat(testDataStore.invoice.getCancelledFrom()).isEqualTo("admin");
        invoiceService.revokeCancellation(10L);
        assertThat(invoiceService.getInvoiceForInvoiceNumber(10L).getCancelledFrom()).isEqualTo("");
    }



}
