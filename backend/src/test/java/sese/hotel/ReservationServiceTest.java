package sese.hotel;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import sese.hotel.Controller.Message.DTO.InvoiceDTO;
import sese.hotel.Controller.Message.DTO.ReservationDTO;
import sese.hotel.Model.Invoice;
import sese.hotel.Model.Reservation;
import sese.hotel.Model.RoomReservation;
import sese.hotel.Model.RoomType;
import sese.hotel.Repository.*;
import sese.hotel.Service.InvoiceService;
import sese.hotel.Service.ReservationService;

import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
@ActiveProfiles("test")
public class ReservationServiceTest {

    @Autowired
    private InvoiceService invoiceService;

    @Autowired
    private ReservationService reservationService;

    @MockBean
    private InvoiceRepository invoiceRepository;

    @MockBean
    private ReservationRepository reservationRepository;

    @MockBean
    private CustomerRepository customerRepository;

    @MockBean
    private RoomTypeRepository roomTypeRepository;

    @MockBean
    private RoomReservationRepository roomReservationRepository;

    @Autowired
    private TestDataStore testDataStore;

    @BeforeEach
    public void setUp() {
        testDataStore.init();
        Mockito.when(reservationRepository.findAll()).thenReturn(Arrays.asList(testDataStore.reservation));
        Mockito.when(reservationRepository.findById(1L)).thenReturn(Optional.of(testDataStore.reservation));
        Mockito.when(customerRepository.findById(testDataStore.reservation.getCustomer().getId())).thenReturn(Optional.ofNullable(testDataStore.customer));
        Mockito.when(roomTypeRepository.findById(testDataStore.roomType.getId())).thenReturn(Optional.ofNullable(testDataStore.roomType));
        Mockito.when(roomReservationRepository.save(Mockito.any(RoomReservation.class))).thenAnswer(i -> i.getArguments()[0]);
        Mockito.when(roomTypeRepository.save(Mockito.any(RoomType.class))).thenAnswer(i -> i.getArguments()[0]);
        Mockito.when(reservationRepository.save(Mockito.any(Reservation.class))).thenAnswer(i -> i.getArguments()[0]);
    }

    @Test
    public void returnAllReservations() {
        List<Reservation> reservations = reservationRepository.findAll();
        assertThat(reservations).hasSize(1).extracting(Reservation::getId).contains((long) 1);

        assert reservationService != null;
        List<ReservationDTO> allReservations = reservationService.findAll();
        assertThat(allReservations).hasSize(1).extracting(ReservationDTO::getId).contains((long) 1);
    }

    @Test
    public void updateReservation() throws ParseException {
        ReservationDTO reservationDTO = new ReservationDTO();
        reservationDTO=reservationService.convertReservationToDTO(testDataStore.reservation);
        assertThat(reservationDTO.discount!=77d);
        reservationDTO.discount=77d;
        reservationService.updateReservation(1L,reservationDTO);
        Optional<Reservation> reservation = reservationRepository.findById(reservationDTO.id);
        assertThat(reservation.get().getDiscount()).isEqualTo(77d);
    }

    @Test
    public void addReservationTest() throws ParseException {
        ReservationDTO reservationDTO = new ReservationDTO();
        reservationDTO=reservationService.convertReservationToDTO(testDataStore.reservation);
        reservationDTO.id=0L;
        reservationDTO.firstName="Test";
        reservationDTO.lastName="Test";
        reservationService.addReservation(reservationDTO);
        verify(reservationRepository, times(1)).save(Mockito.any(Reservation.class));
    }


}
