package sese.hotel;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import sese.hotel.Controller.Message.DTO.ReservationDTO;
import sese.hotel.Controller.Message.DTO.RoomDTO;
import sese.hotel.Model.Reservation;
import sese.hotel.Model.Room;
import sese.hotel.Model.RoomReservation;
import sese.hotel.Model.RoomType;
import sese.hotel.Repository.*;
import sese.hotel.Service.InvoiceService;
import sese.hotel.Service.ReservationService;
import sese.hotel.Service.RoomService;

import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
@ActiveProfiles("test")
public class RoomServiceTest {

    @Autowired
    private InvoiceService invoiceService;

    @Autowired
    private RoomService roomService;

    @Autowired
    private ReservationService reservationService;

    @MockBean
    private InvoiceRepository invoiceRepository;

    @MockBean
    private ReservationRepository reservationRepository;

    @MockBean
    private CustomerRepository customerRepository;

    @MockBean
    private RoomTypeRepository roomTypeRepository;

    @MockBean
    private RoomRepository roomRepository;

    @MockBean
    private RoomReservationRepository roomReservationRepository;

    @Autowired
    private TestDataStore testDataStore;

    @BeforeEach
    public void setUp() {
        testDataStore.init();
        Mockito.when(roomRepository.save(Mockito.any(Room.class))).thenAnswer(i -> i.getArguments()[0]);
        Mockito.when(roomRepository.findAll()).thenReturn(Arrays.asList(testDataStore.room));
        Mockito.when(roomRepository.findById(1L)).thenReturn(Optional.ofNullable(testDataStore.room));
        Mockito.when(roomTypeRepository.findById(testDataStore.roomType.getId())).thenReturn(Optional.ofNullable(testDataStore.roomType));

    }

    @Test
    public void returnAllRooms() {
        List<RoomDTO> allRooms = roomService.findAll();
        assertThat(allRooms).hasSize(1).extracting(RoomDTO::getId).contains((long) 1);
    }

    @Test
    public void updateRoom() throws ParseException {
        RoomDTO roomDTO = new RoomDTO();
        roomDTO = roomService.convertRoomToDTO(testDataStore.room);
        assertThat(roomDTO.occupancy).isEqualTo(2);
        roomDTO.occupancy=3;
        roomService.updateRoom(1L,roomDTO);
        Optional<Room> room = roomRepository.findById(roomDTO.id);
        assertThat(room.get().getOccupancy()).isEqualTo(3);
    }

    @Test
    public void updateRoomInvalidRoom() {
        RoomDTO roomDTO = new RoomDTO();
        roomDTO = roomService.convertRoomToDTO(testDataStore.room);
        assertThat(roomDTO.occupancy).isEqualTo(2);
        roomDTO.occupancy=3;

        RoomDTO finalRoomDTO = roomDTO;
        assertThrows(IllegalArgumentException.class, () -> {
            roomService.updateRoom(6L, finalRoomDTO);
        });
    }

    @Test
    public void addRoomTest() throws ParseException {
        RoomDTO roomDTO = new RoomDTO();
        roomDTO=roomService.convertRoomToDTO(testDataStore.room);
        roomDTO.id=0L;
        roomDTO.occupancy=4;
        roomDTO.overcrowding=false;
        roomDTO.roomType= roomService.convertRoomTypeToDTO(testDataStore.roomType);
        roomService.addRoom(roomDTO);
        verify(roomRepository, times(1)).save(Mockito.any(Room.class));
    }


}
