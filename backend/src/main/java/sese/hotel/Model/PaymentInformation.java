package sese.hotel.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class PaymentInformation {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String creditCardNumber;
    private String creditCardHolder;
    private String creditCardValidUntilMonth;
    private String creditCardValidUntilYear;
    private String creditCardVerificationNumber;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public String getCreditCardHolder() {
        return creditCardHolder;
    }

    public void setCreditCardHolder(String creditCardHolder) {
        this.creditCardHolder = creditCardHolder;
    }

    public String getCreditCardValidUntilMonth() {
        return creditCardValidUntilMonth;
    }

    public void setCreditCardValidUntilMonth(String creditCardValidUntilMonth) {
        this.creditCardValidUntilMonth = creditCardValidUntilMonth;
    }

    public String getCreditCardValidUntilYear() {
        return creditCardValidUntilYear;
    }

    public void setCreditCardValidUntilYear(String creditCardValidUntilYear) {
        this.creditCardValidUntilYear = creditCardValidUntilYear;
    }

    public String getCreditCardVerificationNumber() {
        return creditCardVerificationNumber;
    }

    public void setCreditCardVerificationNumber(String creditCardVerificationNumber) {
        this.creditCardVerificationNumber = creditCardVerificationNumber;
    }
}
