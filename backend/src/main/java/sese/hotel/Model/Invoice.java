package sese.hotel.Model;

import sese.hotel.Model.Archive.ArchivedReservation;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
public class Invoice {


    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private Long invoiceNumber;

    private Date cancellationDate;

    private Date invoiceDate;

    @Column(nullable = true)
    private int runningNumber; //TODO better name

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "reservation_id" , nullable = true)
    private ArchivedReservation reservation;

    private String cancelledFrom;

    @OneToMany()
    @JoinColumn(name = "invoice_invoiceline_id")
    private List<InvoiceLine> lineList;

    @OneToOne(fetch=FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn(name = "blob_id" , nullable = true)
    private BlobEntity blob;

    private int originalReservationNumber;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(Long invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Date getCancellationDate() {
        return cancellationDate;
    }

    public void setCancellationDate(Date cancellationDate) {
        this.cancellationDate = cancellationDate;
    }


    public String getCancelledFrom() {return this.cancelledFrom; }

    public void setCancelledFrom(String cancelledFrom) { this.cancelledFrom = cancelledFrom; }

    public BlobEntity getBlob() {
        return blob;
    }

    public void setBlob(BlobEntity blob) {
        this.blob = blob;
    }

    public ArchivedReservation getReservation() {
        return reservation;
    }

    public void setReservation(ArchivedReservation reservation) {
        this.reservation = reservation;
    }

    public int getRunningNumber() {
        return runningNumber;
    }

    public void setRunningNumber(int runningNumber) {
        this.runningNumber = runningNumber;
    }

    public List<InvoiceLine> getLineList() {
        return lineList;
    }

    public void setLineList(List<InvoiceLine> lineList) {
        this.lineList = lineList;
    }

    public int getOriginalReservationNumber() {
        return originalReservationNumber;
    }

    public void setOriginalReservationNumber(int originalReservationNumber) {
        this.originalReservationNumber = originalReservationNumber;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "id=" + id +
                ", invoiceNumber=" + invoiceNumber +
                ", cancellationDate=" + cancellationDate +
                ", invoiceDate=" + invoiceDate +
                ", runningNumber=" + runningNumber +
                ", reservation=" + reservation +
                ", cancelledFrom='" + cancelledFrom + '\'' +
                ", lineList=" + lineList +
                ", blob=" + blob +
                ", originalReservationNumber=" + originalReservationNumber +
                '}';
    }
}
