package sese.hotel.Model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    private String name;
    private int occupancy; //Belegzahl
    private boolean overcrowding; // Übergelegung möglich

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "room_type_id")
    private RoomType roomType;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOccupancy() {
        return occupancy;
    }

    public void setOccupancy(int occupancy) {
        this.occupancy = occupancy;
    }

    public boolean isOvercrowding() {
        return overcrowding;
    }

    public void setOvercrowding(boolean overcrowding) {
        this.overcrowding = overcrowding;
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public void setRoomType(RoomType roomType) {
        this.roomType = roomType;
    }

    @Override
    public String toString() {
        return "Room{" +
                "Id=" + Id +
                ", name='" + name + '\'' +
                ", occupancy=" + occupancy +
                ", overcrowding=" + overcrowding +
                ", roomType=" + roomType +
                '}';
    }
}
