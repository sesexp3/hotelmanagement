package sese.hotel.Model;

import sese.hotel.Model.Archive.ArchivedRoomReservation;
import sese.hotel.Model.Archive.ArchivedRoomType;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.Period;


@Entity
@DiscriminatorValue("C")
public class InvoiceLineRoom extends InvoiceLine{

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "roomreservation_id")
    private ArchivedRoomReservation roomReservation;

    public ArchivedRoomReservation getRoomReservation() {
        return roomReservation;
    }

    public void setRoomReservation(ArchivedRoomReservation roomReservation) {
        this.roomReservation = roomReservation;
    }

    @Override
    public double getTotal() {
        LocalDate arrivalLocalDate = getInvoice().getReservation().getArrivalDate().toLocalDate();
        LocalDate departureLocalDate = getInvoice().getReservation().getDepartureDate().toLocalDate();
        int num_stays_days = Period.between(arrivalLocalDate, departureLocalDate).getDays();


        if(this.getRoomReservation().isOvercrowding()){
            return this.getRoomReservation().getRoomType().getPriceOvercrowding()*this.getRoomReservation().getCount()*num_stays_days*((100-getInvoice().getReservation().getDiscount())/100);
        }
        return this.getRoomReservation().getRoomType().getPriceBase()*this.getRoomReservation().getCount()*num_stays_days*((100-getInvoice().getReservation().getDiscount())/100);
    }
}
