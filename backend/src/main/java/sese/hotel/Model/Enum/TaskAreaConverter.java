package sese.hotel.Model.Enum;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

@Converter(autoApply = true)
public class TaskAreaConverter implements AttributeConverter<TaskArea, String> {
    @Override
    public String convertToDatabaseColumn(TaskArea taskArea) {
        if (taskArea == null) {
            return null;
        }
        return taskArea.getCode();
    }

    @Override
    public TaskArea convertToEntityAttribute(String code) {
        if (code == null) {
            return null;
        }

        return Stream.of(TaskArea.values())
                .filter(c -> c.getCode().equals(code))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
