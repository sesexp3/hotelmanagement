package sese.hotel.Model.Enum;

public enum TaskArea {

    RECEPTION("R"),KITCHEN("K"),SERVICE("S"),CLEANING("C"),OTHERS("O");

    private String code;

    private TaskArea(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
