package sese.hotel.Model.Archive;

import sese.hotel.Model.Address;
import sese.hotel.Model.Customer;
import sese.hotel.Model.PaymentInformation;
import sese.hotel.Model.RoomReservation;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
public class ArchivedReservation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    private Address address;

    private Date departureDate;

    private Date arrivalDate;

    @OneToMany()
    @JoinColumn(name = "room_reservation_id")
    private List<ArchivedRoomReservation> roomReservations;

    private double price;

    private double discount;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

   public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<ArchivedRoomReservation> getRoomReservations() {
        return roomReservations;
    }

    public void setRoomReservations(List<ArchivedRoomReservation> roomReservations) {
        this.roomReservations = roomReservations;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "id=" + id +
                ", departureDate=" + departureDate +
                ", arrivalDate=" + arrivalDate +
                ", price=" + price +
                ", discount=" + discount +
                '}';
    }
}
