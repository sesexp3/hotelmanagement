package sese.hotel.Model.Archive;

import org.springframework.beans.factory.annotation.Autowired;
import sese.hotel.Model.RoomReservation;
import sese.hotel.Model.RoomType;
import sese.hotel.Repository.ArchivedRoomTypeRepository;

import javax.persistence.*;


@Entity
public class ArchivedRoomReservation {


    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;
    private int count;


    @ManyToOne()
    @JoinColumn(name = "room_type_id")
    private ArchivedRoomType roomType;
    private boolean overcrowding;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArchivedRoomType getRoomType() {
        return roomType;
    }

    public void setRoomType(ArchivedRoomType roomType) {
        this.roomType = roomType;
    }

    public boolean isOvercrowding() {
        return overcrowding;
    }

    public void setOvercrowding(boolean overcrowding) {
        this.overcrowding = overcrowding;
    }

    @Override
    public String toString() {
        return "ArchivedRoomReservation{" +
                "Id=" + Id +
                ", count=" + count +
                ", roomType='" + roomType + '\'' +
                ", overcrowding=" + overcrowding +
                '}';
    }
}