package sese.hotel.Model.Archive;

import sese.hotel.Model.RoomType;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ArchivedRoomType {
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    private String name;
    private double priceBase;
    private double priceOvercrowding;

    public ArchivedRoomType(RoomType roomType) {
        this.name = roomType.getName();
        this.priceBase = roomType.getPriceBase();
        this.priceOvercrowding = roomType.getPriceOvercrowding();
    }

    public ArchivedRoomType() {

    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPriceBase() {
        return priceBase;
    }

    public void setPriceBase(double priceBase) {
        this.priceBase = priceBase;
    }

    public double getPriceOvercrowding() {
        return priceOvercrowding;
    }

    public void setPriceOvercrowding(double priceOvercrowding) {
        this.priceOvercrowding = priceOvercrowding;
    }

    @Override
    public String toString() {
        return "ArchivedRoomType{" +
                "Id=" + Id +
                ", name='" + name + '\'' +
                ", priceBase=" + priceBase +
                ", priceOption=" + priceOvercrowding +
                '}';
    }
}
