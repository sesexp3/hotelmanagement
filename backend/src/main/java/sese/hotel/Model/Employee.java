package sese.hotel.Model;

import sese.hotel.Model.Enum.TaskArea;

import javax.persistence.*;
import java.util.Date;

@Entity
@DiscriminatorValue("E")
public class Employee extends User{

    //@Id
    //@GeneratedValue(strategy = GenerationType.AUTO)
    //private long id;

    @Column(unique=true)
    private String employeeCode;

    private Date dateOfBirth;

    private String sex;

    private String phoneNumber;

    private String insuranceNumber;

    private String iban;

    private int minutesPerWeek;

    private TaskArea taskArea = TaskArea.OTHERS;

/*
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "bank_id" , nullable = true)
    private BankAccount bankAccount;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "employment_id" , nullable = true)
    private Employment employment;
*/



    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getInsuranceNumber() {
        return insuranceNumber;
    }

    public void setInsuranceNumber(String insuranceNumber) {
        this.insuranceNumber = insuranceNumber;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public int getMinutesPerWeek() {
        return minutesPerWeek;
    }

    public void setMinutesPerWeek(int minutesPerWeek) {
        this.minutesPerWeek = minutesPerWeek;
    }

    public TaskArea getTaskArea() {
        return taskArea;
    }

    public void setTaskArea(TaskArea taskArea) {
        this.taskArea = taskArea;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "employeeCode='" + employeeCode + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", sex='" + sex + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", insuranceNumber='" + insuranceNumber + '\'' +
                ", iban='" + iban + '\'' +
                ", minutesPerWeek=" + minutesPerWeek +
                ", taskArea=" + taskArea +
                '}';
    }
}
