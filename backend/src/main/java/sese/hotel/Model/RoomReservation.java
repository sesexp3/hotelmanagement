package sese.hotel.Model;

import sese.hotel.Controller.Message.DTO.RoomReservationDTO;

import javax.persistence.*;

@Entity
public class RoomReservation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;
    private int count;


    @ManyToOne()
    @JoinColumn(name = "room_type_id")
    private RoomType roomType;
    private boolean overcrowding;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public void setRoomType(RoomType roomType) {
        this.roomType = roomType;
    }

    public boolean isOvercrowding() {
        return overcrowding;
    }

    public void setOvercrowding(boolean overcrowding) {
        this.overcrowding = overcrowding;
    }

    @Override
    public String toString() {
        return "RoomReservation{" +
                "Id=" + Id +
                ", count=" + count +
                ", roomType='" + roomType + '\'' +
                ", overcrowding=" + overcrowding +
                '}';
    }
}

