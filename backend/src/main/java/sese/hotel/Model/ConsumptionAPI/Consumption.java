package sese.hotel.Model.ConsumptionAPI;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Consumption {

    @SerializedName("customer")
    @Expose
    private Customer customer;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "Consumption{" +
                "customer=" + customer +
                '}';
    }
}
