package sese.hotel.Model.ConsumptionAPI;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Invoice {

    @SerializedName("invoice")
    @Expose
    private Invoice_ invoice;

    public Invoice_ getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice_ invoice) {
        this.invoice = invoice;
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "invoice=" + invoice +
                '}';
    }
}