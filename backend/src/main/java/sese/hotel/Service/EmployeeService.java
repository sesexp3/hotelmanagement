package sese.hotel.Service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import sese.hotel.Controller.Message.DTO.AddressDTO;
import sese.hotel.Controller.Message.Request.AddressForm;
import sese.hotel.Controller.Message.Request.SignUpEmployeeForm;
import sese.hotel.Controller.Message.Request.SignUpEmployeeForm2;
import sese.hotel.Model.Address;
import sese.hotel.Model.Employee;
import sese.hotel.Model.User;
import sese.hotel.Repository.EmployeeRepository;
import sese.hotel.Repository.RoleRepository;

import java.sql.Date;
import java.util.HashMap;

@Service
public class EmployeeService {

    private HashMap<String, String> loggedInUserToken = new HashMap<>();

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;



    public User createAndStoreEmployee(SignUpEmployeeForm signUpEmployeeForm){
        Employee employee = employeeConverter(signUpEmployeeForm);
        employee.setAddress(addressConverter(signUpEmployeeForm.getAddress()));


        System.out.println("Attempt to register:" + employee.toString());
        return employeeRepository.save(employee);
    }

    public User createAndStoreEmployee2(SignUpEmployeeForm2 signUpEmployeeForm){
        Employee employee = employeeConverter2(signUpEmployeeForm);
        employee.setAddress(addressConverter2(signUpEmployeeForm.addressDTO));


        System.out.println("Attempt to register:" + employee.toString());
        return employeeRepository.save(employee);
    }




    private Address addressConverter(AddressForm addressForm){
        Address address=new Address();
        address.setNumber(StringUtils.defaultIfEmpty(addressForm.getNumber(), ""));
        address.setCountry(StringUtils.defaultIfEmpty(addressForm.getCountry(), ""));
        address.setCity(StringUtils.defaultIfEmpty(addressForm.getCity(), ""));
        address.setPostcode(StringUtils.defaultIfEmpty(addressForm.getPostcode(), ""));
        address.setStreet(StringUtils.defaultIfEmpty(addressForm.getStreet(), ""));
        return address;
    }

    private Employee employeeConverter(SignUpEmployeeForm signUpEmployeeForm){
        Employee employee = new Employee();
        employee.setEmail(signUpEmployeeForm.getEmail());
        employee.setPassword(encoder.encode(signUpEmployeeForm.getPassword()));
        employee.setPhoneNumber(StringUtils.defaultIfEmpty(signUpEmployeeForm.getPhoneNumber(), ""));
        employee.setDateOfBirth(Date.valueOf(StringUtils.defaultIfEmpty(signUpEmployeeForm.getDateOfBirth(), "2000-01-01")));
        employee.setInsuranceNumber(StringUtils.defaultIfEmpty(signUpEmployeeForm.getInsuranceNumber(), ""));
        employee.setSex(StringUtils.defaultIfEmpty(signUpEmployeeForm.getSex(), ""));
        employee.setEmployeeCode(StringUtils.defaultIfEmpty(signUpEmployeeForm.getEmployeeCode(), ""));
        employee.setIban(StringUtils.defaultIfEmpty(signUpEmployeeForm.getIBAN(), ""));
        employee.setMinutesPerWeek(Integer.parseInt(StringUtils.defaultIfEmpty(signUpEmployeeForm.getMinutesPerWeek(), "2310")));
        return employee;
    }

    private Address addressConverter2(AddressDTO addressForm){
        Address address=new Address();
        address.setNumber(StringUtils.defaultIfEmpty(addressForm.getNumber(), ""));
        address.setCountry(StringUtils.defaultIfEmpty(addressForm.getCountry(), ""));
        address.setCity(StringUtils.defaultIfEmpty(addressForm.getCity(), ""));
        address.setPostcode(StringUtils.defaultIfEmpty(addressForm.getPostcode(), ""));
        address.setStreet(StringUtils.defaultIfEmpty(addressForm.getStreet(), ""));
        return address;
    }

    private Employee employeeConverter2(SignUpEmployeeForm2 signUpEmployeeForm){
        Employee employee = new Employee();
        employee.setEmail(signUpEmployeeForm.userDTO.getEmail());
        employee.setPassword(encoder.encode(signUpEmployeeForm.userDTO.getPassword()));
        employee.setPhoneNumber(StringUtils.defaultIfEmpty(signUpEmployeeForm.employeeDTO.getPhoneNumber(), ""));
        employee.setDateOfBirth(Date.valueOf(StringUtils.defaultIfEmpty(signUpEmployeeForm.employeeDTO.getDateOfBirth(), "2000-01-01")));
        employee.setInsuranceNumber(StringUtils.defaultIfEmpty(signUpEmployeeForm.employeeDTO.getInsuranceNumber(), ""));
        employee.setSex(StringUtils.defaultIfEmpty(signUpEmployeeForm.employeeDTO.getSex(), ""));
        employee.setEmployeeCode(StringUtils.defaultIfEmpty(signUpEmployeeForm.employeeDTO.getEmployeeCode(), ""));
        employee.setIban(StringUtils.defaultIfEmpty(signUpEmployeeForm.employeeDTO.getIBAN(), ""));
        employee.setMinutesPerWeek(Integer.parseInt(StringUtils.defaultIfEmpty(signUpEmployeeForm.employeeDTO.getMinutesPerWeek(), "2310")));
        return employee;
    }
}
