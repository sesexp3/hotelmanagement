package sese.hotel.Service;

import org.hibernate.criterion.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sese.hotel.Controller.Message.DTO.*;
import sese.hotel.Model.*;
import sese.hotel.Model.Archive.ArchivedRoomType;
import sese.hotel.Repository.ArchivedRoomTypeRepository;
import sese.hotel.Repository.RoomRepository;
import sese.hotel.Repository.RoomTypeRepository;

import java.text.ParseException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoomService {
    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private ArchivedRoomTypeRepository archivedRoomTypeRepository;

    @Autowired
    private RoomTypeRepository roomTypeRepository;

    public void addRoom(RoomDTO roomDTO) throws ParseException {
        Room room = new Room();
        roomDTO.id = -1;
        room = this.convertToRoom(roomDTO, room);
        System.out.println(room);
        roomRepository.save(room);
    }

    public List<RoomDTO> findAll() {
        var rooms = this.roomRepository.findAll();
        var roomDTOs = rooms
                .stream()
                .map(room -> convertRoomToDTO(room))
                .collect(Collectors.toList());
        return roomDTOs;
    }

    public void updateRoom(long id, RoomDTO dto) throws ParseException {
        var room = this.roomRepository.findById(id);
        if (room.isPresent()) {
            // Update entity
            var roomEntity = room.get();
            roomEntity = this.convertToRoom(dto, roomEntity);
            this.roomRepository.saveAndFlush(roomEntity);
        } else {
            throw new IllegalArgumentException("The room to be updated does not exist");
        }
    }

    public boolean deleteRoom(long id) {
        var room = this.roomRepository.findById(id);
        if (room.isPresent()) {
            this.roomRepository.delete(room.get());
            return true;
        }
        return false;
    }

    public void addRoomType(RoomTypeDTO roomTypeDTO) throws ParseException {
        RoomType roomType = new RoomType();
        roomTypeDTO.id = -1;
        roomType = this.convertToRoomType(roomTypeDTO, roomType);
        System.out.println(roomType);
        roomTypeRepository.save(roomType);
    }

    public List<RoomTypeDTO> findAllTypes() {
        var roomTypes = this.roomTypeRepository.findAll();
        var roomTypeDTOs = roomTypes
                .stream()
                .map(roomType -> convertRoomTypeToDTO(roomType))
                .collect(Collectors.toList());
        return roomTypeDTOs;
    }

    public void updateRoomType(long id, RoomTypeDTO dto) throws ParseException {
        var roomType = this.roomTypeRepository.findById(id);
        if (roomType.isPresent()) {
            // Update entity
            var roomTypeEntity = roomType.get();
            roomTypeEntity = this.convertToRoomType(dto, roomTypeEntity);
            this.roomTypeRepository.saveAndFlush(roomTypeEntity);
        } else {
            throw new IllegalArgumentException("The room to be updated does not exist");
        }
    }

    public boolean deleteRoomType(long id) {
        var roomType = this.roomTypeRepository.findById(id);
        if (roomType.isPresent()) {
            this.roomTypeRepository.delete(roomType.get());
            return true;
        }
        return false;
    }

    public Room convertToRoom(RoomDTO roomDTO, Room room) throws ParseException {
        if (roomDTO.id != -1)
            room.setId(roomDTO.id);
        room.setName(roomDTO.name);
        room.setOccupancy(roomDTO.occupancy);
        room.setOvercrowding(roomDTO.overcrowding);
        var optionalRoomType = roomTypeRepository.findById(roomDTO.roomType.id);
        if (optionalRoomType.isPresent()) {
            room.setRoomType(optionalRoomType.get());
        } else {
            throw new IllegalArgumentException("The room type used does not exist");
        }
        return room;
    }

    public RoomDTO convertRoomToDTO(Room room) {
        RoomDTO roomDTO = new RoomDTO();
        roomDTO.id = room.getId();
        roomDTO.name = room.getName();
        roomDTO.occupancy = room.getOccupancy();
        roomDTO.overcrowding = room.isOvercrowding();
        roomDTO.roomType = convertRoomTypeToDTO(room.getRoomType());
        return roomDTO;
    }

    public RoomType convertToRoomType(RoomTypeDTO roomTypeDTO, RoomType roomType) throws ParseException {
        if (roomTypeDTO.id != -1)
            roomType.setId(roomTypeDTO.id);
        roomType.setName(roomTypeDTO.name);
        roomType.setPriceBase(roomTypeDTO.priceBase);
        roomType.setPriceOvercrowding(roomTypeDTO.priceOvercrowding);
        return roomType;
    }

    public RoomTypeDTO convertRoomTypeToDTO(RoomType roomType) {
        RoomTypeDTO roomTypeDTO = new RoomTypeDTO();
        roomTypeDTO.id = roomType.getId();
        roomTypeDTO.name = roomType.getName();
        roomTypeDTO.priceBase = roomType.getPriceBase();
        roomTypeDTO.priceOvercrowding = roomType.getPriceOvercrowding();
        roomTypeDTO.quantity = roomRepository.findAllByRoomType(roomType).size();
        roomTypeDTO.quantityOvercrowding = roomRepository.findAllByRoomTypeAndOvercrowding(roomType, true).size();
        return roomTypeDTO;
    }
    //Same but for archive
    public RoomTypeDTO convertRoomTypeToDTO(ArchivedRoomType roomType) {
        RoomTypeDTO roomTypeDTO = new RoomTypeDTO();
        roomTypeDTO.id = roomType.getId();
        roomTypeDTO.name = roomType.getName();
        roomTypeDTO.priceBase = roomType.getPriceBase();
        roomTypeDTO.priceOvercrowding = roomType.getPriceOvercrowding();
        //roomTypeDTO.quantity = roomRepository.findAllByRoomType(roomType).size();
        //roomTypeDTO.quantityOvercrowding = roomRepository.findAllByRoomTypeAndOvercrowding(roomType, true).size();
        return roomTypeDTO;
    }

}
