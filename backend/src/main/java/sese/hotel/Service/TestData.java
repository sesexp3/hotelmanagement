package sese.hotel.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import sese.hotel.Controller.Message.DTO.InvoiceDTO;
import sese.hotel.Model.*;
import sese.hotel.Model.Enum.TaskArea;
import sese.hotel.Repository.*;

import javax.transaction.Transactional;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Component
public class TestData {

    @Autowired
    private RoomReservationRepository roomReservationRepository;

    @Autowired
    private ConsumptionService consumptionService;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    ReservationService reservationService;

    @Autowired
    private InvoiceService invoiceService;

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private ReservationRepository reservationRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private RoomTypeRepository roomTypeRepository;

    @Autowired
    PasswordEncoder encoder;

    public void createDefaultRooms(){
        System.out.println("Creating Default Rooms");


        RoomType dz = new RoomType();
        dz.setName("Doppelzimmer");
        dz.setPriceBase(200);
        dz.setPriceOvercrowding(250);
        //RoomType dzd = new RoomType();
        //dzd.setName("Doppelzimmer Delux");
        //dzd.setPriceBase(350);
        //dzd.setPriceOvercrowding(500);
        RoomType ez = new RoomType();
        ez.setName("Einzelzimmer");
        ez.setPriceBase(120);
        ez.setPriceOvercrowding(190);
        roomTypeRepository.saveAndFlush(dz);
        roomTypeRepository.saveAndFlush(ez);
        //roomTypeRepository.saveAndFlush(dzd);


        Room room = new Room();
        room.setName("Zimmer 101");
        room.setOccupancy(2);
        room.setOvercrowding(true);
        room.setRoomType(dz);
        roomRepository.save(room);
        room = new Room();
        room.setName("Zimmer 102");
        room.setOccupancy(2);
        room.setOvercrowding(true);
        room.setRoomType(dz);
        roomRepository.save(room);
        room = new Room();
        room.setName("Zimmer 103");
        room.setOccupancy(2);
        room.setOvercrowding(false);
        room.setRoomType(dz);
        roomRepository.save(room);

        room = new Room();
        room.setName("Zimmer 104");
        room.setOccupancy(1);
        room.setOvercrowding(true);
        room.setRoomType(ez);
        roomRepository.save(room);
        room = new Room();
        room.setName("Zimmer 105");
        room.setOccupancy(1);
        room.setOvercrowding(true);
        room.setRoomType(ez);
        roomRepository.save(room);


        room = new Room();
        room.setName("Zimmer 201");
        room.setOccupancy(2);
        room.setOvercrowding(false);
        room.setRoomType(dz);
        roomRepository.save(room);
        room = new Room();
        room.setName("Zimmer 202");
        room.setOccupancy(2);
        room.setOvercrowding(false);
        room.setRoomType(dz);
        roomRepository.save(room);

        System.out.println("Done Creating Default Rooms");
    }

    @Transactional
    public void createDefaultInvoice() {
        System.out.println("Creating Default Invoice");

        Reservation reservation = new Reservation();
        Address address = new Address();
        address.setStreet("Teststraße");
        address.setCity("Vienna");
        address.setPostcode("1100");
        address.setNumber("11");
        address.setCountry("Austria");
        //addressRepository.save(address);

        Customer customer = new Customer();
        customer.setFirstName("Max");
        customer.setSurName("Mustermann");
        customer.setName(customer.getFirstName() + " " + customer.getSurName());
        customer.setPassword(encoder.encode("admin"));
        customer.setEmail("max.mustermann@admin.com");
        customer.setPhoneNumber("+43123123123");
        customer.setAddress(address);
        //customerRepository.save(customer);

        reservation.setAddress(address);
        reservation.setArrivalDate(new Date(System.currentTimeMillis()));
        reservation.setDepartureDate(new Date(System.currentTimeMillis() + 86400000));
        reservation.setCustomer(customer);
        reservation.setDiscount(10d);

        //defaults.createDefaultRooms();

        RoomReservation roomReservation = new RoomReservation();
        RoomType roomType = roomTypeRepository.findAll().get(0);
        roomReservation.setCount(3);
        roomReservation.setRoomType(roomType);
        roomReservation.setOvercrowding(false);
        roomReservationRepository.save(roomReservation);

        RoomReservation roomReservation1 = new RoomReservation();
        RoomType roomType1 = roomTypeRepository.findAll().get(1);
        roomReservation1.setCount(1);
        roomReservation1.setRoomType(roomType1);
        roomReservation1.setOvercrowding(true);
        roomReservationRepository.save(roomReservation1);

        reservation.setPrice((roomType.getPriceBase() * 3 + roomType1.getPriceOvercrowding()) * (100. - reservation.getDiscount()) / 100.);

        List<RoomReservation> roomReservations = new ArrayList<>();
        roomReservations.add(roomReservation);
        roomReservations.add(roomReservation1);
        reservation.setRoomReservations(roomReservations);

        PaymentInformation paymentInformation = new PaymentInformation();
        paymentInformation.setCreditCardHolder(customer.getName());
        paymentInformation.setCreditCardNumber("1234123412341234");
        paymentInformation.setCreditCardValidUntilMonth("12");
        paymentInformation.setCreditCardValidUntilYear("25");
        paymentInformation.setCreditCardVerificationNumber("089");
        reservation.setPaymentInformation(paymentInformation);

        reservationRepository.save(reservation);

        InvoiceDTO invoice=invoiceService.createInvoice(reservation.getId());


        Reservation reservation1 = new Reservation();
        Address address1 = new Address();
        address1.setStreet("Planetenstraße");
        address1.setCity("Bergwacht");
        address1.setPostcode("3434");
        address1.setNumber("2");
        address1.setCountry("Austria");
        //addressRepository.save(address);

        Customer customer1 = new Customer();
        customer1.setFirstName("Sarah");
        customer1.setSurName("Heidel");
        customer1.setName(customer1.getFirstName() + " " + customer1.getSurName());
        customer1.setPassword(encoder.encode("admin"));
        customer1.setEmail("sa.he@gmail.com");
        customer1.setPhoneNumber("+436763898745");
        customer1.setAddress(address1);
        //customerRepository.save(customer);

        reservation1.setAddress(address1);
        reservation1.setArrivalDate(Date.valueOf("2021-01-30"));
        reservation1.setDepartureDate(Date.valueOf("2021-01-31"));
        reservation1.setCustomer(customer1);
        reservation1.setDiscount(0d);

        //defaults.createDefaultRooms();

        RoomReservation roomReservation2 = new RoomReservation();
        RoomType roomType2 = roomTypeRepository.findAll().get(0);
        roomReservation2.setCount(1);
        roomReservation2.setRoomType(roomType2);
        roomReservation2.setOvercrowding(false);
        roomReservationRepository.save(roomReservation2);

        reservation1.setPrice((roomType2.getPriceBase()) * 1/*days*/ * (100. - reservation1.getDiscount()) / 100.);

        List<RoomReservation> roomReservations1 = new ArrayList<>();
        roomReservations1.add(roomReservation2);
        reservation1.setRoomReservations(roomReservations1);

        PaymentInformation paymentInformation1 = new PaymentInformation();
        paymentInformation.setCreditCardHolder(customer1.getName());
        paymentInformation.setCreditCardNumber("2988533422221987");
        paymentInformation.setCreditCardValidUntilMonth("07");
        paymentInformation.setCreditCardValidUntilYear("24");
        paymentInformation.setCreditCardVerificationNumber("495");
        reservation1.setPaymentInformation(paymentInformation1);

        reservationRepository.save(reservation1);

        InvoiceDTO invoice1=invoiceService.createInvoice(reservation1.getId());

        System.out.println("Done Creating Default Invoice");
    }

    public void createDefaultEmployee(){
        System.out.println("Creating Default User");
        Employee employee = new Employee();
        employee.setPassword(encoder.encode("admin"));
        employee.setName("Test User");
        employee.setEmail("admin@admin.com");
        employee.setMinutesPerWeek(1000);
        employee.setIban("asdasd");
        employee.setEmployeeCode("asd");
        employee.setDateOfBirth(Date.valueOf("2020-01-01"));
        employee.setPhoneNumber("1111");
        employee.setInsuranceNumber("11111");
        employee.setSex("M");
        employee.setTaskArea(TaskArea.OTHERS);

        Address address = new Address();
        address.setStreet("asd");
        address.setCity("asda");
        address.setPostcode("11");
        address.setNumber("11");
        address.setCountry("asda");
        employee.setAddress(address);
        employeeRepository.save(employee);

        Employee employee2 = new Employee();
        employee2.setPassword(encoder.encode("admin2"));
        employee2.setName("Test User");
        employee2.setEmail("admin2@admin.com");
        employee2.setMinutesPerWeek(1000);
        employee2.setIban("asdasd");
        employee2.setEmployeeCode("asd2");
        employee2.setDateOfBirth(Date.valueOf("2020-01-01"));
        employee2.setPhoneNumber("1111");
        employee2.setInsuranceNumber("11111");
        employee2.setSex("M");
        employee2.setTaskArea(TaskArea.OTHERS);

        Address address2 = new Address();
        address2.setStreet("asd");
        address2.setCity("asda");
        address2.setPostcode("11");
        address2.setNumber("11");
        address2.setCountry("asda");
        employee2.setAddress(address2);
        employeeRepository.save(employee2);

        System.out.println("Done Creating Default User");
        System.out.println("Default Login: admin@admin.com PW: admin");
    }

    public void createDefaultReservation() {
        Reservation reservation = new Reservation();
        Address address = new Address();
        address.setStreet("Bergweg");
        address.setCity("Hinterkreuz");
        address.setPostcode("4564");
        address.setNumber("75");
        address.setCountry("Austria");
        //addressRepository.save(address);

        Customer customer = new Customer();
        customer.setFirstName("Georg");
        customer.setSurName("Rust");
        customer.setName(customer.getFirstName() + " " + customer.getSurName());
        customer.setPassword(encoder.encode("admin"));
        customer.setEmail("georg.rust@yahoo.com");
        customer.setPhoneNumber("+4318765982");
        customer.setAddress(address);
        //customerRepository.save(customer);

        reservation.setAddress(address);
        reservation.setArrivalDate(Date.valueOf("2021-01-28"));
        reservation.setDepartureDate(Date.valueOf("2021-02-01"));
        reservation.setCustomer(customer);
        reservation.setDiscount(0d);

        //defaults.createDefaultRooms();

        RoomReservation roomReservation = new RoomReservation();
        RoomType roomType = roomTypeRepository.findAll().get(0);
        roomReservation.setCount(2);
        roomReservation.setRoomType(roomType);
        roomReservation.setOvercrowding(false);
        roomReservationRepository.save(roomReservation);

        RoomReservation roomReservation1 = new RoomReservation();
        RoomType roomType1 = roomTypeRepository.findAll().get(1);
        roomReservation1.setCount(2);
        roomReservation1.setRoomType(roomType1);
        roomReservation1.setOvercrowding(false);
        roomReservationRepository.save(roomReservation1);

        reservation.setPrice((roomType.getPriceBase() * 2 + roomType1.getPriceBase()*2) * 4/*days*/ * (100. - reservation.getDiscount()) / 100.);

        List<RoomReservation> roomReservations = new ArrayList<>();
        roomReservations.add(roomReservation);
        roomReservations.add(roomReservation1);
        reservation.setRoomReservations(roomReservations);

        PaymentInformation paymentInformation = new PaymentInformation();
        paymentInformation.setCreditCardHolder(customer.getName());
        paymentInformation.setCreditCardNumber("2847365901887362");
        paymentInformation.setCreditCardValidUntilMonth("01");
        paymentInformation.setCreditCardValidUntilYear("25");
        paymentInformation.setCreditCardVerificationNumber("309");
        reservation.setPaymentInformation(paymentInformation);

        reservationRepository.save(reservation);
    }


}