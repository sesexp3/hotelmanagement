package sese.hotel.Service;

import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class UserService {

    private HashMap<String, String> loggedInUserToken = new HashMap<>();

    public void storeToken(String email,String token) {
            loggedInUserToken.put(email, token);
    }
    public boolean logout(String email) {
        return loggedInUserToken.remove(email) != null;
    }

    public String getToken(String username) {
        return this.loggedInUserToken.getOrDefault(username, null);
    }

    public void setToken(String username, String newToken) {
        this.loggedInUserToken.put(username, newToken);
    }


}
