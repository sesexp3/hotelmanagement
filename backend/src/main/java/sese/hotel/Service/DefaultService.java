package sese.hotel.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import sese.hotel.Controller.Message.DTO.InvoiceDTO;
import sese.hotel.Controller.Message.Response.ResponseMessage;
import sese.hotel.Model.*;
import sese.hotel.Model.Enum.TaskArea;
import sese.hotel.Repository.*;
import sese.hotel.Service.ReservationService;

import javax.transaction.Transactional;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Component
@Profile("!test")
public class DefaultService implements ApplicationListener<ApplicationReadyEvent> {

    @Autowired
    TestData testData;

    @Autowired
    private ConsumptionService consumptionService;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        testData.createDefaultRooms();
        //
        testData.createDefaultEmployee();
        testData.createDefaultInvoice();
        testData.createDefaultReservation();
        try {
            consumptionService.createBuffer();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
