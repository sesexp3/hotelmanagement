package sese.hotel.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sese.hotel.Controller.Message.DTO.*;
import sese.hotel.Model.*;
import sese.hotel.Model.Archive.ArchivedReservation;
import sese.hotel.Model.Archive.ArchivedRoomReservation;
import sese.hotel.Repository.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
public class ReservationService {
    @Autowired
    private ReservationRepository reservationRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private RoomReservationRepository roomReservationRepository;

    @Autowired
    private RoomTypeRepository roomTypeRepository;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private RoomService roomService;

    @Autowired
    private AddressRepository addressRepository;

    public void addReservation(ReservationDTO reservationDTO) throws ParseException {
        Reservation reservation = new Reservation();
        reservationDTO.customerId = "";
        reservation = this.convertToReservation(reservationDTO, reservation);
        System.out.println(reservation);
        reservationRepository.save(reservation);
    }

    public List<ReservationDTO> findAll() {
        var reservations = this.reservationRepository.findAll();
        var reservationDTOs = reservations
                .stream()
                .map(reservation -> convertReservationToDTO(reservation))
                .collect(Collectors.toList());
        return reservationDTOs;
    }

    public void updateReservation(long id, ReservationDTO dto) throws ParseException {
        var reservation = this.reservationRepository.findById(id);
        if (reservation.isPresent()) {
            // Update entity
            var reservationEntity = reservation.get();
            this.convertToReservation(dto, reservationEntity);
            System.out.println(dto);
            System.out.println(reservationEntity);
            this.reservationRepository.saveAndFlush(reservationEntity);
            System.out.println(this.reservationRepository.findById(id));
        } else {
            throw new IllegalArgumentException("The reservation to be updated does not exist");
        }
    }

    public boolean deleteReservation(long id) {
        var reservation = this.reservationRepository.findById(id);
        if (reservation.isPresent()) {
            this.reservationRepository.delete(reservation.get());
            return true;
        }
        return false;
    }

    public RoomReservationStatsDTO getStats() {

        RoomReservationStatsDTO result = new RoomReservationStatsDTO();
        result.stats = new ArrayList<>();

        // Get all reservations for each room.
        HashMap<String, List<Reservation>> roomReservationsHashMap = new HashMap<>();
        var reservations = this.reservationRepository.findAll();
        reservations.forEach((reservation -> {
            var roomReservationOfReservation = reservation.getRoomReservations();
            roomReservationOfReservation.forEach(roomReservation ->
            {
                var roomtype = roomReservation.getRoomType().getName();
                var entryList = roomReservationsHashMap.getOrDefault(roomtype, new ArrayList<Reservation>());
                entryList.add(reservation);
                roomReservationsHashMap.put(roomtype, entryList);
            });
        }));

        // Get all dates for next 30 days
        LocalDate startDate = LocalDate.now();
        LocalDate endDate = startDate.plusMonths(1);

        List<LocalDate> listOfDates = startDate
                .datesUntil(endDate)
                .collect(Collectors
                        .toList());

        // Iterate through all roomtypes
        var roomtypes = roomReservationsHashMap.keySet();
        for (var roomtype: roomtypes) {
            var roomTypeStats = new RoomTypeStatsDTO();
            roomTypeStats.occupancies = new ArrayList<>();
            roomTypeStats.roomType = roomtype;

            for (var date: listOfDates) {
                // Calculate reservations for date
                int reservationsOnDay = 0;
                var reservationsOfRoomType = roomReservationsHashMap.get(roomtype);
                for (var r: reservationsOfRoomType) {
                    if (
                            (
                                    r.getArrivalDate().toLocalDate().isBefore(date) ||
                                            r.getArrivalDate().toLocalDate().isEqual(date)
                            )
                            && (
                                    r.getDepartureDate().toLocalDate().isAfter(date) ||
                                            r.getDepartureDate().toLocalDate().isEqual(date)
                            )
                    )
                    {
                        var roomReservationsOfRoomType = r.getRoomReservations()
                                .stream()
                                .filter((reservation) -> reservation.getRoomType().getName().equals(roomtype))
                                .collect(Collectors.toList());
                        for (var a: roomReservationsOfRoomType) {
                            reservationsOnDay = reservationsOnDay + a.getCount();
                        }
                    }
                }

                var occupancy = new DailyOccupancyDTO(date, reservationsOnDay);
                roomTypeStats.occupancies.add(occupancy);
            }
            result.stats.add(roomTypeStats);
        }
        return result;
    }

    public ReservationDTO checkReservation(ReservationDTO reservationDTO) throws ParseException {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"); // Quoted "Z" to indicate UTC, no timezone offset
        df.setTimeZone(tz);
        var arrivalDate_Parse = df.parse(reservationDTO.arrival_date);
        var departureDate_Parse = df.parse(reservationDTO.departure_date);
        var arrivalDate = new java.sql.Date(arrivalDate_Parse.getTime());
        var departureDate = new java.sql.Date(departureDate_Parse.getTime());
        long daysBetween = Duration.between(arrivalDate.toLocalDate().atStartOfDay(), departureDate.toLocalDate().atStartOfDay()).toDays();

        List<Reservation> reservations = reservationRepository.findAll();
        reservations = reservations.stream().filter(reservation ->
                !(reservation.getArrivalDate().after(departureDate) || reservation.getDepartureDate().before(arrivalDate))
                        && reservation.getId() != reservationDTO.id)
                .collect(Collectors.toList());
        List<Reservation> finalReservations = reservations;
        List<RoomReservationDTO> returnRoomReservationDTOs = new ArrayList<>();
        AtomicBoolean valid = new AtomicBoolean(true);
        AtomicReference<Double> price = new AtomicReference<>((double) 0);
        reservationDTO.rooms.forEach(roomReservationDTO -> {
            var optRoomRes = roomTypeRepository.findById(roomReservationDTO.room_type.id);
            if (optRoomRes.isPresent()) {
                RoomType roomType = optRoomRes.get();
                int maxRooms;
                if (roomReservationDTO.overcrowding)
                    maxRooms = roomRepository.findAllByRoomTypeAndOvercrowding(roomType, true).size();
                else
                    maxRooms = roomRepository.findAllByRoomType(roomType).size();
                int curRoomMaxRooms = maxRooms;
                for (LocalDate date = arrivalDate.toLocalDate(); date.isBefore(departureDate.toLocalDate()); date = date.plusDays(1)) {
                    AtomicInteger curDayMaxRooms = new AtomicInteger(maxRooms);
                    LocalDate finalDate = date;
                    List<Reservation> curReservation = finalReservations.stream().filter(reservation ->
                            reservation.getArrivalDate().toLocalDate().isBefore(finalDate.plusDays(1)) &&
                                    reservation.getDepartureDate().toLocalDate().isAfter(finalDate)
                    ).collect(Collectors.toList());
                    curReservation.forEach(
                            reservation -> reservation.getRoomReservations().forEach(
                                    roomReservation -> {
                                        if (roomReservation.getRoomType().getId().equals(roomType.getId()) &&
                                                (!roomReservationDTO.overcrowding || (roomReservationDTO.overcrowding && roomReservation.isOvercrowding()))) {
                                            curDayMaxRooms.addAndGet(-roomReservation.getCount());
                                        }
                                    }
                            ));
                    if (curDayMaxRooms.get() < curRoomMaxRooms)
                        curRoomMaxRooms = curDayMaxRooms.get();
                }
                var roomRes = new RoomReservationDTO();
                roomRes.overcrowding = roomReservationDTO.overcrowding;
                roomRes.count = curRoomMaxRooms;
                roomRes.room_type = roomReservationDTO.room_type;
                returnRoomReservationDTOs.add(roomRes);

                if (roomReservationDTO.overcrowding)
                    price.updateAndGet(v -> new Double((double) (v + (roomReservationDTO.count * daysBetween * roomType.getPriceOvercrowding()))));
                else
                    price.updateAndGet(v -> new Double((double) (v + (roomReservationDTO.count * daysBetween * roomType.getPriceBase()))));

                if (curRoomMaxRooms < roomReservationDTO.count)
                    valid.set(false);

            }
        });
        reservationDTO.price = price.get() * (100 - reservationDTO.discount) / 100.0;
        if (!valid.get())
            reservationDTO.rooms = returnRoomReservationDTOs;
        return reservationDTO;
    }

    private Reservation convertToReservation(ReservationDTO reservationDTO, Reservation reservation) throws ParseException {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"); // Quoted "Z" to indicate UTC, no timezone offset
        df.setTimeZone(tz);
        var arrivalDate = df.parse(reservationDTO.arrival_date);
        var departureDate = df.parse(reservationDTO.departure_date);
        reservation.setArrivalDate(
                new java.sql.Date(arrivalDate.getTime())
        );
        reservation.setDepartureDate(
                new java.sql.Date(departureDate.getTime())
        );

        reservation.setPrice(reservationDTO.price);
        reservation.setDiscount(reservationDTO.discount);
        // Check if customer exists
        if (reservationDTO.customerId.isEmpty()) {
            // No customer exists, Create new customer
            Customer reservationCustomer = new Customer();
            reservationCustomer.setFirstName(reservationDTO.firstName);
            reservationCustomer.setSurName(reservationDTO.lastName);
            reservationCustomer.setName(reservationDTO.firstName + " " + reservationDTO.lastName);
            reservationCustomer.setEmail(reservationDTO.email);
            reservationCustomer.setPhoneNumber(reservationDTO.tel);

            Address address = new Address();
            address.setCity(reservationDTO.city);
            address.setPostcode(reservationDTO.postcode);
            address.setStreet(reservationDTO.address);

            //atm these are never used
            address.setCountry("");
            address.setNumber("");
            addressRepository.save(address);
            reservationCustomer.setAddress(address);

            customerRepository.save(reservationCustomer);
            reservation.setCustomer(reservationCustomer);
        } else {
            // Customer exists
            var customer = customerRepository.findById(Long.valueOf(reservationDTO.customerId));
            if (customer.isPresent()) {
                Customer reservationCustomer = customer.get();
                reservationCustomer.setFirstName(reservationDTO.firstName);
                reservationCustomer.setSurName(reservationDTO.lastName);
                reservationCustomer.setName(reservationDTO.firstName + " " + reservationDTO.lastName);
                reservationCustomer.setEmail(reservationDTO.email);
                reservationCustomer.setPhoneNumber(reservationDTO.tel);

                //TODO: copy paste from above, should be unified
                Address address = new Address();
                address.setCity(reservationDTO.city);
                address.setPostcode(reservationDTO.postcode);
                address.setStreet(reservationDTO.address);

                //atm these are never used
                address.setCountry("");
                address.setNumber("");
                addressRepository.save(address);
                reservationCustomer.setAddress(address);

                customerRepository.save(reservationCustomer);
                reservation.setCustomer(reservationCustomer);
            } else {
                throw new IllegalArgumentException("Invalid ReservationDTO " +
                        reservationDTO.customerId + " Provided customer ID does not exist.");
            }
        }

        // Reserved rooms
        if (reservation.getRoomReservations() != null) {
            reservation.getRoomReservations().forEach(roomReservation -> roomReservationRepository.delete(roomReservation));
        }
        List<RoomReservation> roomReservations = reservationDTO.rooms.stream()
                .map(room -> convertToRoomReservation(room)).collect(Collectors.toList());
        roomReservations.forEach(roomReservation -> {
            roomReservationRepository.save(roomReservation);
        });
        reservation.setRoomReservations(roomReservations);

        reservation.setPaymentInformation(convertToPaymentInformation(reservationDTO.paymentInformation));

        // Create address entity
        // TODO: Adjust reservation dto in frontend to match address object in backend
        // TODO: (Bernhard) This is super strange and awkward, double city, and why does reservation have
        // an address field in addition to the customer???
        Address address = new Address();
        address.setCity(reservationDTO.city);
        address.setPostcode(reservationDTO.postcode);
        address.setStreet(reservationDTO.address);
        address.setCity(reservationDTO.city);
        reservation.setAddress(address);
        return reservation;
    }

    public ReservationDTO convertReservationToDTO(Reservation reservation) {
        var dto = new ReservationDTO();
        dto.id = reservation.getId();
        dto.customerId = reservation.getCustomer().getId().toString();
        dto.customer = reservation.getCustomer().getName();
        dto.city = reservation.getAddress().getCity();
        dto.address = reservation.getAddress().getStreet();
        dto.discount = reservation.getDiscount();
        dto.price = reservation.getPrice();
        TimeZone tz = TimeZone.getTimeZone("CET");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); // Quoted "Z" to indicate UTC, no timezone offset
        DateFormat df2 = new SimpleDateFormat("HH:mm:ss.SSS"); // Quoted "Z" to indicate UTC, no timezone offset
        df.setTimeZone(tz);
        df.setTimeZone(tz);
        dto.arrival_date = df.format(reservation.getArrivalDate()) + "T" + df2.format(reservation.getArrivalDate()) + "Z";
        dto.departure_date = df.format(reservation.getDepartureDate()) + "T" + df2.format(reservation.getDepartureDate()) + "Z";
        LocalDate arrivalLocalDate = reservation.getArrivalDate().toLocalDate();
        LocalDate departureLocalDate = reservation.getDepartureDate().toLocalDate();
        dto.num_stays_days = Period.between(arrivalLocalDate, departureLocalDate).getDays();
        dto.email = reservation.getCustomer().getEmail();
        dto.postcode = reservation.getAddress().getPostcode();
        dto.firstName = reservation.getCustomer().getFirstName();
        dto.lastName = reservation.getCustomer().getSurName();
        dto.tel = reservation.getCustomer().getPhoneNumber();
        dto.rooms = reservation.getRoomReservations().stream().map(
                roomReservation -> convertRoomReservationToDTO(roomReservation)
        ).collect(Collectors.toList());
        dto.paymentInformation = convertPaymentInformationToDTO(reservation.getPaymentInformation());
        return dto;
    }
    //same but for archived Reservations
    public ReservationDTO convertReservationToDTO(ArchivedReservation reservation) {
        var dto = new ReservationDTO();
        dto.id = reservation.getId();
        dto.customerId = reservation.getCustomer().getId().toString();
        dto.customer = reservation.getCustomer().getName();
        dto.city = reservation.getAddress().getCity();
        dto.address = reservation.getAddress().getStreet();
        dto.discount = reservation.getDiscount();
        dto.price = reservation.getPrice();
        TimeZone tz = TimeZone.getTimeZone("CET");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); // Quoted "Z" to indicate UTC, no timezone offset
        DateFormat df2 = new SimpleDateFormat("HH:mm:ss.SSS"); // Quoted "Z" to indicate UTC, no timezone offset
        df.setTimeZone(tz);
        df.setTimeZone(tz);
        dto.arrival_date = df.format(reservation.getArrivalDate()) + "T" + df2.format(reservation.getArrivalDate()) + "Z";
        dto.departure_date = df.format(reservation.getDepartureDate()) + "T" + df2.format(reservation.getDepartureDate()) + "Z";
        LocalDate arrivalLocalDate = reservation.getArrivalDate().toLocalDate();
        LocalDate departureLocalDate = reservation.getDepartureDate().toLocalDate();
        dto.num_stays_days = Period.between(arrivalLocalDate, departureLocalDate).getDays();
        dto.email = reservation.getCustomer().getEmail();
        dto.postcode = reservation.getAddress().getPostcode();
        dto.firstName = reservation.getCustomer().getFirstName();
        dto.lastName = reservation.getCustomer().getSurName();
        dto.tel = reservation.getCustomer().getPhoneNumber();
        dto.rooms = reservation.getRoomReservations().stream().map(
                roomReservation -> convertRoomReservationToDTO(roomReservation)
        ).collect(Collectors.toList());
        //dto.paymentInformation = convertPaymentInformationToDTO(reservation.getPaymentInformation());
        return dto;
    }

    public RoomReservationDTO convertRoomReservationToDTO(RoomReservation roomReservation) {
        RoomReservationDTO roomReservationDTO = new RoomReservationDTO();
        roomReservationDTO.room_type = roomService.convertRoomTypeToDTO(roomReservation.getRoomType());
        roomReservationDTO.count = roomReservation.getCount();
        roomReservationDTO.overcrowding = roomReservation.isOvercrowding();
        return roomReservationDTO;
    }
    //same but for archive
    public RoomReservationDTO convertRoomReservationToDTO(ArchivedRoomReservation roomReservation) {
        RoomReservationDTO roomReservationDTO = new RoomReservationDTO();
        roomReservationDTO.room_type = roomService.convertRoomTypeToDTO(roomReservation.getRoomType());
        roomReservationDTO.count = roomReservation.getCount();
        roomReservationDTO.overcrowding = roomReservation.isOvercrowding();
        return roomReservationDTO;
    }

    public RoomReservation convertToRoomReservation(RoomReservationDTO dto) {
        RoomReservation roomReservation = new RoomReservation();
        Optional<RoomType> roomType = roomTypeRepository.findById(dto.room_type.id);
        if (roomType.isPresent())
            roomReservation.setRoomType(roomType.get());
        else
            throw new IllegalArgumentException("The room type used does not exist");
        roomReservation.setCount(dto.count);
        roomReservation.setOvercrowding(dto.overcrowding);
        return roomReservation;
    }

    public static PaymentInformation convertToPaymentInformation(PaymentInformationDTO paymentInformationDTO) {
        PaymentInformation paymentInformation = new PaymentInformation();
        paymentInformation.setCreditCardHolder(paymentInformationDTO.getCreditCardHolder());
        paymentInformation.setCreditCardNumber(paymentInformationDTO.getCreditCardNumber());
        paymentInformation.setCreditCardValidUntilMonth(paymentInformationDTO.getCreditCardValidUntilMonth());
        paymentInformation.setCreditCardValidUntilYear(paymentInformationDTO.getCreditCardValidUntilYear());
        paymentInformation.setCreditCardVerificationNumber(paymentInformationDTO.getCreditCardVerificationNumber());
        return paymentInformation;
    }

    public static PaymentInformationDTO convertPaymentInformationToDTO(PaymentInformation paymentInformation) {
        PaymentInformationDTO paymentInformationDTO = new PaymentInformationDTO();

        paymentInformationDTO.setCreditCardHolder(paymentInformation.getCreditCardHolder());
        paymentInformationDTO.setCreditCardNumber(paymentInformation.getCreditCardNumber());
        paymentInformationDTO.setCreditCardValidUntilMonth(paymentInformation.getCreditCardValidUntilMonth());
        paymentInformationDTO.setCreditCardValidUntilYear(paymentInformation.getCreditCardValidUntilYear());
        paymentInformationDTO.setCreditCardVerificationNumber(paymentInformation.getCreditCardVerificationNumber());
        return paymentInformationDTO;
    }



}
