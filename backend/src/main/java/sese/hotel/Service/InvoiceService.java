package sese.hotel.Service;

import com.lowagie.text.DocumentException;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;
import sese.hotel.Controller.Message.DTO.InvoiceDTO;
import sese.hotel.Model.*;
import sese.hotel.Model.Archive.ArchivedReservation;
import sese.hotel.Model.Archive.ArchivedRoomReservation;
import sese.hotel.Model.Archive.ArchivedRoomType;
import sese.hotel.Model.ConsumptionAPI.Consumption;
import sese.hotel.Repository.*;

import javax.transaction.Transactional;
import java.beans.Transient;
import java.io.*;
import java.sql.Date;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;

@Service
public class InvoiceService {

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private BlobRepository blobRepository;

    @Autowired
    private ReservationRepository reservationRepository;

    @Autowired
    private InvoiceLineRepository invoiceLineRepository;

    @Autowired
    private ArchivedRoomReservationRepository archivedRoomReservationRepository;

    @Autowired
    private ArchivedRoomTypeRepository archivedRoomTypeRepository;

    @Autowired
    private ArchivedReservationRepository archivedReservationRepository;

    @Autowired
    private ConsumptionService consumptionService;

    @Autowired
    private ReservationService reservationService;

    @Transactional
    public void revokeCancellation (Long invoiceNumber){
        Invoice invoice = invoiceRepository.findByInvoiceNumber(invoiceNumber);
        invoice.setCancellationDate(null);
        invoice.setCancelledFrom("");
        invoiceRepository.saveAndFlush(invoice);
    }

    //This makes sense once we have proper invoice management
    /*public Invoice cancelInvoice(Long invoiceNumber) {
        Invoice invoice = invoiceRepository.findByInvoiceNumber(invoiceNumber);
        invoice.setCancellationDate(new Date(System.currentTimeMillis()));
        invoiceRepository.save(invoice);
        return invoice;
    }*/

    @Transactional
    public InvoiceDTO createInvoice(long reservationId){
        System.out.println(reservationId);
        Invoice invoice = getInvoiceForReservation(reservationId);
        int running = 0;
        if(invoice!=null) {
            //invalidate old invoice
            this.cancelInvoice(invoice.getInvoiceNumber());
            try {
                invoice.setBlob(createPDFInvoice(invoice));
            } catch (IOException | DocumentException e) {
                e.printStackTrace();
            }
            running = invoice.getRunningNumber();
        }
        //create new

        Invoice invoiceNew = new Invoice();

        Optional<Reservation> reservation = reservationRepository.findById(reservationId);
        if(reservation.isEmpty()){
            return null;
        }
        System.out.println("Reservation: "+reservation);
        invoiceNew.setReservation(convert(reservation.get()));
        invoiceNew.setRunningNumber(running+1);
        invoiceNew.setInvoiceNumber(Long.parseLong(""+reservation.get().getId()+running));
        invoiceNew.setOriginalReservationNumber(Math.toIntExact(reservationId));
        invoiceNew.setInvoiceDate(new Date(System.currentTimeMillis()));

        List<InvoiceLine> invoiceLines = new ArrayList<>();
        int line = 0;

        System.out.println("RoomReservations: "+reservation.get().getRoomReservations());
        for(RoomReservation e:reservation.get().getRoomReservations()){
            InvoiceLineRoom invoiceLine = new InvoiceLineRoom();
            invoiceLine.setRoomReservation(convert(e));
            //invoiceLine.setAmount(e.getCount());
            invoiceLine.setLineNumber(line);
            invoiceLine.setInvoice(invoiceNew);
            invoiceLines.add(invoiceLine);
            invoiceLineRepository.save(invoiceLine);
            line+=1;
        }

        Consumption consumption=consumptionService.getConsumption();
            for (sese.hotel.Model.ConsumptionAPI.Invoice e : consumption.getCustomer().getInvoices()) {
                InvoiceLineConsumption invoiceLine = new InvoiceLineConsumption();
                invoiceLine.setLineNumber(line);
                invoiceLine.setTotalPrice(e.getInvoice().getTotal());
                invoiceLine.setInvoice(invoiceNew);
                invoiceLines.add(invoiceLine);
                invoiceLineRepository.save(invoiceLine);
                line += 1;
            }
        invoiceNew.setLineList(invoiceLines);

        try {
            invoiceNew.setBlob(createPDFInvoice(invoiceNew));
        } catch (IOException | DocumentException e) {
            e.printStackTrace();
        }
        invoiceRepository.saveAndFlush(invoiceNew);

        InvoiceDTO invoiceDTO = new InvoiceDTO();
        invoiceDTO.setInvoiceNumber(invoiceNew.getInvoiceNumber());
        invoiceDTO.setInvoiceCancellationDate(invoiceNew.getCancellationDate());
        invoiceDTO.setId(invoiceNew.getId());
        invoiceDTO.setCancelledFrom(invoiceNew.getCancelledFrom());
        invoiceDTO.setInvoiceDate(invoiceNew.getInvoiceDate());
        double grandTotal = 0;
        for(InvoiceLine i:invoiceNew.getLineList()){
            grandTotal+=i.getTotal();
        }
        invoiceDTO.setAmount(grandTotal);
        return invoiceDTO;

    }

    //Returns latest invoice
    public Invoice getInvoiceForReservation(long reserverationId) {
        Optional<Reservation>reservation = reservationRepository.findById(reserverationId);
        if(reservation.isEmpty()){
            return null;
        }
        List<Invoice> invoices = invoiceRepository.findByOriginalReservationNumber(Math.toIntExact(reservation.get().getId()));
        if(invoices.isEmpty()){
            return null;
        }
        if(invoices.size()==1){
            return invoices.get(0);
        }
        return Collections.max(invoices, Comparator.comparing(Invoice::getRunningNumber));
    }

    public List<InvoiceDTO> getAllInvoices() {
        List<Invoice> invoices = invoiceRepository.findAll();
        List<InvoiceDTO> invoiceDTOS = new ArrayList<>();

        for(Invoice e:invoices){
            InvoiceDTO invoiceDTO = convertInvoiceToDTO(e);
            invoiceDTOS.add(invoiceDTO);
        }

        return invoiceDTOS;
    }

    @Transactional
    public InvoiceDTO cancelInvoice(Long invoiceNumber) {
        Invoice invoice = invoiceRepository.findByInvoiceNumber(invoiceNumber);
        if(invoice==null){
            return null;
        }
        invoice.setCancellationDate(new Date(System.currentTimeMillis()));
        invoice.setCancelledFrom("admin"); //TODO
        invoiceRepository.saveAndFlush(invoice);

        return this.getInvoiceForInvoiceNumber(invoiceNumber);
    }

    @Transactional
    public InvoiceDTO getInvoiceForInvoiceNumber(long invoiceNumber) {
        Invoice invoice = invoiceRepository.findByInvoiceNumber(invoiceNumber);
        if(invoice==null){
            return null;
        }
        InvoiceDTO invoiceDTO = convertInvoiceToDTO(invoice);
        return invoiceDTO;
    }

    @Transactional
    public String getOrCreateInvoicePdfForInvoiceNumber(long invoiceNumber) throws IOException, DocumentException {

        Invoice invoice = invoiceRepository.findByInvoiceNumber(invoiceNumber);
        if (invoice == null) return null;
        if(invoice.getBlob()!=null){
            return convertByteArrayToBase64String(invoice.getBlob().getData());
        }
        else{
            BlobEntity entity = this.createPDFInvoice(invoice);
            return convertByteArrayToBase64String(entity.getData());
        }
    }

    private BlobEntity createPDFInvoice(Invoice invoice) throws IOException, DocumentException {
        String html = parseThymeleafTemplate(invoice);
        return generatePdfFromHtml(html);
    }

    private String parseThymeleafTemplate(Invoice invoice) {
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode(TemplateMode.HTML);

        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);


        Context context = new Context();
        if(invoice.getCancellationDate()!=null){

        }
        String streetAddress = "";

        if(invoice.getReservation().getAddress().getNumber()!=null){
            streetAddress = invoice.getReservation().getAddress().getStreet() +" "+invoice.getReservation().getAddress().getNumber();
        }
        else{
            streetAddress = invoice.getReservation().getAddress().getStreet();
        }

        context.setVariable("InvoiceHeaderText", "Rechnung");
        context.setVariable("InvoiceDate", invoice.getInvoiceDate());
        context.setVariable("InvoiceCustomerName", invoice.getReservation().getCustomer().getFirstName()+" "+invoice.getReservation().getCustomer().getSurName());
        context.setVariable("InvoiceCustomerPostcode", invoice.getReservation().getAddress().getPostcode());
        context.setVariable("InvoiceCustomerCity", invoice.getReservation().getAddress().getCity());
        context.setVariable("InvoiceCustomerCountry", invoice.getReservation().getAddress().getCountry());
        context.setVariable("InvoiceCustomerEmail", invoice.getReservation().getCustomer().getEmail());
        context.setVariable("InvoiceCustomerStreet", streetAddress);



        List<String> headers = Arrays.asList("Service", "Typ", "Preis", "Rabatt","Anzahl","Gesamt");
        List<Map<String, Object>> rows = new ArrayList<>();
        double grandTotal=0;
        //DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.GERMAN);
        //DecimalFormat df = new DecimalFormat("#,##",symbols);
         for(InvoiceLine e:invoice.getLineList()){
             if(e.getClass()==InvoiceLineRoom.class){
                 InvoiceLineRoom temp = (InvoiceLineRoom)e;

                 if(temp.getRoomReservation().isOvercrowding()){
                     double tempSum = temp.getRoomReservation().getRoomType().getPriceOvercrowding()*temp.getRoomReservation().getCount()*((100-invoice.getReservation().getDiscount())/100);
                     tempSum =  Math.round(tempSum * 100.0) / 100.0;
                     grandTotal += tempSum;
                     rows.add(Map.of("Service", "Zimmer", "Typ", temp.getRoomReservation().getRoomType().getName()+" +Überbelegung", "Preis", Math.round(temp.getRoomReservation().getRoomType().getPriceOvercrowding() * 100.0) / 100.0, "Rabatt", invoice.getReservation().getDiscount() + " %", "Anzahl", temp.getRoomReservation().getCount(),"Gesamt", tempSum));

                 }
                 else{
                     double tempSum = temp.getRoomReservation().getRoomType().getPriceBase()*temp.getRoomReservation().getCount()*((100-invoice.getReservation().getDiscount())/100);
                     tempSum = Math.round(tempSum * 100.0) / 100.0;
                     grandTotal += tempSum;
                     rows.add(Map.of("Service", "Zimmer", "Typ", temp.getRoomReservation().getRoomType().getName(), "Preis", Math.round(temp.getRoomReservation().getRoomType().getPriceBase() * 100.0) / 100.0, "Rabatt", invoice.getReservation().getDiscount() + " %", "Anzahl", temp.getRoomReservation().getCount(),"Gesamt", tempSum));

                 }

             }
             if(e.getClass()==InvoiceLineConsumption.class){
                 InvoiceLineConsumption temp = (InvoiceLineConsumption)e;
                 grandTotal += temp.getTotalPrice();
                 rows.add(Map.of("Service", "Konsumation", "Typ", "Restaurant", "Preis",Math.round(temp.getTotalPrice() * 100.0) / 100.0, "Rabatt", "0.00 %", "Anzahl", 1,"Gesamt",Math.round(temp.getTotalPrice() * 100.0) / 100.0 ));

             }


         }

        context.setVariable("InvoiceGrandtotal",Math.round(grandTotal * 100.0) / 100.0);

        context.setVariable("headers",headers);
        context.setVariable("rows",rows);

        return templateEngine.process("templates/invoice_template", context);
    }

    private BlobEntity generatePdfFromHtml(String html) throws IOException, DocumentException {
        String outputFolder = System.getProperty("user.home") + File.separator + "thymeleaf.pdf";
        System.out.println("folder:"+outputFolder);
        //OutputStream outputStream = new FileOutputStream(outputFolder);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocumentFromString(html);
        renderer.layout();
        renderer.createPDF(outputStream);
        return writeBlobToDb(outputStream);
    }
    @Transactional
    BlobEntity writeBlobToDb(ByteArrayOutputStream outputStream) throws IOException {
        InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
        byte[] byteArray = IOUtils.toByteArray(inputStream);

        BlobEntity blobEntity = new BlobEntity();
        blobEntity.setName("test");

        Byte[] a =new Byte[byteArray.length];

        for (int i = 0; i < a.length; i++) {
            a[i] = byteArray[i];
        }

        blobEntity.setData(a);
        outputStream.close();
        return blobRepository.save(blobEntity);
    }

    private String convertByteArrayToBase64String(Byte[] bytes){
        Byte[] tmp = bytes;
        byte[] a = new byte[tmp.length];
        for (int i = 0; i < a.length; i++) {
            a[i] = tmp[i];
        }
        byte[] encodedBytes = Base64.getEncoder().encode(a);
        return new String(encodedBytes);
    }

    public InvoiceDTO convertInvoiceToDTO(Invoice invoice) {
        InvoiceDTO invoiceDTO = new InvoiceDTO();
        invoiceDTO.setInvoiceNumber(invoice.getInvoiceNumber());
        invoiceDTO.setInvoiceCancellationDate(invoice.getCancellationDate());
        invoiceDTO.setId(invoice.getId());
        invoiceDTO.setCancelledFrom(invoice.getCancelledFrom());
        invoiceDTO.setInvoiceDate(invoice.getInvoiceDate());
        double grandTotal = 0;
        for (InvoiceLine i : invoice.getLineList()) {
            grandTotal += i.getTotal();
        }
        invoiceDTO.setAmount(grandTotal);
        invoiceDTO.setReservation(reservationService.convertReservationToDTO(invoice.getReservation()));

        return invoiceDTO;
    }
    private ArchivedRoomReservation convert(RoomReservation roomReservation){
        ArchivedRoomReservation archivedRoomReservation = new ArchivedRoomReservation();
        archivedRoomReservation.setOvercrowding(roomReservation.isOvercrowding());
        archivedRoomReservation.setCount(roomReservation.getCount());

        ArchivedRoomType roomType = new ArchivedRoomType();
        roomType.setName(roomReservation.getRoomType().getName());
        roomType.setPriceBase(roomReservation.getRoomType().getPriceBase());
        roomType.setPriceOvercrowding(roomReservation.getRoomType().getPriceOvercrowding());
        archivedRoomTypeRepository.save(roomType);
        archivedRoomReservation.setRoomType(roomType);
        archivedRoomReservationRepository.save(archivedRoomReservation);

        return archivedRoomReservation;
    }

    private ArchivedReservation convert(Reservation reservation){
        ArchivedReservation archivedReservation = new ArchivedReservation();
        archivedReservation.setArrivalDate(reservation.getArrivalDate());
        archivedReservation.setDepartureDate(reservation.getDepartureDate());
        archivedReservation.setDiscount(reservation.getDiscount());
        archivedReservation.setPrice(reservation.getPrice());
        archivedReservation.setCustomer(reservation.getCustomer());
        archivedReservation.setAddress(reservation.getAddress());
        List <ArchivedRoomReservation> roomReservations = new ArrayList<>();
        for (RoomReservation roomReservation: reservation.getRoomReservations()) {
            roomReservations.add(convert(roomReservation));
        }
        archivedReservation.setRoomReservations(roomReservations);
        archivedReservationRepository.save(archivedReservation);
        return archivedReservation;
    }


}
