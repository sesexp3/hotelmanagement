package sese.hotel.Service;

import com.google.gson.Gson;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import sese.hotel.Model.ConsumptionAPI.Consumption;

import java.net.SocketTimeoutException;
import java.time.Duration;
import java.util.concurrent.*;

@Service
public class ConsumptionService {

    private final RestTemplate restTemplate;

    ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

    private String cache = "";

    public ConsumptionService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder
                .setConnectTimeout(Duration.ofSeconds(20))
           .setReadTimeout(Duration.ofSeconds(20)).build();
    }

    public Consumption getConsumption(){
        String json = getPostsPlainJSON();
        return convertToObject(json);
    }

    private String getPostsPlainJSON() {
        System.out.println("API Call");
        String url = "https://sese-rest-api.herokuapp.com/api/customers/1/invoices";
        String temp= "";
        try{
            temp = this.restTemplate.getForObject(url, String.class);
        }
        catch(Exception e){
            System.out.println("API Timeout, using cache:"+ cache);
            temp = cache;
            executor.schedule(callableTask, 5, TimeUnit.SECONDS);
            return temp;
        }
        return temp;
    }

    private Consumption convertToObject(String json){
        Gson gson = new Gson();
        Consumption model = gson.fromJson(json, Consumption.class);
        return model;
    }

    Callable<String> callableTask = () -> {
        System.out.println("Creating Cache");
        this.cache = getPostsPlainJSON();
        return "Success:"+this.cache;
    };

    public void createBuffer() throws ExecutionException, InterruptedException {
        System.out.println("Creating Cache");
        Future<String> test = executor.submit(callableTask);
        System.out.println(test.get());
    }

}

