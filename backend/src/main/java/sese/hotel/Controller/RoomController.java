package sese.hotel.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sese.hotel.Controller.Message.DTO.RoomDTO;
import sese.hotel.Controller.Message.DTO.RoomTypeDTO;
import sese.hotel.Controller.Message.Response.ResponseMessage;
import sese.hotel.Model.Room;
import sese.hotel.Model.RoomType;
import sese.hotel.Repository.RoomRepository;
import sese.hotel.Repository.RoomTypeRepository;
import sese.hotel.Service.RoomService;

import java.util.List;

@RestController
@RequestMapping(value = "/room")
public class RoomController {
    @Autowired
    RoomService roomService;

    @Autowired
    RoomRepository roomRepository;

    @Autowired
    RoomTypeRepository roomTypeRepository;

    @PostMapping("/")
    ResponseEntity createRoom(
            @RequestBody RoomDTO roomDTO
    ) {
        try {
            this.roomService.addRoom(roomDTO);
            // TODO Should return newly created dto
            return new ResponseEntity(new ResponseMessage(""), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
            return new ResponseEntity(new ResponseMessage("ERROR:" + e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/")
    List<RoomDTO> getAll() {
        //TODO Ka. welcher HTTP Status code zurückgeliefert wird, im Fehlerfall.
        return roomService.findAll();
    }

    @PutMapping("/{id}")
    ResponseEntity updateRoom(
            @PathVariable Long id,
            @RequestBody RoomDTO roomDTO
    ) {
        try {
            roomService.updateRoom(id, roomDTO);
            return new ResponseEntity(new ResponseMessage(""), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(new ResponseMessage("ERROR: " + e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    ResponseEntity deleteRoom(@PathVariable Long id) {
        var wasDeleted = roomService.deleteRoom(id);
        if (wasDeleted) {
            return new ResponseEntity(new ResponseMessage(""), HttpStatus.OK);
        } else {
            return new ResponseEntity(new ResponseMessage("Could delete reservation " + id), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/type/")
    ResponseEntity createRoomType(
            @RequestBody RoomTypeDTO roomTypeDTO
    ) {
        try {
            this.roomService.addRoomType(roomTypeDTO);
            // TODO Should return newly created dto
            return new ResponseEntity(new ResponseMessage(""), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
            return new ResponseEntity(new ResponseMessage("ERROR:" + e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/type")
    List<RoomTypeDTO> getAllTypes() {
        //TODO Ka. welcher HTTP Status code zurückgeliefert wird, im Fehlerfall.
        return roomService.findAllTypes();
    }

    @PutMapping("/type/{id}")
    ResponseEntity updateRoomType(
            @PathVariable Long id,
            @RequestBody RoomTypeDTO roomTypeDTO
    ) {
        try {
            roomService.updateRoomType(id, roomTypeDTO);
            return new ResponseEntity(new ResponseMessage(""), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(new ResponseMessage("ERROR: " + e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/type/{id}")
    ResponseEntity deleteRoomType(@PathVariable Long id) {
        var wasDeleted = roomService.deleteRoomType(id);
        if (wasDeleted) {
            return new ResponseEntity(new ResponseMessage(""), HttpStatus.OK);
        } else {
            return new ResponseEntity(new ResponseMessage("Could delete reservation " + id), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/default")
    @Deprecated
    public ResponseEntity<?> createDefaultRooms(){
        RoomType dz = new RoomType();
        dz.setName("Doppelzimmer");
        dz.setPriceBase(200);
        dz.setPriceOvercrowding(250);
        //RoomType dzd = new RoomType();
        //dzd.setName("Doppelzimmer Delux");
        //dzd.setPriceBase(350);
        //dzd.setPriceOvercrowding(500);
        RoomType ez = new RoomType();
        ez.setName("Einzelzimmer");
        ez.setPriceBase(120);
        ez.setPriceOvercrowding(190);
        roomTypeRepository.saveAndFlush(dz);
        roomTypeRepository.saveAndFlush(ez);
        //roomTypeRepository.saveAndFlush(dzd);


        Room room = new Room();
        room.setName("Zimmer 101");
        room.setOccupancy(2);
        room.setOvercrowding(true);
        room.setRoomType(dz);
        roomRepository.save(room);
        room = new Room();
        room.setName("Zimmer 102");
        room.setOccupancy(2);
        room.setOvercrowding(true);
        room.setRoomType(dz);
        roomRepository.save(room);
        room = new Room();
        room.setName("Zimmer 103");
        room.setOccupancy(2);
        room.setOvercrowding(false);
        room.setRoomType(dz);
        roomRepository.save(room);

        room = new Room();
        room.setName("Zimmer 104");
        room.setOccupancy(1);
        room.setOvercrowding(true);
        room.setRoomType(ez);
        roomRepository.save(room);
        room = new Room();
        room.setName("Zimmer 105");
        room.setOccupancy(1);
        room.setOvercrowding(true);
        room.setRoomType(ez);
        roomRepository.save(room);


        room = new Room();
        room.setName("Zimmer 201");
        room.setOccupancy(2);
        room.setOvercrowding(false);
        room.setRoomType(dz);
        roomRepository.save(room);
        room = new Room();
        room.setName("Zimmer 202");
        room.setOccupancy(2);
        room.setOvercrowding(false);
        room.setRoomType(dz);
        roomRepository.save(room);

        return ResponseEntity.ok("Default Rooms added: DZ 3(1/2), EZ 2(0/2), DZD 2(2/0)");
    }
}
