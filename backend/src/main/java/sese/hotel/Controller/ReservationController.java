package sese.hotel.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sese.hotel.Controller.Message.DTO.RoomReservationStatsDTO;
import sese.hotel.Controller.Message.Response.ResponseMessage;
import sese.hotel.Controller.Message.DTO.ReservationDTO;
import sese.hotel.Service.ReservationService;

import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping(value = "/reservation")
public class ReservationController {
    @Autowired
    ReservationService reservationService;

    @GetMapping("/stats")
    RoomReservationStatsDTO getStats() {
        return this.reservationService.getStats();
    }

    @PostMapping("/")
    ResponseEntity createReservation(
            @RequestBody ReservationDTO reservation
    ) {
        try {
            this.reservationService.addReservation(reservation);
            // TODO Should return newly created dto
            return new ResponseEntity(new ResponseMessage(""), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
            return new ResponseEntity(new ResponseMessage("ERROR:" + e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/")
    List<ReservationDTO> getAll() {
        //TODO Ka. welcher HTTP Status code zurückgeliefert wird, im Fehlerfall.
        return reservationService.findAll();
    }

    @PutMapping("/{id}")
    ResponseEntity updateReservation(
            @PathVariable Long id,
            @RequestBody ReservationDTO reservationDTO
    ) {
        try {
            reservationService.updateReservation(id, reservationDTO);
            return new ResponseEntity(new ResponseMessage(""), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(new ResponseMessage("ERROR: " + e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    ResponseEntity deleteReservation(@PathVariable Long id) {
        var wasDeleted = reservationService.deleteReservation(id);
        if (wasDeleted) {
            return new ResponseEntity(new ResponseMessage(""), HttpStatus.OK);
        } else {
            return new ResponseEntity(new ResponseMessage("Could delete reservation " + id), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/check/")
    ReservationDTO checkReservation(@RequestBody ReservationDTO reservationDTO) {
        ReservationDTO returnReservation = null;
        try {
            returnReservation = reservationService.checkReservation(reservationDTO);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return returnReservation;
    }
}
