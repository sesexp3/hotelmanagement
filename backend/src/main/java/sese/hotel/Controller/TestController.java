package sese.hotel.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import sese.hotel.Model.Customer;
import sese.hotel.Model.Employee;
import sese.hotel.Model.User;
import sese.hotel.Repository.CustomerRepository;
import sese.hotel.Repository.UserRepository;

import java.util.Random;

@RestController
public class TestController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    CustomerRepository employeeRepository;

    @Autowired
    PasswordEncoder encoder;

    @GetMapping(value = "/", produces = MediaType.TEXT_PLAIN_VALUE)
    public String index() {
        User test = new User();
        test.setEmail("test@test.com");
        test.setName("TestName");
        Random rand = new Random();
        Customer employee = new Customer();
        employee.setName("asd");
        employee.setEmail("asda"+rand.nextInt());
        employee.setPassword("asdasd");
        employee.customer_number=512;
        employeeRepository.save(employee);

        Customer customer = new Customer();
        customer.setPassword(encoder.encode("admin"));
        employeeRepository.save(customer);

        return "Hello, World!";//+test.getId();
    }

}
