/*
package sese.hotel.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sese.hotel.Controller.Message.DTO.ReservationDTO;
import sese.hotel.Controller.Message.Response.ResponseMessage;
import sese.hotel.Model.Invoice;
import sese.hotel.Repository.InvoiceRepository;
import sese.hotel.Service.InvoiceService;

import java.sql.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/invoices")
public class InvoiceController {

    @Autowired
    private InvoiceService invoiceService;

    @Autowired
    private InvoiceRepository invoiceRepository;

    @PutMapping()
    public ResponseEntity createInvoice(@RequestBody long reservationId) {
        return new ResponseEntity<>(new ResponseMessage("Invoice cancelled:" + invoiceService.createInvoice(reservationId)), HttpStatus.OK);
        //return new ResponseEntity<>(new ResponseMessage("Invoice cancelled:"+invoiceService.cancelInvoice(Long.parseLong(invoiceNumber)).toString()), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity cancelInvoice(@RequestBody Invoice invoice) {
        return new ResponseEntity<>(new ResponseMessage("Invoice cancelled:" + invoiceService.cancelInvoice(invoice)), HttpStatus.OK);
        //return new ResponseEntity<>(new ResponseMessage("Invoice cancelled:"+invoiceService.cancelInvoice(Long.parseLong(invoiceNumber)).toString()), HttpStatus.OK);
    }
   @DeleteMapping("/{invoiceNumber}")
    public ResponseEntity cancelInvoiceByNumber(@PathVariable long invoiceNumber) {
        return new ResponseEntity<>(invoiceService.cancelInvoice(invoiceNumber), HttpStatus.OK);
    }

    @GetMapping("/{invoiceNumber}")
    public ResponseEntity getInvoice(@PathVariable long invoiceNumber) {
        return new ResponseEntity<>(invoiceService.getInvoiceForInvoiceNumber(invoiceNumber), HttpStatus.OK);
    }

    @GetMapping("/default")
    public ResponseEntity createDefaultInvoice() {
        Invoice invoice = new Invoice();
        invoice.setInvoiceNumber(1L);
        invoice.setCancellationDate(new Date(System.currentTimeMillis()));
        invoice.setCancelledFrom("admin");
        invoiceRepository.save(invoice);
        return new ResponseEntity<>(new ResponseMessage("Invoice created:"+invoiceRepository.findByInvoiceNumber(invoice.getInvoiceNumber())), HttpStatus.OK);
    }


    // Cancellations

    @GetMapping("/cancellations")
    public List<Invoice> getAllCancelledInvoices() {
        */
/*StringBuilder responseBuilder = new StringBuilder();
        responseBuilder.append("{[");
        List<Invoice> cancelledInvoices = invoiceRepository.findByCancellationDateNotNull();
        for (Invoice currentInvoice : cancelledInvoices) {
            responseBuilder.append(currentInvoice.toString());
            responseBuilder.append(",");
        }
        if(responseBuilder.charAt(responseBuilder.length()-1) == ',') responseBuilder.deleteCharAt(responseBuilder.length()-1);
        responseBuilder.append("]}");*//*

        //return new ResponseEntity<>(new ResponseMessage(responseBuilder.toString()), HttpStatus.OK);

        return invoiceRepository.findByCancellationDateNotNull();
    }
    */
/*@DeleteMapping("/{invoiceNumber}")
    public ResponseEntity revokeCancellation(@PathVariable Long invoiceNumber) {
        boolean ret = invoiceService.revokeCancellation(invoiceNumber);
        if(ret) return new ResponseEntity<>(new ResponseMessage("Successfully revoked cancellation: "+ invoiceNumber),HttpStatus.OK);
        else return new ResponseEntity<>(new ResponseMessage("Deletion Failed"), HttpStatus.BAD_REQUEST);
    }*//*

}
*/
