package sese.hotel.Controller.Message.Request;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

public class LogoutRequest implements Serializable {
    @NotBlank
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
