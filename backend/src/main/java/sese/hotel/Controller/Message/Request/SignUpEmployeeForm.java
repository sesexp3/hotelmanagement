package sese.hotel.Controller.Message.Request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

public class SignUpEmployeeForm implements Serializable {

    @NotBlank
    private String username;

    @NotBlank
    @Email
    private String email;

    private String name;

    //private Set<String> role;

    @NotBlank
    private String password;

    private String employeeCode;

    private String dateOfBirth;

    private String sex;

    private String phoneNumber;

    private String insuranceNumber;

    private String IBAN;

    private String minutesPerWeek;

    private AddressForm address;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getInsuranceNumber() {
        return insuranceNumber;
    }

    public void setInsuranceNumber(String insuranceNumber) {
        this.insuranceNumber = insuranceNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AddressForm getAddress() {
        return address;
    }

    public void setAddress(AddressForm address) {
        this.address = address;
    }

    public String getIBAN() {
        return IBAN;
    }

    public void setIBAN(String IBAN) {
        this.IBAN = IBAN;
    }

    public String getMinutesPerWeek() {
        return minutesPerWeek;
    }

    public void setMinutesPerWeek(String minutesPerWeek) {
        this.minutesPerWeek = minutesPerWeek;
    }

    @Override
    public String toString() {
        return "SignUpEmployeeForm{" +
                "username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", employeeCode='" + employeeCode + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", sex='" + sex + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", insuranceNumber='" + insuranceNumber + '\'' +
                ", IBAN='" + IBAN + '\'' +
                ", minutesPerWeek='" + minutesPerWeek + '\'' +
                ", address=" + address +
                '}';
    }
}
