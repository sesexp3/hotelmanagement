package sese.hotel.Controller.Message.DTO;
import java.util.List;

public class RoomDTO {
    public long id;
    public String name;
    public int occupancy; // Belegzahl
    public boolean overcrowding; // Überbelegung möglich
    public RoomTypeDTO roomType;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
