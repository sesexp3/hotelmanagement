package sese.hotel.Controller.Message.Request;

import com.fasterxml.jackson.annotation.JsonAlias;
import sese.hotel.Controller.Message.DTO.AddressDTO;
import sese.hotel.Controller.Message.DTO.EmployeeDTO;
import sese.hotel.Controller.Message.DTO.UserDTO;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

public class SignUpEmployeeForm2 implements Serializable {

    @JsonAlias("employee")
    public EmployeeDTO employeeDTO;
    @JsonAlias("user")
    public UserDTO userDTO;
    @JsonAlias("address")
    public AddressDTO addressDTO;

    public EmployeeDTO getEmployeeDTO() {
        return employeeDTO;
    }

    public void setEmployeeDTO(EmployeeDTO employeeDTO) {
        this.employeeDTO = employeeDTO;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    @Override
    public String toString() {
        return "SignUpEmployeeForm2{" +
                "employeeDTO=" + employeeDTO +
                ", userDTO=" + userDTO +
                ", addressDTO=" + addressDTO +
                '}';
    }
}
