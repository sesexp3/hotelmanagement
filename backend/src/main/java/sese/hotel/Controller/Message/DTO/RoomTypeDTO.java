package sese.hotel.Controller.Message.DTO;

public class RoomTypeDTO {
    public long id;
    public String name;
    public double priceBase;
    public double priceOvercrowding;
    public int quantity;
    public int quantityOvercrowding;
}
