package sese.hotel.Controller.Message.DTO;

import java.util.List;

/**
 * export interface Reservation {
 *   id: number | string; // Id of reservation
 *   customer: string; // Pre + Sur name of customer
 *   customerId: string; // Customer id
 *   firstName: string; // first name of customer
 *   lastName: string; // last name of customer
 *   email: string; // emailaddress of customer
 *   tel: string; // telefonnumber of customer
 *   address: string;  // Address of customer
 *   postcode: string; // Postcode of customer
 *   city: string; // City of customer
 *   rooms: RoomReservation[];  // Room reservations made by the customer
 *   arrival_date: Date; // Arrival date of the customer
 *   departure_date: Date; // Departure date of the customer
 *   num_stays_days: number;  // Number of days at the hotel
 *   price: number;  // Price in €
 *   discount: number; // Discount in percentage.
 * }
 */
public class ReservationDTO {
    public long id;
    public String customer;
    public String customerId;
    public String firstName;
    public String lastName;
    public String email;
    public String tel;
    public String address;
    public String postcode;
    public String city;
    public List<RoomReservationDTO> rooms;
    public String arrival_date;
    public String departure_date;
    public int num_stays_days;
    public double price;
    public double discount;
    public PaymentInformationDTO paymentInformation;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
