package sese.hotel.Controller.Message.DTO;

import java.time.LocalDate;

public class DailyOccupancyDTO {
    public LocalDate date;
    public int occupancy;

    public DailyOccupancyDTO(LocalDate date, int occupancy) {
        this.date = date;
        this.occupancy = occupancy;
    }
}
