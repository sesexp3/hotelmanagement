package sese.hotel.Controller.Message.DTO;

import java.io.Serializable;
import java.sql.Date;

public class InvoiceDTO implements Serializable {

    private long id;

    private long invoiceNumber;

    //the total sum over all costs
    private double amount;

    private ReservationDTO reservation;

    private Date invoiceCancellationDate;

    private Date invoiceDate;

    private String cancelledFrom;

    public long getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(long invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Date getInvoiceCancellationDate() {
        return invoiceCancellationDate;
    }

    public void setInvoiceCancellationDate(Date invoiceCancellationDate) {
        this.invoiceCancellationDate = invoiceCancellationDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCancelledFrom() {
        return cancelledFrom;
    }

    public void setCancelledFrom(String cancelledFrom) {
        this.cancelledFrom = cancelledFrom;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }


    public ReservationDTO getReservation() {
        return reservation;
    }

    public void setReservation(ReservationDTO reservation) {
        this.reservation = reservation;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    @Override
    public String toString() {
        return "InvoiceDTO{" +
                "id=" + id +
                ", invoiceNumber=" + invoiceNumber +
                ", amount=" + amount +
                ", reservation=" + reservation +
                ", invoiceCancellationDate=" + invoiceCancellationDate +
                ", invoiceDate=" + invoiceDate +
                ", cancelledFrom='" + cancelledFrom + '\'' +
                '}';
    }
}
