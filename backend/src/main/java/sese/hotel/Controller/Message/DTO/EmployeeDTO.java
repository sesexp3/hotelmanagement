package sese.hotel.Controller.Message.DTO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import sese.hotel.Controller.Message.Request.AddressForm;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

public class EmployeeDTO implements Serializable {

    private String employeeCode;

    private String dateOfBirth;

    private String sex;

    private String phoneNumber;

    private String insuranceNumber;

    private String IBAN;

    private String minutesPerWeek;

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getInsuranceNumber() {
        return insuranceNumber;
    }

    public void setInsuranceNumber(String insuranceNumber) {
        this.insuranceNumber = insuranceNumber;
    }

    public String getIBAN() {
        return IBAN;
    }

    public void setIBAN(String IBAN) {
        this.IBAN = IBAN;
    }

    public String getMinutesPerWeek() {
        return minutesPerWeek;
    }

    public void setMinutesPerWeek(String minutesPerWeek) {
        this.minutesPerWeek = minutesPerWeek;
    }

    @Override
    public String toString() {
        return "EmployeeDTO{" +
                "employeeCode='" + employeeCode + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", sex='" + sex + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", insuranceNumber='" + insuranceNumber + '\'' +
                ", IBAN='" + IBAN + '\'' +
                ", minutesPerWeek='" + minutesPerWeek + '\'' +
                '}';
    }
}
