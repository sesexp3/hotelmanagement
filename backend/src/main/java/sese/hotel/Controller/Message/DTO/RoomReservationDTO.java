package sese.hotel.Controller.Message.DTO;

public class RoomReservationDTO {
    public int count;
    public RoomTypeDTO room_type;
    public boolean overcrowding;
}
