package sese.hotel.Controller.Message.Request;

public class InvoiceCancelRequest {

    private long invoiceNumber;

    public long getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(long invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }
}
