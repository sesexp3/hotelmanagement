package sese.hotel.Controller.Message.DTO;

import java.io.Serializable;

public class PaymentInformationDTO implements Serializable {

    private String creditCardNumber;
    private String creditCardHolder;
    private String creditCardValidUntilMonth;
    private String creditCardValidUntilYear;
    private String creditCardVerificationNumber;

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public String getCreditCardHolder() {
        return creditCardHolder;
    }

    public void setCreditCardHolder(String creditCardHolder) {
        this.creditCardHolder = creditCardHolder;
    }

    public String getCreditCardValidUntilMonth() {
        return creditCardValidUntilMonth;
    }

    public void setCreditCardValidUntilMonth(String creditCardValidUntilMonth) {
        this.creditCardValidUntilMonth = creditCardValidUntilMonth;
    }

    public String getCreditCardValidUntilYear() {
        return creditCardValidUntilYear;
    }

    public void setCreditCardValidUntilYear(String creditCardValidUntilYear) {
        this.creditCardValidUntilYear = creditCardValidUntilYear;
    }

    public String getCreditCardVerificationNumber() {
        return creditCardVerificationNumber;
    }

    public void setCreditCardVerificationNumber(String creditCardVerificationNumber) {
        this.creditCardVerificationNumber = creditCardVerificationNumber;
    }

    @Override
    public String toString() {
        return "PaymentInformationDTO{" +
                "creditCardNumber='" + creditCardNumber + '\'' +
                ", creditCardHolder='" + creditCardHolder + '\'' +
                ", creditCardValidUntilMonth='" + creditCardValidUntilMonth + '\'' +
                ", creditCardValidUntilYear='" + creditCardValidUntilYear + '\'' +
                ", getCreditCardVerificationNumber='" + creditCardVerificationNumber + '\'' +
                '}';
    }
}
