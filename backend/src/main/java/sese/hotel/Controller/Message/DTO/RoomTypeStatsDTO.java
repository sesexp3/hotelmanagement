package sese.hotel.Controller.Message.DTO;

import java.util.List;

public class RoomTypeStatsDTO {
    public String roomType;
    public List<DailyOccupancyDTO> occupancies;
}
