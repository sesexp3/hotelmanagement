package sese.hotel.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import sese.hotel.Controller.Message.Request.LoginRequest;
import sese.hotel.Controller.Message.Request.LogoutRequest;
import sese.hotel.Controller.Message.Request.SignUpEmployeeForm2;
import sese.hotel.Controller.Message.Response.JwtResponse;
import sese.hotel.Controller.Message.Response.ResponseMessage;
import sese.hotel.Model.Address;
import sese.hotel.Model.Employee;
import sese.hotel.Model.Enum.TaskArea;
import sese.hotel.Repository.CustomerRepository;
import sese.hotel.Repository.EmployeeRepository;
import sese.hotel.Repository.RoleRepository;
import sese.hotel.Repository.UserRepository;
import sese.hotel.Security.JwtProvider;
import sese.hotel.Service.EmployeeService;
import sese.hotel.Service.UserService;

import javax.validation.Valid;
import java.sql.Date;

@RestController
@RequestMapping(value = "/employees")
public class EmployeeController {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    EmployeeService employeeService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    UserService userService;

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtProvider.generateJwtToken(authentication);
        userService.storeToken(loginRequest.getUsername(),jwt);
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getUsername()));
    }

    @PostMapping("/logout")
    public ResponseEntity<Boolean> logout(@Valid @RequestBody LogoutRequest logoutRequest) {

        if (userService.logout(logoutRequest.getEmail())) {
            return new ResponseEntity<>(true, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);
        }
    }


    @PostMapping("/")
    public ResponseEntity<?> registerUser(@RequestBody SignUpEmployeeForm2 signUpRequest) {

        System.out.println(signUpRequest.toString());

        try {
            employeeService.createAndStoreEmployee2(signUpRequest);
        }
        catch (DataIntegrityViolationException  e){
            return new ResponseEntity<>(new ResponseMessage("Fail -> Email is already in use!"),HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new ResponseMessage("Employee registered successfully!"), HttpStatus.OK);
    }


    @GetMapping("/default")
    @Deprecated
    public ResponseEntity<?> createDefaultEmployee(){
        Employee employee = new Employee();
        employee.setPassword(encoder.encode("admin"));
        employee.setName("Test User");
        employee.setEmail("admin@admin.com");
        employee.setMinutesPerWeek(1000);
        employee.setIban("asdasd");
        employee.setEmployeeCode("asd");
        employee.setDateOfBirth(Date.valueOf("2020-01-01"));
        employee.setPhoneNumber("1111");
        employee.setInsuranceNumber("11111");
        employee.setSex("M");
        employee.setTaskArea(TaskArea.OTHERS);

        Address address = new Address();
        address.setStreet("asd");
        address.setCity("asda");
        address.setPostcode("11");
        address.setNumber("11");
        address.setCountry("asda");
        employee.setAddress(address);
        employeeRepository.save(employee);

        return ResponseEntity.ok("Default User added: admin@admin.com/admin");
    }



    @GetMapping("/{userId}")
    public ResponseEntity getEmployee(@PathVariable long userId) {
        return new ResponseEntity<>(employeeRepository.findById(userId), HttpStatus.OK);
    }
/*    @GetMapping("/{userId}/base")
    public ResponseEntity getBaseEmployeeInformation(@PathVariable long userId) {
        return new ResponseEntity<>(new ResponseMessage("Employee:"+employeeRepository.findById(userId).toString()), HttpStatus.OK);
    }
    @GetMapping("/{userId}/extended")
    public ResponseEntity getExtendedEmployeeInformation(@PathVariable long userId) {
        return new ResponseEntity<>(new ResponseMessage("Employee:"+employeeRepository.findById(userId).toString()), HttpStatus.OK);
    }
    @GetMapping("/{userId}/address")
    public ResponseEntity getAddressEmployeeInformation(@PathVariable long userId) {
        return new ResponseEntity<>(new ResponseMessage("Employee:"+employeeRepository.findById(userId).toString()), HttpStatus.OK);
    }*/
/*    @PatchMapping("/{userId}")
    public ResponseEntity editEmployee(@PathVariable long userId) {
        return new ResponseEntity<>(new ResponseMessage("Employee:"+employeeRepository.findById(userId).toString()), HttpStatus.OK);
    }*/




}
