package sese.hotel.Controller;

import com.lowagie.text.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import sese.hotel.Controller.Message.DTO.InvoiceDTO;
import sese.hotel.Controller.Message.Response.ResponseMessage;
import sese.hotel.Model.*;
import sese.hotel.Repository.*;
import sese.hotel.Service.DefaultService;
import sese.hotel.Service.InvoiceService;
import sese.hotel.Service.ReservationService;

import javax.transaction.Transactional;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/invoices")
public class InvoiceController2 {

    @Autowired
    private InvoiceService invoiceService;

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private ReservationRepository reservationRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private RoomReservationRepository roomReservationRepository;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    ReservationService reservationService;

    @Autowired
    PasswordEncoder encoder;

    @PutMapping("/{reservationId}")
    public ResponseEntity createInvoice(@PathVariable long reservationId) {
        InvoiceDTO invoice = invoiceService.createInvoice(reservationId);
        if(invoice == null){
            return new ResponseEntity<>(new ResponseMessage("Reservation not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(invoice, HttpStatus.OK);
    }

    @DeleteMapping("/{invoiceNumber}")
    public ResponseEntity cancelInvoiceByNumber(@PathVariable long invoiceNumber) {
        InvoiceDTO invoice = invoiceService.cancelInvoice(invoiceNumber);
        if(invoice == null){
            return new ResponseEntity<>(new ResponseMessage("Invoice not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(invoice, HttpStatus.OK);
    }

    @GetMapping("/{invoiceNumber}")
    public ResponseEntity getInvoice(@PathVariable long invoiceNumber) {
        InvoiceDTO invoice = invoiceService.getInvoiceForInvoiceNumber(invoiceNumber);
        if(invoice == null){
            return new ResponseEntity<>(new ResponseMessage("Invoice not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(invoice, HttpStatus.OK);

    }

    @GetMapping("/reservation/{reserverationId}")
    @Transactional
    public ResponseEntity getInvoiceByReservation(@PathVariable long reserverationId) {
        Invoice invoice = invoiceService.getInvoiceForReservation(reserverationId);
        if(invoice == null){
            return new ResponseEntity<>(new ResponseMessage("Reservation not found"), HttpStatus.NOT_FOUND);
        }
        InvoiceDTO invoiceDTO = new InvoiceDTO();
        invoiceDTO.setInvoiceNumber(invoice.getInvoiceNumber());
        invoiceDTO.setInvoiceCancellationDate(invoice.getCancellationDate());
        invoiceDTO.setId(invoice.getId());
        invoiceDTO.setCancelledFrom(invoice.getCancelledFrom());
        invoiceDTO.setInvoiceDate(invoice.getInvoiceDate());
        double grandTotal = 0;
        for(InvoiceLine i:invoice.getLineList()){
            grandTotal+=i.getTotal();
        }
        invoiceDTO.setAmount(grandTotal);
        return new ResponseEntity<>(invoiceDTO, HttpStatus.OK);
    }

    @GetMapping("/")
    @Transactional
    public ResponseEntity getAllInvoices() {
        List<InvoiceDTO> invoiceDTOS = invoiceService.getAllInvoices();

        return new ResponseEntity<>(invoiceDTOS, HttpStatus.OK);
    }

    @PostMapping("/{invoiceNumber}/revoke")
    public ResponseEntity revokeCancellation(@PathVariable Long invoiceNumber) {
        invoiceService.revokeCancellation(invoiceNumber);
        return new ResponseEntity<>(new ResponseMessage("Successfully revoked cancellation: "+ invoiceNumber),HttpStatus.OK);
    }

    @GetMapping("/{invoiceNumber}/pdf")
    public ResponseEntity getInvoicePDF(@PathVariable long invoiceNumber) {
        try {
            String base64PDF= invoiceService.getOrCreateInvoicePdfForInvoiceNumber(invoiceNumber);
            if(base64PDF==null) return new ResponseEntity<>(new ResponseMessage("This invoice doesn't exist"), HttpStatus.NOT_FOUND);
            else return new ResponseEntity<>(base64PDF, HttpStatus.OK);
        } catch (IOException | DocumentException e) {
            return new ResponseEntity<>(new ResponseMessage(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*@GetMapping("/default")
    public ResponseEntity createDefaultInvoice() {
        Reservation reservation = new Reservation();
        Address address = new Address();
        address.setStreet("asd");
        address.setCity("asda");
        address.setPostcode("11");
        address.setNumber("11");
        address.setCountry("asda");
        addressRepository.save(address);

        Customer customer = new Customer();
        customer.setPassword(encoder.encode("admin"));
        customer.setName("Test User");
        customer.setEmail("admin@admin.com");
        customer.setPhoneNumber("1111");
        customer.setAddress(address);
        customerRepository.save(customer);

        reservation.setAddress(address);
        reservation.setDepartureDate(new Date(System.currentTimeMillis()));
        reservation.setArrivalDate(new Date(System.currentTimeMillis()));
        reservation.setCustomer(customer);
        reservation.setDiscount(0d);
        reservation.setPrice(100d);

        //defaults.createDefaultRooms();

        RoomReservation roomReservation = new RoomReservation();
        roomReservation.setRoom(roomRepository.findAll().get(0));
        roomReservation.setCount(1);
        roomReservation.setRoomType("Test");

        roomReservationRepository.save(roomReservation);

        List<RoomReservation> roomReservations = new ArrayList<>();
        roomReservations.add(roomReservation);
        reservation.setRoomReservations(roomReservations);

        reservationRepository.saveAndFlush(reservation);

        InvoiceDTO invoice=invoiceService.createInvoice(reservation.getId());

        return new ResponseEntity<>(new ResponseMessage("Invoice created:"+invoice.getInvoiceNumber()), HttpStatus.OK);
    }*/
}
