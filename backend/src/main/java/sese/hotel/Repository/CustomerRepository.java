package sese.hotel.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sese.hotel.Model.Customer;
import sese.hotel.Model.Employee;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
