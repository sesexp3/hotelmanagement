package sese.hotel.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sese.hotel.Model.Archive.ArchivedRoomReservation;
import sese.hotel.Model.RoomReservation;

public interface ArchivedRoomReservationRepository extends JpaRepository<ArchivedRoomReservation, Long> {
}
