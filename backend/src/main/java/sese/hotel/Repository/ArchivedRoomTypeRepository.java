package sese.hotel.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sese.hotel.Model.Archive.ArchivedRoomType;
import sese.hotel.Model.RoomType;

public interface ArchivedRoomTypeRepository extends JpaRepository<ArchivedRoomType, Long> {
}
