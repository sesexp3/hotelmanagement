package sese.hotel.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sese.hotel.Model.InvoiceLine;
import sese.hotel.Model.InvoiceLineConsumption;

@Repository
public interface InvoiceLineConsumptionRepository extends JpaRepository<InvoiceLineConsumption, Long> {
}
