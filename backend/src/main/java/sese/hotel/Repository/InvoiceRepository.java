package sese.hotel.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sese.hotel.Model.Archive.ArchivedReservation;
import sese.hotel.Model.Invoice;
import sese.hotel.Model.Reservation;
import sese.hotel.Model.Role;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface InvoiceRepository  extends JpaRepository<Invoice, Long> {

    List<Invoice> findByCancellationDateNotNull();

    Invoice findByInvoiceNumber(Long invoiceNumber);

    List<Invoice> findByOriginalReservationNumber(int originalReservationNumber);
}
