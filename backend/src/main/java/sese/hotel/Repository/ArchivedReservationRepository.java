package sese.hotel.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sese.hotel.Model.Archive.ArchivedReservation;
import sese.hotel.Model.Archive.ArchivedRoomType;

public interface ArchivedReservationRepository extends JpaRepository<ArchivedReservation, Long> {
}
