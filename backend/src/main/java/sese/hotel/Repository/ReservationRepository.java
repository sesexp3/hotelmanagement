package sese.hotel.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sese.hotel.Model.Reservation;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {
}
