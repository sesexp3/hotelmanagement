package sese.hotel.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sese.hotel.Model.Employee;
import sese.hotel.Model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {


}
