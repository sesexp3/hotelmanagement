package sese.hotel.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sese.hotel.Model.RoomReservation;

public interface RoomReservationRepository extends JpaRepository<RoomReservation, Long> {
}
