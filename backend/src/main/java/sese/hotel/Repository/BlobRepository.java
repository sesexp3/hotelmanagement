package sese.hotel.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sese.hotel.Model.BlobEntity;
import sese.hotel.Model.Customer;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface BlobRepository extends JpaRepository<BlobEntity, Long> {
}
