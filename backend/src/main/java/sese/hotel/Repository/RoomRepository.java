package sese.hotel.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import sese.hotel.Model.Room;
import sese.hotel.Model.RoomReservation;
import sese.hotel.Model.RoomType;

import java.util.List;

public interface RoomRepository extends JpaRepository<Room, Long> {
    public List<Room> findAllByRoomType(RoomType roomType);
    public List<Room> findAllByRoomTypeAndOvercrowding(RoomType roomType, boolean overcrowding);
}
