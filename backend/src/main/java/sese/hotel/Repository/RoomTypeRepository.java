package sese.hotel.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sese.hotel.Model.RoomType;

public interface RoomTypeRepository extends JpaRepository<RoomType, Long> {
}
