package sese.hotel.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sese.hotel.Model.Address;
import sese.hotel.Model.Invoice;
import sese.hotel.Model.Reservation;

import java.util.List;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {

}
