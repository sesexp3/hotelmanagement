# Hotelmanagement

A hotel management software developed as part of the Software Seminar course (WS-20/21) at TU-Wien.

## Installation
To build or run this project locally, install following dependencies:

## Downloads

### NodeJS
Install the latest version of NodeJS
https://nodejs.org/en/download/

### Angular CLI
Install as described here, make sure you have NodeJS already installed prior.
> npm install -g @angular/cli
https://angular.io/guide/setup-local

### Java JDK 1.15
Install Java Development Kit in version 1.15
https://jdk.java.net/15/

### Docker
For Windows10 Pro, Education or Enterprise, install [Docker for Windows] (https://docs.docker.com/docker-for-windows/install/) 

Docker Toolbox is not recommended, but may be used on user's own risk.

## Build or run locally

`cd /<YOUR_DIR>`

start all parts: 
`docker-compose up -d`

Currently the provided frontend communicates hardcoded with the deployment server.
It is therfore advised to start and use ng serve manually as described in "individual start up". Use only the frontend this way.
For completeness, the frontend provided by compose up is running on localhost:80



## Individual start up

### Starting up frontend

`cd /<YOUR_DIR>/frontend` 

Install all npm dependencies
`npm i`

Starting up development server
`ng serve` 

You can access the frontend using following URL in your browser
`http://localhost:4200`

## Starting up Database

To faciliate database setup, you can start up PostgreSQL using Docker.

`cd /<YOUR_DIR>/` 

In the root directory, where the docker-compose.yml file is located, execute:
`docker-compose run --service-ports db`

This will start the PostgreSQL instance at localhost:5432 with the following login credentials

>      POSTGRES_USER: postgres
>      POSTGRES_PASSWORD: postgres
>      POSTGRES_DB: postgres

To stop execute:

`docker-compose stop db`

### Starting up Backend

`cd /<YOUR_DIR>/backend` 

`mvn clean install` 

For the first time, use mvn clean install,
(then use mvn install for all following builds)

Before starting Spring server, make sure that the path to the database is pointed to localhost.
Otherwise the server won't start.

In `src/main/resources/application.properties` file, change `spring.datasource.url=jdbc:postgresql://db:5432/postgres`
to `spring.datasource.url=jdbc:postgresql://localhost:5432/postgres`.

DO NOT PUSH THIS CHANGE TO MAIN REPOSITORY and UNDO THE CHANGE BEFORE SUBMITTING TO CODE REPOSITORY.

Run Spring Server 
 `cd /YOUR_DIR/backend`  
 `mvn spring-boot:run` 
 
The server is now available at http://localhost:8081
 
### Development Environment

Any IDEs or Editors can be used, we recommend the usage of 

VSCode for the frontend
IntelliJ Idea for the backend

### Continous Integration 

The continous integration system tests and builds the project automatically.
It goes through the stages: test, build

The test stage is triggered before each push to a branch marked with an open merge request, a merge into the 'dev' branch, a merge into the 'master' branch.

For Angular, it executes tests using Karma, Protractor.
For Java, it executes tests using Spring Boot Test (Surefire).

The build stage is triggered before each push a merge into the 'dev' branch, a merge into the 'master' branch and after a sucessful run of the test stage.

It builds the frontend, backend using the provided Dockerfiles and pushes the built apps to the Container Registry provided in Gitlab.
https://gitlab.com/sesexp3/hotelmanagement/container_registry

### Continous deployment

The continous deployment system detects new versions of our application and deploys them automatically. Instead of a push principle, we use a pull principle where the server looks for application version changes by polling the Container Registry provided by Gitlab in a recurring manner using WatchTower and Docker-Compose.

To start execute the following command on your server:
`docker-compose -f docker-compose.deployment.portainer.yml up`  







